// eslint-disable-next-line @typescript-eslint/no-var-requires
const withTM = require('next-transpile-modules')(['react-markdown']);
// eslint-disable-next-line @typescript-eslint/no-var-requires
const withNx = require('@nrwl/next/plugins/with-nx');

module.exports = withTM(
  withNx({
    nx: {
      // SVGR is disabled (https://github.com/nrwl/nx/pull/6634)
      // in favor of next/image (https://nextjs.org/docs/basic-features/image-optimization)
      svgr: false,
    },
    webpack: (config, { webpack, isServer }) => {
      config.module.rules.push({
        test: /\.md$/,
        use: 'raw-loader',
      });
      // TODO: Upgrade to Next.js 10 / Webpack 5 to use this PR:
      // https://github.com/webpack/webpack/pull/11316
      config.plugins.push(
        new webpack.IgnorePlugin({
          resourceRegExp: /dotenv/,
        }),
      );

      if (!isServer) {
        config.resolve.fallback.fs = false;
        config.resolve.fallback.console = false;
      }

      return config;
    },
    images: {
      unoptimized: true,
    },
    experimental: {
      forceSwcTransforms: true,
    },
    env: {
      infuraKey: process.env.INFURA_PROJECT_ID,
    },
  }),
);
