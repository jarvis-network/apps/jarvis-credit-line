import React, { Suspense, useState } from 'react';

import { Icon, Modal, styled, useTheme } from '@jarvisnetwork/ui';

import { NetworkId } from '@jarvisnetwork/core-sdk';

import { useAccount, useDisconnect, useNetwork } from 'wagmi';

import { defaultSupportedNetworkId, NETWORKS } from '@jarvisnetwork/toolkit';

import { AccountBalances } from '../AccountBalances';
import { Loader } from '../Loader';

const ModalContainer = styled.div`
  background: ${props => props.theme.background.secondary};
  color: ${props => props.theme.text.primary};
  border-radius: 27px;
  height: 485px;
  width: 600px;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  overflow: hidden;

  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.mobileIndex]}px) {
    min-width: 80vw;
    max-width: calc(100% - 60px);
    max-height: 600px;
    width: auto;
    height: auto;
    margin: auto;
    overflow: auto;
  }
`;

const ModalWrapper = styled.div`
  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    .account-modal {
      > * {
        height: auto;
        padding-bottom: 30px;
      }
    }
  }
`;

const HeaderWrapper = styled.div`
  display: block;
  background: ${props => props.theme.background.primary};
  font-size: ${props => props.theme.font.sizes.s};
  align-items: center;
  padding: 31px 24px;
`;
const HeaderTitle = styled.strong`
  text-align: center;
  color: ${props => props.theme.text.primary};
  margin: auto;
`;

const IconButton = styled.button`
  border: none;
  background: none;
  padding: 10px;
  cursor: pointer;
  outline: none !important;
  color: ${props => props.theme.text.primary};
  i svg {
    width: 12px;
    height: 12px;
    position: relative;
    top: 3px;
    fill: ${props => props.theme.text.primary};
  }
`;

const HeaderIconButton = styled(IconButton)`
  padding: 0;
  position: flex;
  color: ${props => props.theme.text.primary};
`;

const HeaderTitleWrapper = styled.div`
  display: flex;
  margin-bottom: 22px;
`;
const HeaderActionContainer = styled.div`
  display: grid;
  grid-template-columns: auto auto auto;
  justify-items: center;
  font-size: ${props => props.theme.font.sizes.xs};
`;
const ActionHeaderButton = styled(IconButton)`
  position: flex;
  padding: 0;
  i {
    margin-right: 8px;
  }
`;

const LoaderWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

interface AccountSummaryProps {
  toggleAccountSummaryModal: () => void;
  isAccountSummaryModalOpen: boolean;
}

export const AccountSummary = ({
  toggleAccountSummaryModal,
  isAccountSummaryModalOpen,
}: AccountSummaryProps) => {
  const theme = useTheme();
  const { connector: activeConnector, address } = useAccount();
  const { chain } = useNetwork();
  const chainId = chain?.id ?? defaultSupportedNetworkId;

  const { disconnect } = useDisconnect();

  const [isCopied, setIsCopied] = useState(false);

  const handleCopy = () => {
    if (!address) return;
    const copyAddressToClipBoard = () => {
      if ('clipboard' in navigator) {
        return navigator.clipboard.writeText(address);
      }
      return document.execCommand('copy', true, address);
    };
    copyAddressToClipBoard();
    setIsCopied(true);
    setTimeout(() => {
      setIsCopied(false);
    }, 1500);
  };

  const handleViewOnExplorer = () => {
    if (!chainId) return;
    window.open(
      `${NETWORKS[chainId as NetworkId]?.blockExplorerUrls}/address/${address}`,
      '_blank',
    );
  };

  const handleDisconnected = () => {
    if (!activeConnector) return;
    // eslint-disable-next-line no-unused-expressions
    disconnect();
    toggleAccountSummaryModal();
  };

  return (
    <ModalWrapper>
      <Modal
        isOpened={isAccountSummaryModalOpen}
        onClose={toggleAccountSummaryModal}
        overlayStyle={{ zIndex: 3 }}
        overlayClassName="account-modal"
      >
        <ModalContainer>
          <HeaderWrapper>
            <HeaderTitleWrapper>
              <HeaderTitle>{address}</HeaderTitle>
              <HeaderIconButton onClick={toggleAccountSummaryModal}>
                <Icon icon="closeIcon" size="small" />
              </HeaderIconButton>
            </HeaderTitleWrapper>
            <HeaderActionContainer>
              <ActionHeaderButton onClick={handleCopy}>
                <Icon icon="copyIcon" size="small" />
                {isCopied ? 'Copied!' : 'Copy Address'}
              </ActionHeaderButton>
              <ActionHeaderButton onClick={handleViewOnExplorer}>
                <Icon icon="launchIcon" size="small" />
                View on Explorer
              </ActionHeaderButton>
              <ActionHeaderButton onClick={handleDisconnected}>
                <Icon icon="disconnectIcon" size="small" />
                Disconnect
              </ActionHeaderButton>
            </HeaderActionContainer>
          </HeaderWrapper>
          <Suspense
            fallback={
              <LoaderWrapper>
                <Loader size="xl" color={theme.common.primary} />
              </LoaderWrapper>
            }
          >
            <AccountBalances />
          </Suspense>
        </ModalContainer>
      </Modal>
    </ModalWrapper>
  );
};
