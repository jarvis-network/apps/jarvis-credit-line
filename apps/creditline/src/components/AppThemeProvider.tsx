import React, { ReactNode, useEffect } from 'react';
import { ThemeProvider } from '@jarvisnetwork/ui';

import { useAppTheme } from '@/state/theme/hooks';

export const AppThemeProvider = ({ children }: { children: ReactNode }) => {
  const theme = useAppTheme();

  useEffect(() => {
    Array.from(document.body.classList).forEach(cls => {
      if (cls.startsWith('theme-')) {
        document.body.classList.remove(cls);
      }
    });
    document.body.classList.add(`theme-${theme}`);
  }, [theme]);

  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};
