import React, { useState } from 'react';
import Image from 'next/image';
import { Button, Checkbox, styled } from '@jarvisnetwork/ui';

import ReactMarkdown from 'react-markdown';

import {
  ContentWrapper,
  ModalContainer,
  ModalHeader,
} from '@jarvisnetwork/toolkit';

import privacyPolicyText from '@/components/Auth//policies/pp.md';
import termsOfServiceText from '@/components/Auth/policies/tos.md';

interface PageProps {
  onNext: () => void;
}
const ModalTitle = styled.div`
  font-size: 20px;
  margin-bottom: 16px;
  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.mobileIndex]}px) {
    margin: 2vh 0;
  }
`;

const ParagraphContainer = styled.div`
  font-size: ≈
  > :first-child {
    margin-bottom: 24px;
  }
  margin-bottom: 24px;
`;

export const ImgContainer = styled.div`
  flex: 1;
  text-align: center;
  margin-bottom: 24px;
  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    justify-content: center;
    align-items: center;
  }
`;
const Submit = styled(Button)`
  text-align: center;
  font-size: 18px;
  :not(:disabled) {
    ${props => (props.theme.name === 'light' ? 'color: black;' : '')}
  }
`;
export const ModalFooter = styled.div`
  text-align: center;
  margin: 30px 0 16px 0;
  > button {
    width: 176px;
    height: 60px;
  }
`;
const ModalLink = styled.a`
  color: ${props => props.theme.common.success};
  font-size: ${props => props.theme.font.sizes.m};
`;
const CheckboxContainer = styled.div`
  color: ${props => props.theme.text.primary};
  display: flex;
  align-items: center;
  font-size: ${props => props.theme.font.sizes.m};
  > label {
    padding: 0px;
    > i {
      width: 24px;
      height: 24px;
      top: 0px;
      border-radius: 8px;
    }
    input:checked ~ i > i {
      display: flex;
      align-items: center;
      justify-content: center;
      visibility: visible;
      width: 22px;
      height: 22px;
    }
  }
`;
export const Welcome: React.FC<PageProps> = ({ onNext }): JSX.Element => {
  const [checked, setChecked] = useState(false);
  const [mode, setMode] = useState<'approve' | 'terms' | 'privacy'>('approve');
  const toggle = () => setChecked(p => !p);
  const approve = (
    <>
      <ImgContainer>
        <Image
          alt="Welcome to Jarvis"
          src="/images/girl-sculpture-3.svg"
          width="100"
          height="100"
        />
      </ImgContainer>
      <ModalTitle>Welcome to Jarvis Money App!</ModalTitle>
      <ParagraphContainer>
        <p>
          Jarvis Money is a platform to open jFIATs credit lines by depositing
          collateral.
        </p>
      </ParagraphContainer>
      <CheckboxContainer>
        <Checkbox checked={checked} onChange={toggle} name="acc" />
        <div>
          I have read the{' '}
          <ModalLink onClick={() => setMode('terms')}>
            terms of service
          </ModalLink>{' '}
          and{' '}
          <ModalLink onClick={() => setMode('privacy')}>
            privacy policy
          </ModalLink>
        </div>
      </CheckboxContainer>
      <ModalFooter>
        <Submit type="primary" disabled={!checked} onClick={onNext}>
          Connect
        </Submit>
      </ModalFooter>
    </>
  );

  const terms = (
    <>
      <ModalHeader title="Terms of Service" onBack={() => setMode('approve')} />
      <ContentWrapper>
        <ReactMarkdown>{termsOfServiceText}</ReactMarkdown>
      </ContentWrapper>
    </>
  );

  const privacy = (
    <>
      <ModalHeader title="Privacy Policy" onBack={() => setMode('approve')} />
      <ContentWrapper>
        <ReactMarkdown>{privacyPolicyText}</ReactMarkdown>
      </ContentWrapper>
    </>
  );

  const Content = {
    approve,
    terms,
    privacy,
  };
  return <ModalContainer>{Content[mode]}</ModalContainer>;
};
