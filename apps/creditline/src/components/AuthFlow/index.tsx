// TODO rework with notification
import React, { useEffect, useState } from 'react';

import { Modal, styled } from '@jarvisnetwork/ui';
import { useAccount } from 'wagmi';
import { useConnectModal } from '@rainbow-me/rainbowkit';

const ModalWrapper = styled.div`
  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    .auth-modal {
      > * {
        height: auto;
        padding-bottom: 30px;
      }
    }
  }
`;

interface PageProps {
  onNext(): void;
  onPrev(): void;
}

type Page = React.FC<PageProps>;

interface Props {
  appName: string;
  closeAuthModal: () => void;
  isAuthModalOpen: boolean;
  Welcome: Page;
}

export const AuthFlow = ({
  appName,
  closeAuthModal,
  isAuthModalOpen,
  Welcome,
}: Props): JSX.Element => {
  const { isConnected } = useAccount();
  const [current, setPage] = useState(0);
  const { openConnectModal } = useConnectModal();
  const pages = [Welcome];
  const Page = pages[current];

  const next = () => {
    if (current === 0) {
      localStorage.setItem(`${appName}/tos-accepted`, 'true');
    }
    setPage(p => p + 1);

    if (current === pages.length - 1) {
      openConnectModal();
    }
  };
  const prev = () => setPage(p => p - 1);

  useEffect(() => {
    if (isConnected) {
      closeAuthModal();
    }
  }, [isConnected]);

  useEffect(() => {
    if (isAuthModalOpen) {
      if (localStorage.getItem(`${appName}/tos-accepted`) === 'true') {
        setPage(1);
        openConnectModal();
        return;
      }
      setPage(0);
    }
  }, [appName, isAuthModalOpen]);

  return (
    <ModalWrapper>
      <Modal
        isOpened={isAuthModalOpen}
        onClose={closeAuthModal}
        overlayStyle={{ zIndex: 3 }}
        overlayClassName="auth-modal"
      >
        {Page && <Page onNext={next} onPrev={prev} />}
      </Modal>
    </ModalWrapper>
  );
};
