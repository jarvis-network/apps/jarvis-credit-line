import { Button, Icon, styled } from '@jarvisnetwork/ui';

const BackButtonContainer = styled.div``;

const BackButtonWrapper = styled(Button)`
  display: flex;
  color: ${props => props.theme.text.primary};
  background: ${props => props.theme.background.primary};
`;

const BackButtonIcon = styled(Icon)`
  margin-right: 5px;
`;

const BackButton = ({
  className,
  onBackButtonClick,
}: {
  className: string;
  onBackButtonClick: () => void;
}) => (
  <BackButtonContainer>
    <BackButtonWrapper
      size="m"
      className={className}
      onClick={onBackButtonClick}
    >
      <BackButtonIcon icon="backArrowIcon" /> Back
    </BackButtonWrapper>
  </BackButtonContainer>
);

export default BackButton;
