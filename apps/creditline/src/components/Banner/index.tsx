import React from 'react';
import styled from '@emotion/styled';

interface BannerProps {
  message: string;
  type: 'Danger' | 'Warning' | 'success';
  // eslint-disable-next-line react/require-default-props
  emoji?: string;
}

const BannerContainer = styled.div<{ type: 'Danger' | 'Warning' | 'success' }>`
  text-align: center;
  padding: 5px 0;
  color: black;
  background-color: ${props =>
    props.type === 'Danger'
      ? 'red'
      : props.type === 'Warning'
      ? 'orange'
      : 'green'};
`;

export const Banner = ({ message, type, emoji }: BannerProps) => (
  <BannerContainer type={`${type}`}>
    {emoji && <span style={{ paddingRight: '12px' }}>{emoji}</span>}
    {message}
  </BannerContainer>
);
