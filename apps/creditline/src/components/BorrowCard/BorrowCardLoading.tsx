import React from 'react';

import {
  BorrowButton,
  Card,
  CardContent,
  CreditLineFlag,
  Title,
  Text,
} from './styles';

export const BorrowCardLoading = () => (
  <Card>
    <CreditLineFlag
      flag=""
      className="placeholder placeholder-img"
      size="big"
    />
    <CardContent>
      <Title className="placeholder placeholder-m" />

      <Text className="placeholder placeholder-m" />

      <BorrowButton
        type="secondary"
        size="m"
        className="placeholder placeholder-btn"
      />
    </CardContent>
  </Card>
);
