import React from 'react';

import { NativeCurrency, Token } from '@jarvisnetwork/core-sdk';
import { useWeb3React } from '@web3-react/core';
import { useDispatch } from 'react-redux';

import { useAccount } from 'wagmi';

import {
  Card,
  CardContent,
  CreditLineFlag,
  Title,
  Text,
  BorrowButton,
  SignIn,
} from './styles';

import { setSelectedSynthetic } from '@/state/assets/reducer';
import { useToggleAuthenticationModal } from '@/state/app/hooks';

interface BorrowProps {
  token: Token | NativeCurrency;
}

const BorrowCard = ({ token }: BorrowProps) => {
  const dispatch = useDispatch();
  const { address: account } = useAccount();
  const toggleAuthModal = useToggleAuthenticationModal();
  return (
    <Card>
      <CreditLineFlag flag={token.symbol} size="big" />
      <CardContent>
        <Title>{token.symbol}</Title>
        <Text>{token.label}</Text>
        {account ? (
          <BorrowButton
            type="secondary"
            size="m"
            onClick={() => dispatch(setSelectedSynthetic(token.symbol))}
          >
            Borrow
          </BorrowButton>
        ) : (
          <SignIn type="primary" size="m" onClick={toggleAuthModal}>
            Sign In
          </SignIn>
        )}
      </CardContent>
    </Card>
  );
};

export default BorrowCard;
