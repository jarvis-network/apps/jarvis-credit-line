import { Button, Flag, styled } from '@jarvisnetwork/ui';

export const Card = styled.div`
  padding: 24px;
  width: 343px;
  height: 315px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background: ${props => props.theme.background.secondary};
  border: 1px solid ${props => props.theme.border.primary};
  border-radius: ${props => props.theme.borderRadius.l};
  transition: box-shadow 0.3s;
  box-shadow: ${props => props.theme.shadow.small};

  &:hover {
    box-shadow: ${props => props.theme.shadow.hover};
  }

  @media screen and (min-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.tabletIndex]}px) {
    width: 224px;
    height: 280px;
  }
`;

export const CreditLineFlag = styled(Flag)`
  width: 120px;
  height: 120px;
  @media screen and (min-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.tabletIndex]}px) {
    width: 100px;
    height: 100px;
  }
`;

export const BorrowButton = styled(Button)`
  width: 176px;
  height: 50px;
  border-radius: 12px;
  text-align: center;
  margin-top: 16px;
  &:hover {
    background: ${props => props.theme.button.secondary.active.background};
  }
`;

export const SignIn = styled(Button)`
  width: 176px;
  height: 50px;
  border-radius: 12px;
  text-align: center;
  margin-top: 16px;
  font-weight: 500;
`;

export const CardContent = styled.div`
  margin-top: 16px;
`;

export const Title = styled.h3`
  height: 24px;
  text-align: center;
  margin-bottom: 4px;
`;

export const Text = styled.p`
  height: 16px;
  text-align: center;
  font: normal normal medium 17px/26px 'Krub';
  color: #939393;
`;
