import React from 'react';
import { useDispatch } from 'react-redux';

import { OnDesktop } from '@jarvisnetwork/ui';

import { PANEL_TYPE } from '@/enums';

import CreateLoanCard from '@/components/LoanManagementCard/CreateLoan';
import { RenderPageProps } from '@/components/LoanManagementCard/interfaces';
import {
  DesktopView,
  Layout,
  LayoutLoan,
  LoanOverviewDesktop,
  LayoutWidget,
  WidgetContainer,
  ExchangeCardWrapper,
  CardWrapper,
} from '@/components/BorrowPage/styles';
import LoanOverview from '@/components/LoanOverview';
import BorrowCard from '@/components/LoanManagementCard/Borrow';
import HomeLoanCard from '@/components/LoanManagementCard/Home';
import RepayPanel from '@/components/LoanManagementCard/Repay';
import DepositPanel from '@/components/LoanManagementCard/Deposit';
import WithdrawPanel from '@/components/LoanManagementCard/Withdraw';
import ClosePanel from '@/components/LoanManagementCard/Close';
import { useUserCurrentLoanPosition } from '@/state/loan/hooks';
import { backgroundMap, themeType } from '@/config/app';
import { setPanelType } from '@/state/app/reducer';
import { usePanelType } from '@/state/app/hooks';
import { useAppTheme } from '@/state/theme/hooks';

const PANEL_TITLE: { [key in PANEL_TYPE]: string } = {
  [PANEL_TYPE.HOME]: 'Manage Loan',
  [PANEL_TYPE.CREATE]: 'Create Loan',
  [PANEL_TYPE.CLOSE]: 'Close Loan',
  [PANEL_TYPE.REPAY]: 'Repay',
  [PANEL_TYPE.BORROW_MORE]: 'Borrow',
  [PANEL_TYPE.REMOVE]: 'Remove Collateral',
  [PANEL_TYPE.ADD_COLLATERAL]: 'Add Collateral',
};

const BorrowPageDesktopView = ({
  redirectTo,
}: RenderPageProps): JSX.Element => {
  const dispatch = useDispatch();
  const theme = useAppTheme();
  const bgUrl = backgroundMap[theme as themeType];
  const panelType = usePanelType();
  const loanPositionData = useUserCurrentLoanPosition();

  const renderCardFromType = () => {
    if (panelType === PANEL_TYPE.CREATE || !loanPositionData) {
      return <CreateLoanCard />;
    }
    if (panelType === PANEL_TYPE.BORROW_MORE) {
      return <BorrowCard />;
    }
    if (panelType === PANEL_TYPE.REPAY) {
      return <RepayPanel />;
    }
    if (panelType === PANEL_TYPE.ADD_COLLATERAL) {
      return <DepositPanel />;
    }
    if (panelType === PANEL_TYPE.REMOVE) {
      return <WithdrawPanel />;
    }
    if (panelType === PANEL_TYPE.CLOSE) {
      return <ClosePanel />;
    }
    return <HomeLoanCard />;
  };

  return (
    <OnDesktop>
      <DesktopView>
        <Layout image={bgUrl}>
          <LayoutLoan>
            <LoanOverviewDesktop>
              <LoanOverview
                showBackButton
                showLoanHeading
                onBackClick={redirectTo}
              />
            </LoanOverviewDesktop>
          </LayoutLoan>
          <LayoutWidget>
            <WidgetContainer>
              <CardWrapper
                title={PANEL_TITLE[panelType]}
                onBack={
                  panelType !== PANEL_TYPE.HOME &&
                  panelType !== PANEL_TYPE.CREATE
                    ? () => dispatch(setPanelType(PANEL_TYPE.HOME))
                    : undefined
                }
                pointer={false}
              >
                <ExchangeCardWrapper
                  className="exchange-card"
                  title={PANEL_TITLE[panelType]}
                >
                  {renderCardFromType()}
                </ExchangeCardWrapper>
              </CardWrapper>
            </WidgetContainer>
          </LayoutWidget>
        </Layout>
      </DesktopView>
    </OnDesktop>
  );
};

export default BorrowPageDesktopView;
