import { OnMobile, styled } from '@jarvisnetwork/ui';
import React from 'react';

import BackButton from '@/components/BackButton';
import LoanHeading from '@/components/LoanHeading';
import CreateLoanCard from '@/components/LoanManagementCard/CreateLoan';
import { RenderPageProps } from '@/components/LoanManagementCard/interfaces';
import {
  ExchangeCardWrapper,
  BackButtonWrapper,
} from '@/components/BorrowPage/styles';

const MobileView = styled.div`
  display: none;

  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.mobileIndex]}px) {
    display: block;
    height: 100%;
  }
`;

const LoanHeadingWrapper = styled.div`
  margin-left: 16px;
`;

const BorrowPageMobileView = ({ redirectTo }: RenderPageProps): JSX.Element => (
  <OnMobile>
    <MobileView>
      <BackButtonWrapper>
        <BackButton className="back-button" onBackButtonClick={redirectTo} />
      </BackButtonWrapper>

      <LoanHeadingWrapper>
        <LoanHeading className="heading" />
      </LoanHeadingWrapper>
      <ExchangeCardWrapper>
        <CreateLoanCard />
      </ExchangeCardWrapper>
    </MobileView>
  </OnMobile>
);

export default BorrowPageMobileView;
