import { OnTablet } from '@jarvisnetwork/ui';
import React from 'react';

import BackButton from '@/components/BackButton';
import CreateLoanCard from '@/components/LoanManagementCard/CreateLoan';
import { RenderPageProps } from '@/components/LoanManagementCard/interfaces';
import {
  BackButtonWrapper,
  TabletView,
  TabletViewCard,
  LoanOverviewTablet,
} from '@/components/BorrowPage/styles';
import LoanOverview from '@/components/LoanOverview';

const BorrowPageTabletView = ({ redirectTo }: RenderPageProps): JSX.Element => (
  <OnTablet>
    <TabletView>
      <BackButtonWrapper>
        <BackButton className="back-button" onBackButtonClick={redirectTo} />
      </BackButtonWrapper>

      <TabletViewCard
        tabs={[
          {
            title: 'Overview',
            content: (
              <LoanOverviewTablet>
                <LoanOverview
                  showBackButton={false}
                  showLoanHeading
                  onBackClick={redirectTo}
                />
              </LoanOverviewTablet>
            ),
          },
          {
            title: 'Manage',
            content: <CreateLoanCard />,
          },
        ]}
      />
    </TabletView>
  </OnTablet>
);

export default BorrowPageTabletView;
