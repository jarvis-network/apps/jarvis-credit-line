import { Background, Card, CardTabs, styled } from '@jarvisnetwork/ui';

export const Layout = styled(Background)`
  min-height: 100%;
  background-position: center !important;
  display: flex;
  justify-content: center;
  align-items: center;

  @media screen and (max-height: 850px) {
    height: max-content;
  }
`;
export const LayoutLoan = styled.div`
  background: ${props => props.theme.background.primary};
  height: max-content;
  min-height: 100vh;
  width: 100%;
  align-self: baseline;
  padding-left: 20vw;
  padding-top: 90px;
  padding-bottom: 20px;
  z-index: 2;

  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.tabletIndex]}px) {
    display: none;
  }
`;
export const LayoutWidget = styled.div`
  height: 100%;
  width: 90%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const WidgetContainer = styled.div``;
export const Container = styled.div`
  height: 100%;
`;
export const DesktopView = styled.div`
  display: none;
  height: 100%;

  @media screen and (min-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    display: block;
  }
`;
export const TabletView = styled.div`
  display: none;

  @media screen and (min-width: ${props =>
      props.theme.rwd.breakpoints[
        props.theme.rwd.mobileIndex
      ]}px) and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    display: block;
  }

  .back-button {
    margin-top: 84px;
    margin-left: 12px;
    margin-bottom: 19px;
    background: transparent;
  }
`;
export const TabletViewCard = styled(CardTabs)``;
export const ExchangeCardWrapper = styled.div`
  height: 100%;

  .exchange-card__wrapper {
    padding-top: 32px;
    height: 100%;
  }
`;
export const BackButtonWrapper = styled.div`
  .back-button {
    margin-top: 24px;
    margin-bottom: 19px;
    background: transparent;
  }
`;
export const CardWrapper = styled(Card)`
  width: 360px;
  overflow: hidden;
  border-radius: ${props => props.theme.borderRadius.m};
  i {
    width: ${props => props.theme.font.sizes.l} !important;
  }
  button:first-child {
    background: transparent;
  }
  @media screen and (min-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    min-height: 645px;
  }
`;
export const LoanOverviewTablet = styled.div`
  padding: 20px;
`;
export const LoanOverviewDesktop = styled.div`
  padding: 19px 40px 0 0;
`;
