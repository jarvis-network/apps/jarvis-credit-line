import React from 'react';

import {
  BorrowButton,
  BorrowPart,
  Card,
  CardContent,
  CreditLineFlag,
  Value,
  Text,
} from './styles';

export const CardLoading = () => (
  <Card isBalancePositive={false}>
    <CreditLineFlag flag="" className="placeholder placeholder-img" />
    <CardContent>
      <div>
        <Text>Your balance</Text>
        <Value className="placeholder placeholder-m" />
      </div>
      <BorrowPart>
        <Text>Maximum you can borrow</Text>
        <Value className="placeholder placeholder-m" />
      </BorrowPart>
      <BorrowPart>
        <Text>Borrowed / Available</Text>
        <Value className="placeholder placeholder-m" />
      </BorrowPart>
      <BorrowButton
        type="secondary"
        size="m"
        className="placeholder placeholder-btn"
      />
    </CardContent>
  </Card>
);
