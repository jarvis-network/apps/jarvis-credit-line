import React, { Suspense, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useRouter } from 'next/router';

import { FixBigNumber, Token } from '@jarvisnetwork/core-sdk';

import { Tooltip, Icon } from '@jarvisnetwork/ui';

import { usePriceFeed } from '../PriceFeedProvider';

import { useUserCurrencyBalance } from '../UserCurrenciesBalanceProvider';

import { CardLoading } from './CardLoading';

import {
  BorrowButton,
  BorrowPart,
  Card,
  CardContent,
  CreditLineFlag,
  Value,
  Text,
  TooltipContainer,
  APRInfo,
} from './styles';

import { Collateral, PANEL_TYPE } from '@/enums';

import { setPanelType } from '@/state/app/reducer';
import { CreditLineWithUserLoanPositionData } from '@/hooks/useActiveLoan';
import { DECIMAL_DIGITS_COUNT } from '@/config/app';

import { numberFormat } from '@/utils/formatNumber';

interface CollateralProps {
  selectedSynthetic: Token;
  collateral: Token;
  creditLineData: CreditLineWithUserLoanPositionData;
}

const getToolTipInfo = (collateral: string) => {
  switch (collateral) {
    case Collateral.wstETH:
      return {
        APR: '4.5%',
        link: 'https://lido.fi/ethereum',
        message: (
          <div>
            <div>
              wstETH is a wrapped version of liquid staked ETH (stETH) by Lido.
            </div>
            <br />
            <div>
              stETH are tokens representing staked ETH through Lido. stETH
              earned daily staking rewards in ETH.
            </div>
            <br />
            <div>Click on the `?` for more information</div>
          </div>
        ),
      };
    default:
      return null;
  }
};

const CollateralCard = ({
  selectedSynthetic,
  collateral,
  creditLineData,
}: CollateralProps) => {
  const dispatch = useDispatch();
  const router = useRouter();

  const { balance } = useUserCurrencyBalance(collateral?.symbol);
  const price = usePriceFeed(creditLineData?.pair);

  const [borrowData, setBorrowData] = useState({
    maxToBorrow: '0',
    borrowed: '0',
    available: '0',
  });
  const tooltipData = getToolTipInfo(collateral.symbol);

  useMemo(() => {
    let maxToBorrow = '0';
    let borrowed = '0';
    let available = '0';
    if (price && balance && creditLineData.capMint) {
      maxToBorrow = numberFormat(
        balance
          .subtract(balance.multiply(creditLineData.feePercentage))
          .multiply(FixBigNumber.toWei(1).divide(price))
          .divide(creditLineData.collateralRequirement)
          .toFixed(DECIMAL_DIGITS_COUNT),
        false,
      );

      borrowed = numberFormat(
        creditLineData.globalPosition.totalBorrowed.toFixed(
          DECIMAL_DIGITS_COUNT,
        ),
        false,
      );
      const maxMint = creditLineData.capMint;
      if (maxMint.greaterThan(0)) {
        available = numberFormat(
          maxMint
            .subtract(creditLineData.globalPosition.totalBorrowed)
            .toFixed(DECIMAL_DIGITS_COUNT),
          false,
        );
      }
    }
    setBorrowData({ maxToBorrow, borrowed, available });
  }, [price, creditLineData, balance]);

  const isBalancePositive = balance && balance.greaterThan(0);

  const redirectToBorrow = () => {
    const url = `/loans/${collateral.address}/${selectedSynthetic.address}`;
    dispatch(
      setPanelType(
        creditLineData?.tokensAmount.greaterThan(0)
          ? PANEL_TYPE.HOME
          : PANEL_TYPE.CREATE,
      ),
    );
    router.push(url, undefined, { shallow: true });
  };

  return (
    <Suspense fallback={<CardLoading />}>
      <Card isBalancePositive={isBalancePositive}>
        {tooltipData?.APR && (
          <APRInfo>
            +{tooltipData.APR}
            <TooltipContainer
              onClick={() => window.open(`${tooltipData.link}`, '_blank')}
            >
              <Tooltip tooltip={tooltipData.message} position="left">
                <Icon icon="infoIcon" color="#4FC8F4" />
              </Tooltip>
            </TooltipContainer>
          </APRInfo>
        )}
        <CreditLineFlag flag={collateral.symbol} />
        <CardContent>
          <div>
            <Text>Your balance</Text>
            <Value>
              <strong>
                {isBalancePositive
                  ? `${numberFormat(
                      balance.toFixed(DECIMAL_DIGITS_COUNT),
                      false,
                      0,
                      5,
                    )} ${collateral.symbol}`
                  : `No ${collateral.symbol} in your wallet`}
              </strong>
            </Value>
          </div>
          <BorrowPart>
            <Text>Maximum you can borrow</Text>
            <Value>
              <strong>
                {borrowData.maxToBorrow} {selectedSynthetic.symbol}
              </strong>
            </Value>
          </BorrowPart>
          <BorrowPart>
            <Text>Borrowed / Available</Text>
            <Value>
              <strong>
                {borrowData.borrowed} /{' '}
                {borrowData.available === '∞' ? '∞' : borrowData.available}
              </strong>
            </Value>
          </BorrowPart>
          <BorrowButton
            type="secondary"
            size="m"
            onClick={() => redirectToBorrow()}
            disabled={!isBalancePositive || +borrowData.available === 0}
          >
            {creditLineData?.tokensAmount.greaterThan(0) ? 'Manage' : 'Choose'}
          </BorrowButton>
        </CardContent>
      </Card>
    </Suspense>
  );
};
export default CollateralCard;
