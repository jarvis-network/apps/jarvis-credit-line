import { Button, Flag, styled, Tooltip } from '@jarvisnetwork/ui';

export const Card = styled.div<{ isBalancePositive: boolean }>`
  position: relative;
  opacity: ${props => (props.isBalancePositive ? '1' : '0.6')};
  padding: 24px;
  width: 343px;
  height: 385px;
  display: flex;
  text-align: center;
  flex-direction: column;
  align-items: center;
  background: ${props => props.theme.background.secondary};
  border: 1px solid ${props => props.theme.border.primary};
  border-radius: ${props => props.theme.borderRadius.l};
  transition: box-shadow 0.3s;
  box-shadow: ${props => props.theme.shadow.small};

  &:hover {
    box-shadow: ${props => props.theme.shadow.hover};
  }
  @media screen and (min-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.tabletIndex]}px) {
    width: 260px;
    height: 375px;
  }
`;

export const CreditLineFlag = styled(Flag)`
  width: 60px;
  height: 60px;
  @media (min-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.tabletIndex]}px) {
    width: 50px;
    height: 50px;
  }
`;

export const BorrowButton = styled(Button)`
  border: none;
  width: 176px;
  height: 50px;
  border-radius: 12px;
  text-align: center;
  margin-top: 16px;
  &:disabled {
    background: ${props =>
      props.theme.button.secondary.disabled.background} !important;
    color: ${props => props.theme.button.secondary.disabled.text};
  }
  &:hover {
    background: ${props => props.theme.button.secondary.active.background};
  }
`;

export const CardContent = styled.div`
  margin-top: 24px;
`;
export const BorrowPart = styled.div`
  margin-top: 20px;
`;
export const Value = styled.div`
  margin-bottom: 8px;
  width: 100%;
  span {
    font: normal normal medium 17px/26px 'Krub';
    color: #939393;
  }
`;

export const Text = styled.p`
  font: normal normal medium 17px/26px 'Krub';
  color: #939393;
  margin-bottom: 4px;
`;

export const APRInfo = styled.div`
  height: 30px;
  padding: 4px 8px;
  display: flex;
  gap: 4px;
  align-items: center;
  border-radius: 4px;
  background: ${props => props.theme.common.primary};
  color: ${props => props.theme.common.black};
  position: absolute;
  top: 8px;
  right: 8px;
`;

export const TooltipContainer = styled.div`
  padding-top: 4px !important;
`;
