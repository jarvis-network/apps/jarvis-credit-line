import React, { createContext, useContext, useMemo } from 'react';

import { useRouter } from 'next/router';

import { Token, Currency } from '@jarvisnetwork/core-sdk';

import { useNetworkCurrencies } from '../NetworkCurrenciesProvider';

type CurrentPair = {
  collateralToken: Currency | undefined | null;
  syntheticToken: Token | undefined | null;
};

const CurrentPairContext = createContext<CurrentPair>({
  collateralToken: undefined,
  syntheticToken: undefined,
});

export const CurrentPairProvider = ({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}) => {
  const router = useRouter();
  const networkCurrencies = useNetworkCurrencies();

  const currentPair: CurrentPair = useMemo(() => {
    const collateralTokenAddress = router.query.tokenA;
    const syntheticTokenAddress = router.query.tokenB;
    if (!networkCurrencies || !collateralTokenAddress || !syntheticTokenAddress)
      return {
        collateralToken: undefined,
        syntheticToken: undefined,
      };

    const collateralToken = networkCurrencies.find(
      token => (token as Token).address === collateralTokenAddress,
    );

    const syntheticToken = networkCurrencies.find(
      token => (token as Token).address === syntheticTokenAddress,
    ) as Token;

    if (!collateralToken || !syntheticToken)
      return {
        collateralToken: null,
        syntheticToken: null,
      };
    return {
      collateralToken,
      syntheticToken,
    };
  }, [router, networkCurrencies]);
  return (
    <CurrentPairContext.Provider value={currentPair}>
      {children}
    </CurrentPairContext.Provider>
  );
};

export const useCurrentPair = () => useContext(CurrentPairContext);
