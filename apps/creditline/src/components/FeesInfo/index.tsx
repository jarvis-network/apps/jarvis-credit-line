import React from 'react';

import { styled, CardFooter, Tooltip, Icon } from '@jarvisnetwork/ui';

import { useCurrentPair } from '../CurrentPairProvider';
import { useNativeCurrency } from '../NetworkCurrenciesProvider';

const CardFooterWrapper = styled(CardFooter)``;

const CardFooterContent = styled.div`
  padding: 16px;
  background: ${params => params.theme.background.secondary};
  border-bottom-left-radius: ${params => params.theme.borderRadius.m};
  border-bottom-right-radius: ${params => params.theme.borderRadius.m};
  color: ${params => params.theme.text.medium};
  display: flex;
  flex-direction: column;
  gap: 16px;

  .row {
    display: flex;
    justify-content: space-between;
  }

  .price {
    color: ${params => params.theme.text.secondary};
  }
`;

const TextFeeWrapper = styled.div`
  display: flex;
  gap: 8px;
  align-items: center;
`;
export const FeesInfo = React.memo(
  ({
    networkFee,
    baseFees = undefined,
    feePercentageInfo = '0.5',
  }: {
    networkFee: string;
    // eslint-disable-next-line react/require-default-props
    baseFees?: string;
    // eslint-disable-next-line react/require-default-props
    feePercentageInfo?: string;
  }) => {
    const nativeCurrency = useNativeCurrency();
    const { collateralToken } = useCurrentPair();

    return (
      <CardFooterWrapper>
        <CardFooterContent>
          {baseFees?.length && (
            <div className="row">
              <TextFeeWrapper>
                Origination Fee
                <Tooltip
                  tooltip={`When you borrow, you pay a ${' '}${feePercentageInfo}% one-time fee which is paid out off your collateral`}
                  position="right"
                >
                  <Icon icon="infoIcon" color="#4FC8F4" />
                </Tooltip>
              </TextFeeWrapper>
              <span className="price">
                {baseFees} {collateralToken?.symbol}
              </span>
            </div>
          )}
          <div className="row">
            <span>Network Fee</span>
            <span className="price">
              {networkFee} {nativeCurrency?.symbol}
            </span>
          </div>
        </CardFooterContent>
      </CardFooterWrapper>
    );
  },
);
