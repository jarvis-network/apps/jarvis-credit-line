import React from 'react';

import { LoadingModalBody } from '@jarvisnetwork/ui';

import { DepositRow, DepositRowCurrency } from '../styles';

import { ApproveTokenTransactionContentModalProps } from '@/components/LoadingTransactionModal/interfaces';

export const ApproveTokenTransactionContentModal = ({
  tokenSymbol,
}: ApproveTokenTransactionContentModalProps) => (
  <LoadingModalBody>
    <DepositRow>
      <DepositRowCurrency>
        <span className="value">
          Approving Credit Line contract to transfer your {tokenSymbol}
        </span>
      </DepositRowCurrency>
    </DepositRow>
  </LoadingModalBody>
);
