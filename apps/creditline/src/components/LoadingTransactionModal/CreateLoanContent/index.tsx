import { Flag, LoadingModalBody } from '@jarvisnetwork/ui';
import React from 'react';

import { DepositRow, DepositRowCurrency } from '../styles';

import { CreateLoanTransactionContentModalProps } from '@/components/LoadingTransactionModal/interfaces';
import { numberFormat } from '@/utils/formatNumber';
import { DECIMAL_DIGITS_COUNT } from '@/config/app';

export const CreateLoanModalContent = ({
  collateral,
  synthetic,
}: CreateLoanTransactionContentModalProps) => (
  <LoadingModalBody>
    <DepositRow>
      <p>You deposit</p>

      <DepositRowCurrency>
        <span className="currency">
          <Flag flag={collateral.symbol} /> {collateral.symbol}
        </span>
        <span className="value">
          {numberFormat(collateral.value, false, 0, DECIMAL_DIGITS_COUNT)}
        </span>
      </DepositRowCurrency>
    </DepositRow>

    <DepositRow>
      <p>You borrow</p>

      <DepositRowCurrency>
        <span className="currency">
          <Flag flag={synthetic.symbol} /> {synthetic.symbol}
        </span>
        <span className="value">
          {numberFormat(synthetic.value, false, 0, DECIMAL_DIGITS_COUNT)}
        </span>
      </DepositRowCurrency>
    </DepositRow>
  </LoadingModalBody>
);
