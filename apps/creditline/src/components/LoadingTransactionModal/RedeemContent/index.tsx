import { Flag, LoadingModalBody } from '@jarvisnetwork/ui';
import React from 'react';

import { DepositRow, DepositRowCurrency } from '../styles';

import { RedeemTransactionContentModalProps } from '@/components/LoadingTransactionModal/interfaces';
import { DECIMAL_DIGITS_COUNT } from '@/config/app';
import { numberFormat } from '@/utils/formatNumber';

export const RedeemModalContent = ({
  synthetic,
  collateral,
}: RedeemTransactionContentModalProps) => (
  <LoadingModalBody>
    <DepositRow>
      <p>You repay</p>
      <DepositRowCurrency>
        <span className="currency">
          <Flag flag={synthetic.symbol} /> {synthetic.symbol}
        </span>
        <span className="value">
          {numberFormat(synthetic.value, false, 0, DECIMAL_DIGITS_COUNT)}
        </span>
      </DepositRowCurrency>
    </DepositRow>

    <DepositRow>
      <p>You get back</p>
      <DepositRowCurrency>
        <span className="currency">
          <Flag flag={collateral.symbol} /> {collateral.symbol}
        </span>
        <span className="value">
          {numberFormat(collateral.value, false, 0, DECIMAL_DIGITS_COUNT)}
        </span>
      </DepositRowCurrency>
    </DepositRow>
  </LoadingModalBody>
);
