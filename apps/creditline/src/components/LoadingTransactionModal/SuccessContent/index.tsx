import { LoadingModalBody } from '@jarvisnetwork/ui';

import { SuccessTransactionContentModalProps } from '../interfaces';
import { DepositRow } from '../styles';

export const SuccessModalContent = ({
  type,
}: SuccessTransactionContentModalProps) => (
  <LoadingModalBody>
    <DepositRow>
      <p>{type} transaction validated, you can now close this window</p>
    </DepositRow>
  </LoadingModalBody>
);
