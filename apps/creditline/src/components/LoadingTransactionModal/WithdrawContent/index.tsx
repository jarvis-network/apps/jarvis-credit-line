import { Flag, LoadingModalBody } from '@jarvisnetwork/ui';
import React from 'react';

import { DepositRow, DepositRowCurrency } from '../styles';

import { WithdrawTransactionContentModalProps } from '@/components/LoadingTransactionModal/interfaces';
import { DECIMAL_DIGITS_COUNT } from '@/config/app';
import { numberFormat } from '@/utils/formatNumber';

export const WithdrawModalContent = ({
  collateral,
}: WithdrawTransactionContentModalProps) => (
  <LoadingModalBody>
    <DepositRow>
      <p>You withdraw</p>
      <DepositRowCurrency>
        <span className="currency">
          <Flag flag={collateral.symbol} /> {collateral.symbol}
        </span>
        <span className="value">
          {numberFormat(collateral.value, false, 0, DECIMAL_DIGITS_COUNT)}
        </span>
      </DepositRowCurrency>
    </DepositRow>
  </LoadingModalBody>
);
