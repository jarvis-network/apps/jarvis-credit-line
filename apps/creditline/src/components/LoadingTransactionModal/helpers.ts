import { LoadingModalStatus } from '@jarvisnetwork/ui';

const messages = {
  success: 'Your transaction was completed',
  error: 'Your transaction has failed',
  cancel: 'User denied transaction signature',
  loading: 'Waiting for confirmation',
};

export const getMessage = (status: LoadingModalStatus) => {
  if (status === LoadingModalStatus.cancelled) {
    return messages.cancel;
  }

  if (status === LoadingModalStatus.error) {
    return messages.error;
  }

  if (status === LoadingModalStatus.success) {
    return messages.success;
  }

  return messages.loading;
};
