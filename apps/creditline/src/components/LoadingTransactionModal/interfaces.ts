import { TransactionType } from '@/types';

export interface SuccessTransactionContentModalProps {
  type: TransactionType;
}
export interface ModalCurrencyProps {
  value: string;
  symbol: string;
}

export interface CreateLoanTransactionContentModalProps {
  collateral: ModalCurrencyProps;
  synthetic: ModalCurrencyProps;
}

export interface ApproveTokenTransactionContentModalProps {
  tokenSymbol: string;
}

export interface RedeemTransactionContentModalProps {
  collateral: ModalCurrencyProps;
  synthetic: ModalCurrencyProps;
}

export interface CloseLoanTransactionContentModalProps {
  collateral: ModalCurrencyProps;
  synthetic: ModalCurrencyProps;
}

export interface RepayTransactionContentModalProps {
  synthetic: ModalCurrencyProps;
}
export interface DepositTransactionContentModalProps {
  collateral: ModalCurrencyProps;
}
export interface WithdrawTransactionContentModalProps {
  collateral: ModalCurrencyProps;
}
