import { styled } from '@jarvisnetwork/ui';

export const DepositRow = styled.div`
  display: flex;
  flex-direction: column;
  gap: 12px;

  :first-child {
    margin-bottom: 17px;
  }
`;

export const DepositRowCurrency = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  .currency {
    display: flex;
    justify-content: center;
    align-items: center;
    gap: 5px;
  }
`;
