import { Flag } from '@jarvisnetwork/ui';

import { HeaderLine, LoanIconsWrapper } from '../LoanOverview/styles';

import { useCurrentPair } from '../CurrentPairProvider';

import { PANEL_TYPE } from '@/enums';

import { useReduxSelector } from '@/state/useReduxSelector';

interface LoanHeadingProps {
  className: string;
}

const LoanHeading = ({ className }: LoanHeadingProps) => {
  const { collateralToken, syntheticToken } = useCurrentPair();
  const { panelType } = useReduxSelector(state => state.app);
  const isLoanCreation = panelType === PANEL_TYPE.CREATE;
  return (
    <HeaderLine className={className}>
      <h2 className="title">
        {isLoanCreation ? 'Create your loan' : 'Your loan'}
      </h2>
      <LoanIconsWrapper>
        {collateralToken ? <Flag flag={collateralToken.symbol} /> : ''}
        {syntheticToken ? (
          <Flag className="debt-currency" flag={syntheticToken.symbol} />
        ) : (
          ''
        )}
      </LoanIconsWrapper>
    </HeaderLine>
  );
};

export default LoanHeading;
