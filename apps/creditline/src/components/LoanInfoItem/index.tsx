import { Icon, Tooltip } from '@jarvisnetwork/ui';

import { IconKeys } from '@jarvisnetwork/ui/dist/Icon/files';

import {
  IconWrapper,
  LoanInfoItemHeading,
  LoanInfoItemStyled,
  LoanInfoItemTitle,
} from '@/components/LoanOverview/styles';

const LoanInfoItem = ({
  tooltip,
  title,
  icon,
  children,
}: {
  tooltip: string;
  title: string;
  icon: IconKeys;
  children: JSX.Element | JSX.Element[];
}) => (
  <LoanInfoItemStyled>
    <LoanInfoItemHeading>
      <LoanInfoItemTitle>
        <IconWrapper icon={icon} /> {title}
      </LoanInfoItemTitle>

      <Tooltip tooltip={tooltip} position="right">
        <Icon icon="infoIcon" color="#4FC8F4" />
      </Tooltip>
    </LoanInfoItemHeading>
    <div className="data">{children}</div>
  </LoanInfoItemStyled>
);

export default LoanInfoItem;
