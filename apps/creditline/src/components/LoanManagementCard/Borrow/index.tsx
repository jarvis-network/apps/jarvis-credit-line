import React, { useMemo, useState } from 'react';

import { FixBigNumber } from '@jarvisnetwork/core-sdk';
import { Button, Flag } from '@jarvisnetwork/ui';

import {
  ContentWrapper,
  FieldSet,
  InputActionsWrapper,
  ErrorMessage,
  SubmitButton,
  ErrorMessageWrapper,
  LoanPreviewPositionInfoWrapper,
} from '../styles';
import { FormFields } from '../interfaces';

import { TransactionType } from '@/types';

import { ERROR_TYPE, PANEL_FLOW_STEPS, PANEL_TYPE } from '@/enums';
import WelcomeCard from '@/components/WelcomeCard';
import { NumericalInput } from '@/components/NumericalInput';

import {
  maxToBorrow,
  shouldSkipWelcomeCard,
  calcOriginationFee,
  validateAmount,
} from '@/utils/loanManagementHelpers';
import { useCurrentPairBalances } from '@/hooks/useCurrentPairBalances';
import { useCurrentLoanData } from '@/hooks/useCurrentLoanData';
import { ApprovalState, useApproval } from '@/hooks/useApproval';
import { DECIMAL_DIGITS_COUNT } from '@/config/app';

import { useCreateLoan } from '@/hooks/useCreateLoan';

import { LoanRatioAndLiquidationPriceInfo } from '@/components/LoanRatioAndLiquidationPriceInfo';
import { PriceInfo } from '@/components/PriceInfo';
import { FeesInfo } from '@/components/FeesInfo';
import { useNativeCurrency } from '@/components/NetworkCurrenciesProvider';
import { numberFormat } from '@/utils/formatNumber';

const initialFormState: FormFields = {
  collateral: {
    value: '',
  },
  synthetic: {
    value: '',
    isMaxAvailable: false,
  },
  isDisabled: true,
  errors: null,
  calculations: null,
};

const BorrowCard = () => {
  const { collateralBalance } = useCurrentPairBalances();
  const nativeCurrency = useNativeCurrency();
  const {
    currentPairData,
    pricePair,
    syntheticToken,
    collateralToken,
    loanPositionData,
    creditLineContract,
  } = useCurrentLoanData();

  const [step, setStep] = useState(PANEL_FLOW_STEPS.WELCOME);
  const [skipWelcome, setSkipWelcome] = useState(true);
  const [collateralToggleChecked, setCollateralToggleChecked] = useState(false);

  const [formData, setFormData] = useState<FormFields>(initialFormState);
  const [networkFee, setNetworkFee] = useState('');
  const [baseFees, setBaseFees] = useState<string | undefined>();

  const { approvalState, approvalEstimatedNetworkFee, approve } = useApproval(
    formData.collateral.value
      ? FixBigNumber.toWei(formData.collateral.value, collateralToken?.decimals)
      : loanPositionData?.collateralAmount,
    collateralToken,
    currentPairData?.address,
  );

  const { borrowEstimatedNetworkFee, create } = useCreateLoan(
    formData.collateral.value
      ? FixBigNumber.toWei(formData.collateral.value, collateralToken?.decimals)
      : FixBigNumber.toWei(0, collateralToken?.decimals),
    formData.synthetic.value
      ? FixBigNumber.toWei(formData.synthetic.value)
      : undefined,
    creditLineContract,
    TransactionType.BORROW,
  );

  const shouldDisplay = +formData.synthetic.value > 0;

  useMemo(async () => {
    if (!currentPairData) return;
    let fee;
    if (
      approvalState !== ApprovalState.APPROVED &&
      approvalState !== ApprovalState.UNKNOWN
    ) {
      fee = await approvalEstimatedNetworkFee();
      setNetworkFee(fee);
      return;
    }
    if (shouldDisplay && !!pricePair) {
      fee = await borrowEstimatedNetworkFee();
      setNetworkFee(fee);
      const calcCollateralAmount = FixBigNumber.toWei(
        formData.synthetic.value,
      ).multiply(pricePair);
      const baseFeesValue = calcOriginationFee(
        calcCollateralAmount,
        currentPairData.feePercentage,
      )?.toFixed(DECIMAL_DIGITS_COUNT);
      setBaseFees(baseFeesValue);
    }
  }, [
    approvalState,
    formData.synthetic.value,
    formData.collateral.value,
    pricePair,
  ]);

  useMemo(() => {
    if (
      !collateralToken ||
      !loanPositionData ||
      !collateralBalance ||
      !pricePair ||
      !currentPairData
    )
      return;

    const { minSponsorTokens, collateralRequirement, feePercentage } =
      currentPairData;

    const collateralAmount =
      formData.collateral.value.length && collateralToggleChecked
        ? loanPositionData.collateralAmount.add(
            FixBigNumber.toWei(
              formData.collateral.value,
              collateralToken.decimals,
            ),
          )
        : loanPositionData.collateralAmount;

    const syntheticAmount = formData.synthetic.value.length
      ? loanPositionData.syntheticAmount.add(
          FixBigNumber.toWei(formData.synthetic.value),
        )
      : loanPositionData.syntheticAmount;
    const collateralAmountAfterFees = collateralAmount?.subtract(
      syntheticAmount?.multiply(pricePair).multiply(feePercentage) ?? 0,
    );

    // eslint-disable-next-line prefer-const
    let { calculations, errors } = validateAmount(
      collateralAmountAfterFees,
      syntheticAmount,
      minSponsorTokens,
      collateralRequirement,
      pricePair,
      PANEL_TYPE.BORROW_MORE,
    );
    if (
      !errors &&
      formData.collateral.value &&
      collateralBalance?.balance.lessThan(formData.collateral.value)
    ) {
      errors = {
        type: ERROR_TYPE.BALANCE,
        message: 'Insufficient balance',
        controls: ['collateral'],
      };
    }

    setFormData({
      ...formData,
      isDisabled:
        collateralToggleChecked && !formData.collateral.value ? true : !!errors,
      errors,
      calculations,
    });
  }, [pricePair, formData.collateral.value, formData.synthetic.value]);

  useMemo(() => {
    if (syntheticToken?.symbol && collateralToken?.symbol) {
      setSkipWelcome(
        shouldSkipWelcomeCard(
          step,
          PANEL_TYPE.BORROW_MORE,
          syntheticToken.symbol,
          collateralToken.symbol,
        ),
      );
    }
  }, [syntheticToken, collateralToken, step]);

  const renderWelcomeCard = () => {
    let doNotShowAgain = false;

    const welcomeParams = {
      onNext: () => {
        if (doNotShowAgain) {
          localStorage.setItem(
            `jarvis-creditLine/${syntheticToken?.symbol}${collateralToken?.symbol}/welcome-${PANEL_TYPE.BORROW_MORE}-accepted`,
            `true`,
          );
        }
        setStep(PANEL_FLOW_STEPS.FORM);
      },
      onCheckboxChange: () => {
        doNotShowAgain = !doNotShowAgain;
      },
      content: () => (
        <>
          <strong>Borrow</strong>
          <p>You can draw more from your Credit Line.</p>
          <br />
          <p>
            This action decreases your Health Ratio (HR), increasing your risk
            of liquidation if your HR drops below the liquidation threshold.
          </p>
          <br />
          <p>
            You can also choose to add more collateral at the same time to
            lessen the decrease of your HR.
          </p>
        </>
      ),
    };
    return <WelcomeCard {...welcomeParams} />;
  };

  if (
    !syntheticToken ||
    !collateralToken ||
    !loanPositionData ||
    !currentPairData ||
    !pricePair ||
    !collateralBalance ||
    !nativeCurrency
  )
    return <ContentWrapper />;

  const onUserCollateralInput = (userInput: string) => {
    setFormData({
      ...formData,
      collateral: {
        value: userInput,
      },
    });
  };

  const onCollateralMaxClick = () => {
    setFormData({
      ...formData,
      collateral: {
        value: collateralBalance.balance.toFixed(DECIMAL_DIGITS_COUNT),
      },
    });
  };

  const getMaxToBorrow = () => {
    const collateralInput =
      formData.collateral.value.length && collateralToggleChecked
        ? loanPositionData.collateralAmount.add(
            FixBigNumber.toWei(
              formData.collateral.value,
              collateralToken.decimals,
            ),
          )
        : loanPositionData.collateralAmount;
    const test = maxToBorrow(
      collateralInput,
      currentPairData.feePercentage,
      pricePair,
      currentPairData.collateralRequirement,
    );

    const max = test?.subtract(loanPositionData.syntheticAmount);
    return max?.greaterThan(0) ? max?.toFixed(DECIMAL_DIGITS_COUNT) : '0';
  };

  const onUserSyntheticInput = (userInput: string) => {
    setFormData({
      ...formData,
      synthetic: {
        ...formData.synthetic,
        value: userInput,
      },
    });
  };

  const handleMaxToBorrow = () => {
    const maxSyntheticAmount = getMaxToBorrow();
    if (!maxSyntheticAmount) return;
    setFormData({
      ...formData,
      synthetic: {
        ...formData.synthetic,
        value: maxSyntheticAmount,
      },
    });
  };

  const isApproval =
    approvalState !== ApprovalState.APPROVED &&
    approvalState !== ApprovalState.UNKNOWN;

  return (
    <>
      <ContentWrapper>
        {skipWelcome ? (
          <>
            <FieldSet>
              <NumericalInput
                label="Borrow"
                placeholder="0"
                maxValueInfo={`Max: ${numberFormat(
                  getMaxToBorrow(),
                  false,
                  0,
                  DECIMAL_DIGITS_COUNT,
                )}`}
                isError={formData.errors?.controls.includes('borrow')}
                value={formData.synthetic.value}
                onUserInput={onUserSyntheticInput}
                suffix={
                  <InputActionsWrapper>
                    <Button size="s" onClick={handleMaxToBorrow}>
                      MAX
                    </Button>
                    <Flag flag={syntheticToken.symbol} />{' '}
                    {syntheticToken.symbol}
                  </InputActionsWrapper>
                }
              />
              <NumericalInput
                label="Add Collateral"
                placeholder="0"
                balanceValueInfo={`balance: ${numberFormat(
                  collateralBalance?.balance.toExact(),
                  false,
                  0,
                  DECIMAL_DIGITS_COUNT,
                )}`}
                isError={formData.errors?.controls.includes('collateral')}
                value={formData.collateral.value}
                onUserInput={onUserCollateralInput}
                suffix={
                  <InputActionsWrapper>
                    {collateralToggleChecked && (
                      <Button size="s" onClick={onCollateralMaxClick}>
                        MAX
                      </Button>
                    )}
                    <Flag flag={collateralToken.symbol} />
                    {collateralToken.symbol}
                  </InputActionsWrapper>
                }
                onToggleChange={() => {
                  setCollateralToggleChecked(!collateralToggleChecked);
                  onUserCollateralInput('');
                }}
                inputHidePlaceHolder=" ℹ️ You can also add more collateral at the same time."
              />
            </FieldSet>
            <PriceInfo />
            <ErrorMessageWrapper>
              {formData.errors?.message ? (
                <ErrorMessage>{formData.errors?.message}</ErrorMessage>
              ) : (
                ''
              )}
            </ErrorMessageWrapper>
            <LoanPreviewPositionInfoWrapper>
              {shouldDisplay && formData.calculations && (
                <LoanRatioAndLiquidationPriceInfo
                  loanCalculations={formData.calculations}
                  liquidationTitle={`New ${currentPairData.inversePair} liquidation price`}
                  healthRatioTitle="New health ratio"
                />
              )}
            </LoanPreviewPositionInfoWrapper>
            <SubmitButton
              type="success"
              size="s"
              disabled={formData.isDisabled || !shouldDisplay}
              onClick={() =>
                isApproval
                  ? approve()
                  : create().then(() => setFormData(initialFormState))
              }
            >
              {isApproval ? 'Approve' : 'Borrow'}
            </SubmitButton>
          </>
        ) : (
          renderWelcomeCard()
        )}
      </ContentWrapper>

      {shouldDisplay && skipWelcome && !formData.errors && (
        <FeesInfo
          networkFee={networkFee}
          baseFees={isApproval ? undefined : baseFees}
          feePercentageInfo={currentPairData.feePercentage
            .multiply(100)
            .toFixed(DECIMAL_DIGITS_COUNT)}
        />
      )}
    </>
  );
};
export default BorrowCard;
