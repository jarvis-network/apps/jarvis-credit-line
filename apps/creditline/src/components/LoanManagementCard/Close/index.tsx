import React, { useMemo, useState } from 'react';

import { Flag, styled } from '@jarvisnetwork/ui';

import { TransactionType } from 'types/transactions';

import { ContentWrapper, SubmitButton } from '../styles';

import { PANEL_FLOW_STEPS, PANEL_TYPE } from '@/enums';

import WelcomeCard from '@/components/WelcomeCard';
import { shouldSkipWelcomeCard } from '@/utils/loanManagementHelpers';
import { useCurrentLoanData } from '@/hooks/useCurrentLoanData';

import { useRedeemLoan } from '@/hooks/useRedeemLoan';

import { ApprovalState, useApproval } from '@/hooks/useApproval';

import { FeesInfo } from '@/components/FeesInfo';
import { useNativeCurrency } from '@/components/NetworkCurrenciesProvider';
import { DECIMAL_DIGITS_COUNT } from '@/config/app';
import { numberFormat } from '@/utils/formatNumber';

const DescriptionContent = styled.div`
  margin-top: 32px;
`;
const CloseLoanInfoWrapper = styled.div`
  margin-top: 80%;
`;
const LoanInfoRow = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 16px 0px;
`;

const Divider = styled.hr`
  position: absolute;
  height: 1px;
  background-color: ${props => props.theme.gray.gray200};
  border: none;
  left: 0;
  width: 100%;
`;
const ValueInfo = styled.div`
  display: grid;
  grid-template-columns: 1fr 30px 40px;
  grid-gap: 8px;
  align-items: center;
  padding-right: 8px;
`;

const SymbolWrapper = styled.div`
  text-align: end;
`;

const ClosePanel = () => {
  const nativeCurrency = useNativeCurrency();
  const {
    currentPairData,
    pricePair,
    syntheticToken,
    collateralToken,
    loanPositionData,
    creditLineContract,
  } = useCurrentLoanData();

  const [step, setStep] = useState(PANEL_FLOW_STEPS.WELCOME);
  const [skipWelcome, setSkipWelcome] = useState(true);
  const [networkFee, setNetworkFee] = useState('');

  const { approvalState, approvalEstimatedNetworkFee, approve } = useApproval(
    loanPositionData?.syntheticAmount,
    syntheticToken,
    currentPairData?.address,
  );

  const { redeemEstimatedNetworkFee, redeem } = useRedeemLoan(
    loanPositionData?.syntheticAmount,
    loanPositionData?.collateralAmount,
    creditLineContract,
    TransactionType.CLOSE,
  );

  useMemo(async () => {
    let fee;
    if (
      !loanPositionData?.collateralAmount.greaterThan(0) ||
      !loanPositionData?.syntheticAmount.greaterThan(0)
    )
      return;
    if (
      approvalState !== ApprovalState.APPROVED &&
      approvalState !== ApprovalState.UNKNOWN
    ) {
      fee = await approvalEstimatedNetworkFee();
      setNetworkFee(fee);
      return;
    }

    fee = await redeemEstimatedNetworkFee();
    setNetworkFee(fee);
  }, [approvalState, pricePair]);

  useMemo(() => {
    if (syntheticToken?.symbol && collateralToken?.symbol) {
      setSkipWelcome(
        shouldSkipWelcomeCard(
          step,
          PANEL_TYPE.CLOSE,
          syntheticToken.symbol,
          collateralToken.symbol,
        ),
      );
    }
  }, [syntheticToken, collateralToken, step]);

  const renderWelcomeCard = () => {
    let doNotShowAgain = false;

    const welcomeParams = {
      onNext: () => {
        if (doNotShowAgain) {
          localStorage.setItem(
            `jarvis-creditLine/${syntheticToken?.symbol}${collateralToken?.symbol}/welcome-${PANEL_TYPE.CLOSE}-accepted`,
            `true`,
          );
        }
        setStep(PANEL_FLOW_STEPS.FORM);
      },
      onCheckboxChange: () => {
        doNotShowAgain = !doNotShowAgain;
      },
      content: () => (
        <>
          <strong>Close Loan</strong>
          <p>
            You can fully repay your loan and remove all your collateral to
            close your Credit Line.
          </p>
        </>
      ),
    };
    return <WelcomeCard {...welcomeParams} />;
  };

  if (
    !syntheticToken ||
    !collateralToken ||
    !loanPositionData ||
    !pricePair ||
    !nativeCurrency
  )
    return <ContentWrapper />;

  const isApproval =
    approvalState !== ApprovalState.APPROVED &&
    approvalState !== ApprovalState.UNKNOWN;

  return (
    <>
      <ContentWrapper>
        {skipWelcome ? (
          <>
            <DescriptionContent>
              When you close your loan, all borrowed jFIAT will be repaid and
              your collateral will be withdrawn.
            </DescriptionContent>
            <CloseLoanInfoWrapper>
              <LoanInfoRow>
                <div>You Will Get:</div>
                <ValueInfo>
                  {numberFormat(
                    loanPositionData?.collateralAmount.toExact(),
                    false,
                    0,
                    DECIMAL_DIGITS_COUNT,
                  )}
                  <Flag flag={collateralToken.symbol} />{' '}
                  <SymbolWrapper>{collateralToken.symbol}</SymbolWrapper>
                </ValueInfo>
              </LoanInfoRow>
              <Divider />
              <LoanInfoRow>
                <div>You Pay Back:</div>
                <ValueInfo>
                  {numberFormat(
                    loanPositionData?.syntheticAmount.toExact(),
                    false,
                    0,
                    DECIMAL_DIGITS_COUNT,
                  )}
                  <Flag flag={syntheticToken.symbol} />
                  <SymbolWrapper>{syntheticToken.symbol}</SymbolWrapper>
                </ValueInfo>
              </LoanInfoRow>
            </CloseLoanInfoWrapper>
            <SubmitButton
              type="success"
              size="s"
              disabled={false}
              onClick={() => (isApproval ? approve() : redeem())}
            >
              {isApproval ? 'Approve' : 'Close Loan'}
            </SubmitButton>
          </>
        ) : (
          renderWelcomeCard()
        )}
      </ContentWrapper>
      {skipWelcome && networkFee !== '' && <FeesInfo networkFee={networkFee} />}
    </>
  );
};

export default ClosePanel;
