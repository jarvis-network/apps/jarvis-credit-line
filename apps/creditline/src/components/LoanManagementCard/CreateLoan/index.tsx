import React, { useMemo, useState } from 'react';

import { FixBigNumber } from '@jarvisnetwork/core-sdk';

import { Button, Flag } from '@jarvisnetwork/ui';

import {
  ContentWrapper,
  FieldSet,
  InputActionsWrapper,
  ErrorMessage,
  SubmitButton,
  ErrorMessageWrapper,
  LoanPreviewPositionInfoWrapper,
} from '../styles';
import { FormFields } from '../interfaces';

import { TransactionType } from '@/types';

import { ERROR_TYPE, PANEL_FLOW_STEPS, PANEL_TYPE } from '@/enums';
import { NumericalInput } from '@/components/NumericalInput';

import WelcomeCard from '@/components/WelcomeCard';
import {
  maxToBorrow,
  shouldSkipWelcomeCard,
  calcOriginationFee,
  validateAmount,
} from '@/utils/loanManagementHelpers';
import { useCurrentPairBalances } from '@/hooks/useCurrentPairBalances';
import { ApprovalState, useApproval } from '@/hooks/useApproval';

import { DECIMAL_DIGITS_COUNT } from '@/config/app';
import { useCurrentLoanData } from '@/hooks/useCurrentLoanData';
import { useCreateLoan } from '@/hooks/useCreateLoan';
import { LoanRatioAndLiquidationPriceInfo } from '@/components/LoanRatioAndLiquidationPriceInfo';

import { PriceInfo } from '@/components/PriceInfo';
import { useNativeCurrency } from '@/components/NetworkCurrenciesProvider';
import { FeesInfo } from '@/components/FeesInfo';
import { numberFormat } from '@/utils/formatNumber';

const initialFormState: FormFields = {
  collateral: {
    value: '',
  },
  synthetic: {
    value: '',
    isMaxAvailable: false,
  },
  isDisabled: true,
  errors: null,
  calculations: null,
};

const CreateLoanCard = () => {
  const { collateralBalance } = useCurrentPairBalances();
  const nativeCurrency = useNativeCurrency();
  const {
    currentPairData,
    pricePair,
    syntheticToken,
    collateralToken,
    creditLineContract,
  } = useCurrentLoanData();
  const [step, setStep] = useState(PANEL_FLOW_STEPS.WELCOME);
  const [skipWelcome, setSkipWelcome] = useState(true);
  const [formData, setFormData] = useState<FormFields>(initialFormState);
  const [networkFee, setNetworkFee] = useState('');
  const [baseFees, setBaseFees] = useState<string | undefined>();
  const { approvalState, approvalEstimatedNetworkFee, approve } = useApproval(
    formData.collateral.value
      ? FixBigNumber.toWei(formData.collateral.value, collateralToken?.decimals)
      : undefined,
    collateralToken,
    currentPairData?.address,
  );
  const { createEstimatedNetworkFee, create } = useCreateLoan(
    formData.collateral.value
      ? FixBigNumber.toWei(formData.collateral.value, collateralToken?.decimals)
      : undefined,
    formData.synthetic.value
      ? FixBigNumber.toWei(formData.synthetic.value)
      : undefined,
    creditLineContract,
    TransactionType.CREATE_LOAN,
  );

  useMemo(() => {
    if (syntheticToken?.symbol && collateralToken?.symbol) {
      setSkipWelcome(
        shouldSkipWelcomeCard(
          step,
          PANEL_TYPE.CREATE,
          syntheticToken.symbol,
          collateralToken.symbol,
        ),
      );
    }
  }, [syntheticToken, collateralToken, step]);

  const renderWelcomeCard = () => {
    let doNotShowAgain = false;
    const welcomeParams = {
      onNext: () => {
        if (doNotShowAgain) {
          localStorage.setItem(
            `jarvis-creditLine/${syntheticToken?.symbol}${collateralToken?.symbol}/welcome-${PANEL_TYPE.CREATE}-accepted`,
            `true`,
          );
        }
        setStep(PANEL_FLOW_STEPS.FORM);
      },
      onCheckboxChange: () => {
        doNotShowAgain = !doNotShowAgain;
      },
      content: () => (
        <>
          <strong>Welcome</strong>
          <p>
            To open your {syntheticToken?.symbol} Credit Line, you need to
            deposit collateral in {collateralToken?.symbol}.
          </p>

          <br />
          <p>
            Make sure that your Health Ratio (Value of your Collateral / Value
            of your debt) remains above{' '}
            <strong>
              {currentPairData?.collateralRequirement.multiply(100).toFixed(2)}
              %.{' '}
            </strong>
          </p>
          <br />
          <p>
            If your Health Ratio falls below{' '}
            <strong>
              {currentPairData?.collateralRequirement.multiply(100).toFixed(2)}%
            </strong>
            , your loan will be liquidated, and you will pay a penality fee of{' '}
            <strong>
              {currentPairData?.collateralRequirement
                .multiply(100)
                .subtract(100)
                .multiply(currentPairData?.liquidationReward)
                .toFixed(2)}
              %
            </strong>{' '}
            (
            <strong>
              {FixBigNumber.toWei(100)
                .subtract(
                  currentPairData!.collateralRequirement
                    .multiply(100)
                    .subtract(100)
                    .multiply(currentPairData!.liquidationReward),
                )
                .toFixed(2)}
              %
            </strong>{' '}
            of your collateral will be seized).
          </p>
        </>
      ),
    };
    return <WelcomeCard {...welcomeParams} />;
  };

  useMemo(async () => {
    if (
      !currentPairData ||
      !(+formData.collateral.value > 0) ||
      !(+formData.synthetic.value > 0)
    )
      return;
    let fee;
    if (
      approvalState !== ApprovalState.APPROVED &&
      approvalState !== ApprovalState.UNKNOWN
    ) {
      fee = await approvalEstimatedNetworkFee();
      setNetworkFee(fee);
      return;
    }
    if (
      !!pricePair &&
      currentPairData.minSponsorTokens.lessThanOrEqualTo(
        formData.synthetic.value,
      )
    ) {
      fee = await createEstimatedNetworkFee();
      setNetworkFee(fee);
      const calcCollateralAmount = FixBigNumber.toWei(
        formData.synthetic.value,
      ).multiply(pricePair);
      const baseFeesValue = calcOriginationFee(
        calcCollateralAmount,
        currentPairData.feePercentage,
      )?.toFixed(5);
      setBaseFees(baseFeesValue);
    }
  }, [
    approvalState,
    formData.collateral.value,
    formData.synthetic.value,
    pricePair,
  ]);

  useMemo(() => {
    if (
      !collateralToken ||
      !collateralBalance ||
      !currentPairData ||
      !pricePair
    )
      return;

    const { minSponsorTokens, collateralRequirement, feePercentage } =
      currentPairData;
    const collateralAmount = formData.collateral.value.length
      ? FixBigNumber.toWei(formData.collateral.value, collateralToken.decimals)
      : undefined;

    const syntheticAmount = formData.synthetic.value.length
      ? FixBigNumber.toWei(formData.synthetic.value)
      : undefined;

    const collateralAmountAfterFees = collateralAmount?.subtract(
      syntheticAmount?.multiply(pricePair).multiply(feePercentage) ?? 0,
    );

    // eslint-disable-next-line prefer-const
    let { calculations, errors } = validateAmount(
      collateralAmountAfterFees,
      syntheticAmount,
      minSponsorTokens,
      collateralRequirement,
      pricePair,
      PANEL_TYPE.CREATE,
    );

    if (
      !errors &&
      formData.collateral.value &&
      collateralBalance?.balance.lessThan(formData.collateral.value)
    ) {
      errors = {
        type: ERROR_TYPE.BALANCE,
        message: 'Insufficient balance',
        controls: ['collateral'],
      };
    }

    setFormData({
      ...formData,
      isDisabled:
        !formData.collateral.value || !formData.synthetic.value || !!errors,
      errors,
      calculations,
    });
  }, [pricePair, formData.collateral.value, formData.synthetic.value]);

  if (
    !syntheticToken ||
    !collateralToken ||
    !pricePair ||
    !currentPairData ||
    !collateralBalance ||
    !nativeCurrency
  )
    return <ContentWrapper />;

  const onUserCollateralInput = (userInput: string) => {
    setFormData({
      ...formData,
      collateral: {
        value: userInput,
      },
      synthetic: {
        ...formData.synthetic,
        // eslint-disable-next-line no-restricted-globals
        isMaxAvailable: !!userInput && !isNaN(+userInput),
      },
    });
  };
  const onCollateralMaxClick = () => {
    setFormData({
      ...formData,
      collateral: {
        value: collateralBalance.balance.toExact(),
      },
      synthetic: {
        ...formData.synthetic,
        isMaxAvailable: true,
      },
    });
  };
  const getMaxToBorrow = () => {
    const collaralInput = formData.collateral.value.length
      ? FixBigNumber.toWei(formData.collateral.value, collateralToken.decimals)
      : FixBigNumber.toWei(0);
    return maxToBorrow(
      collaralInput,
      currentPairData.feePercentage,
      pricePair,
      currentPairData.collateralRequirement,
    )?.toFixed(DECIMAL_DIGITS_COUNT);
  };
  const onUserSyntheticInput = (userInput: string) => {
    setFormData({
      ...formData,
      synthetic: {
        ...formData.synthetic,
        value: userInput,
      },
    });
  };
  const handleMaxToBorrow = () => {
    const maxSyntheticAmount = getMaxToBorrow();
    if (!maxSyntheticAmount) return;
    setFormData({
      ...formData,
      synthetic: {
        ...formData.synthetic,
        value: maxSyntheticAmount,
      },
    });
  };
  const isApproval =
    approvalState !== ApprovalState.APPROVED &&
    approvalState !== ApprovalState.UNKNOWN;
  return (
    <>
      <ContentWrapper>
        {skipWelcome ? (
          <>
            <FieldSet>
              <NumericalInput
                label="Deposit Collateral"
                placeholder="0"
                balanceValueInfo={`balance: ${numberFormat(
                  collateralBalance?.balance.toExact(),
                  false,
                  0,
                  DECIMAL_DIGITS_COUNT,
                )}`}
                isError={formData.errors?.controls.includes('collateral')}
                value={formData.collateral.value}
                onUserInput={onUserCollateralInput}
                suffix={
                  <InputActionsWrapper>
                    <Button size="s" onClick={onCollateralMaxClick}>
                      MAX
                    </Button>
                    <Flag flag={collateralToken.symbol} />
                    {collateralToken.symbol}
                  </InputActionsWrapper>
                }
              />
              <NumericalInput
                label="Borrow"
                placeholder="0"
                maxValueInfo={`Max: ${numberFormat(
                  getMaxToBorrow(),
                  false,
                  0,
                  DECIMAL_DIGITS_COUNT,
                )}`}
                isError={formData.errors?.controls.includes('borrow')}
                value={formData.synthetic.value}
                onUserInput={onUserSyntheticInput}
                suffix={
                  <InputActionsWrapper>
                    {formData.synthetic.isMaxAvailable ? (
                      <Button size="s" onClick={handleMaxToBorrow}>
                        MAX
                      </Button>
                    ) : (
                      ''
                    )}
                    <Flag flag={syntheticToken.symbol} />
                    {syntheticToken.symbol}
                  </InputActionsWrapper>
                }
              />
            </FieldSet>
            <PriceInfo />
            <ErrorMessageWrapper>
              {formData.errors?.message ? (
                <ErrorMessage>{formData.errors?.message}</ErrorMessage>
              ) : (
                ''
              )}
            </ErrorMessageWrapper>
            <LoanPreviewPositionInfoWrapper>
              {formData.calculations && (
                <LoanRatioAndLiquidationPriceInfo
                  loanCalculations={formData.calculations}
                  liquidationTitle={`${currentPairData.inversePair} liquidation price`}
                  healthRatioTitle="Health Ratio"
                />
              )}
            </LoanPreviewPositionInfoWrapper>
            <SubmitButton
              type="success"
              size="s"
              disabled={formData.isDisabled}
              onClick={() =>
                isApproval
                  ? approve()
                  : create().then(() => setFormData(initialFormState))
              }
            >
              {isApproval ? 'Approve' : 'Borrow'}
            </SubmitButton>
          </>
        ) : (
          renderWelcomeCard()
        )}
      </ContentWrapper>
      {formData.calculations && skipWelcome && !formData.errors && (
        <FeesInfo
          networkFee={networkFee}
          baseFees={baseFees}
          feePercentageInfo={currentPairData.feePercentage
            .multiply(100)
            .toFixed(3)}
        />
      )}
    </>
  );
};

export default CreateLoanCard;
