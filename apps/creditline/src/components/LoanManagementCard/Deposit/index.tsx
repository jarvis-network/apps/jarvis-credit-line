import React, { useMemo, useState } from 'react';

import { FixBigNumber } from '@jarvisnetwork/core-sdk';
import { Button, Flag, styled } from '@jarvisnetwork/ui';

import { SliderInput } from '@jarvisnetwork/toolkit';

import {
  ContentWrapper,
  FieldSet,
  InputActionsWrapper,
  ErrorMessage,
  SubmitButton,
  ErrorMessageWrapper,
  LoanPreviewPositionInfoWrapper,
} from '../styles';
import { FormFields } from '../interfaces';

import { ERROR_TYPE, PANEL_FLOW_STEPS, PANEL_TYPE } from '@/enums';

import WelcomeCard from '@/components/WelcomeCard';

import {
  shouldSkipWelcomeCard,
  validateAmount,
  calcHealthRatio,
  calcCollateralAmountFromRatioWithoutFee,
} from '@/utils/loanManagementHelpers';
import { useCurrentPairBalances } from '@/hooks/useCurrentPairBalances';
import { useCurrentLoanData } from '@/hooks/useCurrentLoanData';
import { ApprovalState, useApproval } from '@/hooks/useApproval';
import { DECIMAL_DIGITS_COUNT } from '@/config/app';
import { NumericalInput } from '@/components/NumericalInput';

import { LoanRatioAndLiquidationPriceInfo } from '@/components/LoanRatioAndLiquidationPriceInfo';
import { useDepositLoan } from '@/hooks/useDepositLoan';
import { useNativeCurrency } from '@/components/NetworkCurrenciesProvider';
import { FeesInfo } from '@/components/FeesInfo';
import { numberFormat } from '@/utils/formatNumber';

const initialFormState: FormFields = {
  collateral: {
    value: '',
  },
  synthetic: {
    value: '',
    isMaxAvailable: false,
  },
  isDisabled: true,
  errors: null,
  calculations: null,
};

const SliderWrapper = styled.div`
  margin-top: 32px;
  margin-bottom: 8px;
`;

const CollateralValue = styled.div`
  font-size: ${props => props.theme.font.sizes.l};
  display: flex;
  justify-content: space-between;
  margin-bottom: 16px;
`;

const SliderInfoValue = styled.div`
  font-size: ${props => props.theme.font.sizes.s};
  display: flex;
  justify-content: space-between;
  margin-top: 16px;
`;

const DepositPanel = () => {
  const { collateralBalance, syntheticBalance } = useCurrentPairBalances();
  const nativeCurrency = useNativeCurrency();
  const {
    currentPairData,
    pricePair,
    syntheticToken,
    collateralToken,
    loanPositionData,
    creditLineContract,
  } = useCurrentLoanData();

  const [step, setStep] = useState(PANEL_FLOW_STEPS.WELCOME);
  const [skipWelcome, setSkipWelcome] = useState(true);

  const [formData, setFormData] = useState<FormFields>(initialFormState);
  const [networkFee, setNetworkFee] = useState('');
  const { approvalState, approvalEstimatedNetworkFee, approve } = useApproval(
    formData.collateral.value
      ? FixBigNumber.toWei(formData.collateral.value, collateralToken?.decimals)
      : undefined,
    collateralToken,
    currentPairData?.address,
  );
  const { depositEstimatedNetworkFee, deposit } = useDepositLoan(
    formData.collateral.value
      ? FixBigNumber.toWei(formData.collateral.value, collateralToken?.decimals)
      : undefined,
    creditLineContract,
  );

  useMemo(async () => {
    let fee;
    if (!formData.collateral.value) return;
    if (
      approvalState !== ApprovalState.APPROVED &&
      approvalState !== ApprovalState.UNKNOWN
    ) {
      fee = await approvalEstimatedNetworkFee();
      setNetworkFee(fee);
      return;
    }
    fee = await depositEstimatedNetworkFee();
    setNetworkFee(fee);
  }, [approvalState, pricePair]);

  useMemo(() => {
    if (
      !collateralToken ||
      !loanPositionData ||
      !collateralBalance ||
      !currentPairData ||
      !pricePair
    )
      return;

    const collateralAmount =
      +formData.collateral.value > 0
        ? FixBigNumber.toWei(
            formData.collateral.value,
            collateralToken.decimals,
          ).add(loanPositionData.collateralAmount)
        : undefined;

    const { syntheticAmount } = loanPositionData;

    // eslint-disable-next-line prefer-const
    let { calculations, errors } = validateAmount(
      collateralAmount,
      syntheticAmount,
      currentPairData?.minSponsorTokens,
      currentPairData?.collateralRequirement,
      pricePair,
      PANEL_TYPE.ADD_COLLATERAL,
    );

    if (
      formData.collateral.value &&
      collateralBalance?.balance.lessThan(formData.collateral.value)
    ) {
      errors = {
        type: ERROR_TYPE.BALANCE,
        message: `Insufficient collateral balance`,
        controls: ['collateral'],
      };
    }

    setFormData({
      ...formData,
      isDisabled: !!errors || !collateralAmount,
      errors,
      calculations,
    });
  }, [
    pricePair,
    formData.collateral.value,
    formData.synthetic.value,
    currentPairData,
  ]);

  useMemo(() => {
    if (syntheticToken?.symbol && collateralToken?.symbol) {
      setSkipWelcome(
        shouldSkipWelcomeCard(
          step,
          PANEL_TYPE.ADD_COLLATERAL,
          syntheticToken.symbol,
          collateralToken.symbol,
        ),
      );
    }
  }, [syntheticToken, collateralToken, step]);

  const renderWelcomeCard = () => {
    let doNotShowAgain = false;

    const welcomeParams = {
      onNext: () => {
        if (doNotShowAgain) {
          localStorage.setItem(
            `jarvis-creditLine/${syntheticToken?.symbol}${collateralToken?.symbol}/welcome-${PANEL_TYPE.ADD_COLLATERAL}-accepted`,
            `true`,
          );
        }
        setStep(PANEL_FLOW_STEPS.FORM);
      },
      onCheckboxChange: () => {
        doNotShowAgain = !doNotShowAgain;
      },
      content: () => (
        <>
          <strong>Add more collateral</strong>
          <p>You can add more collateral to your Credit Line.</p>
          <br />
          <p>
            This action increases your Health Ratio, reducing the risk of
            liquidation. It also allows you to draw more jFIATs from your Credit
            Line.
          </p>
        </>
      ),
    };
    return <WelcomeCard {...welcomeParams} />;
  };

  if (
    !syntheticToken ||
    !collateralToken ||
    !loanPositionData ||
    !pricePair ||
    !collateralBalance ||
    !syntheticBalance ||
    !nativeCurrency
  )
    return <ContentWrapper />;

  const onUserCollateralInput = (userInput: string) => {
    setFormData({
      ...formData,
      collateral: {
        value: userInput,
      },
    });
  };

  const onCollateralMaxClick = () => {
    setFormData({
      ...formData,
      collateral: {
        ...formData.collateral,
        value: collateralBalance.balance.toExact(DECIMAL_DIGITS_COUNT),
      },
    });
  };

  const getCurrentHealthRatio = () =>
    calcHealthRatio(
      loanPositionData?.collateralAmount,
      loanPositionData?.syntheticAmount,
      pricePair,
    );

  const getMaxHealthRatio = () =>
    calcHealthRatio(
      loanPositionData?.collateralAmount.add(collateralBalance.balance),
      loanPositionData?.syntheticAmount,
      pricePair,
    );
  const handleHealthRatioSlider = (newHealthRatio: string) => {
    const totalCollateralAmount = calcCollateralAmountFromRatioWithoutFee(
      FixBigNumber.toWei(newHealthRatio),
      loanPositionData?.syntheticAmount,
      pricePair,
    );
    const inputCollateralAmount = totalCollateralAmount!.subtract(
      loanPositionData.collateralAmount,
    );
    setFormData({
      ...formData,
      collateral: {
        ...formData.collateral,
        value: inputCollateralAmount.greaterThan(0)
          ? inputCollateralAmount.toExact(DECIMAL_DIGITS_COUNT)
          : '',
      },
    });
  };
  const isApproval =
    approvalState !== ApprovalState.APPROVED &&
    approvalState !== ApprovalState.UNKNOWN;

  return (
    <>
      <ContentWrapper>
        {skipWelcome ? (
          <>
            <FieldSet>
              <NumericalInput
                label="Add"
                placeholder="0"
                isError={formData.errors?.controls.includes('collateral')}
                value={formData.collateral.value}
                onUserInput={onUserCollateralInput}
                suffix={
                  <InputActionsWrapper>
                    <Button size="s" onClick={onCollateralMaxClick}>
                      MAX
                    </Button>
                    <Flag flag={collateralToken.symbol} />{' '}
                    {collateralToken.symbol}
                  </InputActionsWrapper>
                }
                balanceValueInfo={`balance: ${numberFormat(
                  collateralBalance.balance.toExact(),
                  false,
                  0,
                  DECIMAL_DIGITS_COUNT,
                )}`}
              />
            </FieldSet>
            <SliderWrapper>
              <CollateralValue>
                <div>Health Ratio</div>
                <div>
                  {formData.calculations?.healthRatio?.toExact(2) ??
                    getCurrentHealthRatio()?.toExact(2)}
                  %
                </div>
              </CollateralValue>
              <SliderInput
                minInput={getCurrentHealthRatio()?.toExact(2) ?? '0'}
                maxInput={getMaxHealthRatio()?.toExact(2) ?? '100'}
                slideValue={
                  formData.calculations?.healthRatio?.toFixed(1) ?? '0'
                }
                onSlide={val => handleHealthRatioSlider(val.toString())}
              />
              <SliderInfoValue>
                <div>Current: {getCurrentHealthRatio()?.toExact(2)}%</div>
                <div>Max: {getMaxHealthRatio()?.toExact(2)}%</div>
              </SliderInfoValue>
            </SliderWrapper>
            <ErrorMessageWrapper style={{ marginBottom: '35px' }}>
              {formData.errors ? (
                <ErrorMessage>{formData.errors?.message}</ErrorMessage>
              ) : (
                ''
              )}
            </ErrorMessageWrapper>
            <LoanPreviewPositionInfoWrapper>
              {formData.calculations && (
                <LoanRatioAndLiquidationPriceInfo
                  loanCalculations={formData.calculations}
                  liquidationTitle={`New ${currentPairData?.inversePair} liquidation price`}
                  healthRatioTitle="New health ratio"
                />
              )}
            </LoanPreviewPositionInfoWrapper>
            <SubmitButton
              type="success"
              size="s"
              disabled={formData.isDisabled}
              onClick={() =>
                isApproval
                  ? approve()
                  : deposit().then(() => setFormData(initialFormState))
              }
            >
              {isApproval ? 'Approve' : 'Deposit'}
            </SubmitButton>
          </>
        ) : (
          renderWelcomeCard()
        )}
      </ContentWrapper>
      {+formData.collateral.value > 0 && skipWelcome && !formData.errors && (
        <FeesInfo networkFee={networkFee} />
      )}
    </>
  );
};

export default DepositPanel;
