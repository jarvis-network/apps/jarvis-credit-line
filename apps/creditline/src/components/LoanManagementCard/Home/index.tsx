import { CardButton, styled } from '@jarvisnetwork/ui';

import { useDispatch } from 'react-redux';

import { ContentWrapper } from '../styles';

import { PANEL_TYPE } from '@/enums';

import { setPanelType } from '@/state/app/reducer';
import { useCurrentLoanData } from '@/hooks/useCurrentLoanData';
import { useCurrentPairBalances } from '@/hooks/useCurrentPairBalances';

const SectionWrapper = styled.div`
  margin-top: 32px;
  gap: 32px;
  display: flex;
  flex-direction: column;
`;

const Section = styled.div`
  display: flex;
  flex-direction: column;
  gap: 8px;
`;

const SubTitle = styled.div``;

const ManagementType = styled(CardButton)`
  justify-content: start;
  padding: 9px 0px 9px 20px;
  width: 100%;
  border: 1px solid ${props => props.theme.border.secondary};
  border-radius: ${props => props.theme.borderRadius.xs};
  background: ${props => props.theme.background.secondary};

  .left {
    margin-right: 20px;
    button {
      background: ${props => props.theme.background.secondary};
      padding: none;
      height: auto;
      width: auto;
    }
  }
`;

const HomeLoanCard = () => {
  const dispatch = useDispatch();
  const { loanPositionData } = useCurrentLoanData();
  const { syntheticBalance } = useCurrentPairBalances();

  const canCloseLoan =
    !!syntheticBalance?.balance &&
    !!loanPositionData?.syntheticAmount &&
    !!loanPositionData?.collateralAmount &&
    loanPositionData?.syntheticAmount.lessThanOrEqualTo(
      syntheticBalance?.balance,
    );

  return (
    <ContentWrapper>
      <SectionWrapper>
        <Section>
          <SubTitle>Debt</SubTitle>
          <ManagementType
            title="Borrow"
            subtitle="Borrow more jFIAT"
            leftButtonIcon="borrowIcon"
            onClick={() => dispatch(setPanelType(PANEL_TYPE.BORROW_MORE))}
          />
          <ManagementType
            title="Repay"
            subtitle="Burn borrowed jFIAT"
            leftButtonIcon="repayIcon"
            onClick={() => dispatch(setPanelType(PANEL_TYPE.REPAY))}
          />
          {canCloseLoan && (
            <ManagementType
              title="Close"
              subtitle="Burn 100% borrowed jFIAT"
              leftButtonIcon="closeCircleIcon"
              onClick={() => dispatch(setPanelType(PANEL_TYPE.CLOSE))}
            />
          )}
        </Section>
        <Section>
          <SubTitle>Collateral</SubTitle>
          <ManagementType
            title="Add"
            subtitle="Add more collateral"
            leftButtonIcon="addCollateralIcon"
            onClick={() => dispatch(setPanelType(PANEL_TYPE.ADD_COLLATERAL))}
          />
          <ManagementType
            size="s"
            title="Remove"
            subtitle="Withdraw collateral"
            leftButtonIcon="removeCollateralIcon"
            onClick={() => dispatch(setPanelType(PANEL_TYPE.REMOVE))}
          />
        </Section>
      </SectionWrapper>
    </ContentWrapper>
  );
};
export default HomeLoanCard;
