import React, { useMemo, useState } from 'react';

import { FixBigNumber } from '@jarvisnetwork/core-sdk';
import { Button, Flag } from '@jarvisnetwork/ui';

import { TransactionType } from 'types/transactions';

import {
  ContentWrapper,
  FieldSet,
  InputActionsWrapper,
  ErrorMessage,
  SubmitButton,
  ErrorMessageWrapper,
  LoanPreviewPositionInfoWrapper,
} from '../styles';
import { FormFields } from '../interfaces';

import { NumericalInput } from '@/components/NumericalInput';

import { ERROR_TYPE, PANEL_FLOW_STEPS, PANEL_TYPE } from '@/enums';
import WelcomeCard from '@/components/WelcomeCard';

import {
  shouldSkipWelcomeCard,
  validateAmount,
} from '@/utils/loanManagementHelpers';
import { useCurrentPairBalances } from '@/hooks/useCurrentPairBalances';
import { useCurrentLoanData } from '@/hooks/useCurrentLoanData';
import { ApprovalState, useApproval } from '@/hooks/useApproval';
import { DECIMAL_DIGITS_COUNT } from '@/config/app';

import { LoanRatioAndLiquidationPriceInfo } from '@/components/LoanRatioAndLiquidationPriceInfo';
import { useRepayLoan } from '@/hooks/useRepayLoan';
import { useRedeemLoan } from '@/hooks/useRedeemLoan';
import { useNativeCurrency } from '@/components/NetworkCurrenciesProvider';
import { FeesInfo } from '@/components/FeesInfo';
import { numberFormat } from '@/utils/formatNumber';

const initialFormState: FormFields = {
  collateral: {
    value: '',
  },
  synthetic: {
    value: '',
    isMaxAvailable: false,
  },
  isDisabled: true,
  errors: null,
  calculations: null,
};

const RepayPanel = () => {
  const { collateralBalance, syntheticBalance } = useCurrentPairBalances();
  const nativeCurrency = useNativeCurrency();
  const {
    currentPairData,
    pricePair,
    syntheticToken,
    collateralToken,
    loanPositionData,
    creditLineContract,
  } = useCurrentLoanData();

  const [step, setStep] = useState(PANEL_FLOW_STEPS.WELCOME);
  const [skipWelcome, setSkipWelcome] = useState(true);
  const [collateralToggleChecked, setCollateralToggleChecked] = useState(false);

  const [formData, setFormData] = useState<FormFields>(initialFormState);
  const [networkFee, setNetworkFee] = useState('');

  const { approvalState, approvalEstimatedNetworkFee, approve } = useApproval(
    formData.synthetic.value
      ? FixBigNumber.toWei(formData.synthetic.value)
      : undefined,
    syntheticToken,
    currentPairData?.address,
  );
  const { repayEstimatedNetworkFee, repay } = useRepayLoan(
    formData.synthetic.value
      ? FixBigNumber.toWei(formData.synthetic.value)
      : undefined,
    creditLineContract,
  );
  const { redeemEstimatedNetworkFee, redeem } = useRedeemLoan(
    formData.synthetic.value
      ? FixBigNumber.toWei(formData.synthetic.value)
      : undefined,
    formData.collateral.value
      ? FixBigNumber.toWei(formData.collateral.value, collateralToken?.decimals)
      : undefined,
    creditLineContract,
    formData.synthetic.value
      ? FixBigNumber.toWei(formData.synthetic.value).equalTo(
          loanPositionData.syntheticAmount,
        )
        ? TransactionType.CLOSE
        : TransactionType.REDEEM
      : TransactionType.REDEEM,
  );

  const shouldDisplay =
    (collateralToggleChecked &&
      +formData.synthetic.value > 0 &&
      +formData.collateral.value > 0) ||
    (!collateralToggleChecked && +formData.synthetic.value > 0);

  useMemo(async () => {
    let fee;
    if (
      approvalState !== ApprovalState.APPROVED &&
      approvalState !== ApprovalState.UNKNOWN
    ) {
      fee = await approvalEstimatedNetworkFee();
      setNetworkFee(fee);
      return;
    }
    if (shouldDisplay && !!pricePair && currentPairData) {
      if (!collateralToggleChecked) {
        fee = await repayEstimatedNetworkFee();
        setNetworkFee(fee);
      } else {
        fee = await redeemEstimatedNetworkFee();
        setNetworkFee(fee);
      }
    }
  }, [pricePair, approvalState]);

  useMemo(() => {
    if (
      !collateralToken ||
      !loanPositionData ||
      !collateralBalance ||
      !currentPairData ||
      !pricePair
    )
      return;

    const collateralAmount = formData.collateral.value.length
      ? loanPositionData.collateralAmount.subtract(
          FixBigNumber.toWei(
            formData.collateral.value,
            collateralToken.decimals,
          ),
        )
      : loanPositionData.collateralAmount;

    const syntheticAmount = formData.synthetic.value.length
      ? loanPositionData.syntheticAmount.subtract(
          FixBigNumber.toWei(formData.synthetic.value),
        )
      : undefined;

    // eslint-disable-next-line prefer-const
    let { calculations, errors } = validateAmount(
      collateralAmount,
      syntheticAmount,
      currentPairData.minSponsorTokens,
      currentPairData.collateralRequirement,
      pricePair,
      collateralToggleChecked && syntheticAmount?.equalTo(0)
        ? PANEL_TYPE.CLOSE
        : PANEL_TYPE.REPAY,
    );
    if (errors?.type === ERROR_TYPE.MIN_SPONSOR) {
      errors.message = `Minimum leftover in position is ${currentPairData.minSponsorTokens.toExact()} jFIAT`;
    }
    if (
      formData.synthetic.value &&
      syntheticBalance?.balance.lessThan(formData.synthetic.value)
    ) {
      errors = {
        type: ERROR_TYPE.BALANCE,
        message: `Insufficient ${syntheticToken?.symbol} balance`,
        controls: ['borrow'],
      };
    }
    if (
      !errors &&
      formData.synthetic.value &&
      loanPositionData.syntheticAmount?.lessThan(formData.synthetic.value)
    ) {
      errors = {
        type: ERROR_TYPE.BALANCE,
        message: `${syntheticToken?.symbol} input exceed your debt`,
        controls: ['borrow'],
      };
    }
    setFormData({
      ...formData,
      isDisabled: !shouldDisplay || !!errors,
      errors,
      calculations,
    });
  }, [pricePair, formData.collateral.value, formData.synthetic.value]);

  useMemo(() => {
    if (syntheticToken?.symbol && collateralToken?.symbol) {
      setSkipWelcome(
        shouldSkipWelcomeCard(
          step,
          PANEL_TYPE.REPAY,
          syntheticToken.symbol,
          collateralToken.symbol,
        ),
      );
    }
  }, [syntheticToken, collateralToken, step]);

  const renderWelcomeCard = () => {
    let doNotShowAgain = false;

    const welcomeParams = {
      onNext: () => {
        if (doNotShowAgain) {
          localStorage.setItem(
            `jarvis-creditLine/${syntheticToken?.symbol}${collateralToken?.symbol}/welcome-${PANEL_TYPE.REPAY}-accepted`,
            `true`,
          );
        }
        setStep(PANEL_FLOW_STEPS.FORM);
      },
      onCheckboxChange: () => {
        doNotShowAgain = !doNotShowAgain;
      },
      content: () => (
        <>
          <strong>Repay</strong>
          <p>You can repay a part of your loan. </p>
          <br />
          <p>
            This action increases your Health Ratio, reducing the risk of
            liquidation.
          </p>
          <br />
          <p>
            You can also choose to remove a portion of your collateral
            simultaneously.
          </p>
        </>
      ),
    };
    return <WelcomeCard {...welcomeParams} />;
  };

  if (
    !syntheticToken ||
    !collateralToken ||
    !loanPositionData ||
    !pricePair ||
    !collateralBalance ||
    !currentPairData ||
    !syntheticBalance ||
    !nativeCurrency
  )
    return <ContentWrapper />;

  const setNoCollateral = () => {
    setFormData({
      ...formData,
      collateral: {
        value: '',
      },
    });
  };

  const getMaxRemovalCollateral = (userInput: string | undefined) => {
    if (!userInput || !userInput.length) return '0';
    const debtRatio = loanPositionData.collateralAmount.divide(
      loanPositionData.syntheticAmount,
    );
    const repayInput = FixBigNumber.toWei(userInput);
    const max = repayInput.multiply(debtRatio);
    return max?.greaterThan(0) ? max?.toFixed(DECIMAL_DIGITS_COUNT) : '0';
  };

  const setMaxCollateral = () => {
    const maxRemovalCollateralAmount = getMaxRemovalCollateral(
      formData.synthetic.value,
    );
    if (!maxRemovalCollateralAmount) return;
    setFormData({
      ...formData,
      collateral: {
        ...formData.collateral,
        value: maxRemovalCollateralAmount,
      },
    });
  };

  const onUserRepayInput = (userInput: string) => {
    const collateralAmount = collateralToggleChecked
      ? getMaxRemovalCollateral(userInput)
      : '';
    setFormData({
      ...formData,
      synthetic: {
        ...formData.synthetic,
        value: userInput,
      },
      collateral: { value: collateralAmount },
    });
  };

  const getMaxRepaySynthetic = () => {
    const maxToRepay = syntheticBalance.balance.greaterThan(
      loanPositionData.syntheticAmount,
    )
      ? loanPositionData.syntheticAmount
      : syntheticBalance.balance;
    if (!collateralToggleChecked) {
      return maxToRepay.greaterThanOrEqualTo(currentPairData.minSponsorTokens)
        ? maxToRepay
            .subtract(currentPairData.minSponsorTokens)
            .toFixed(DECIMAL_DIGITS_COUNT)
        : '0';
    }
    return maxToRepay.toFixed(DECIMAL_DIGITS_COUNT);
  };

  const handleMaxToRepay = () => {
    const maxSynthetic = getMaxRepaySynthetic();
    const collateralAmount = collateralToggleChecked
      ? getMaxRemovalCollateral(maxSynthetic)
      : '';
    setFormData({
      ...formData,
      synthetic: {
        value: maxSynthetic,
      },
      collateral: { value: collateralAmount },
    });
  };

  const handleSubmit = (isApproval: boolean) => {
    if (isApproval) {
      approve();
    } else if (collateralToggleChecked) {
      redeem().then(() => setFormData(initialFormState));
    } else {
      repay().then(() => setFormData(initialFormState));
    }
  };

  const isApproval =
    approvalState !== ApprovalState.APPROVED &&
    approvalState !== ApprovalState.UNKNOWN;

  return (
    <>
      <ContentWrapper>
        {skipWelcome ? (
          <>
            <FieldSet>
              <NumericalInput
                label="Repay"
                placeholder={`Max: ${getMaxRepaySynthetic()}`}
                isError={formData.errors?.controls.includes('borrow')}
                value={formData.synthetic.value}
                onUserInput={onUserRepayInput}
                suffix={
                  <InputActionsWrapper>
                    <Button size="s" onClick={handleMaxToRepay}>
                      MAX
                    </Button>
                    <Flag flag={syntheticToken.symbol} />{' '}
                    {syntheticToken.symbol}
                  </InputActionsWrapper>
                }
                balanceValueInfo={`balance: ${numberFormat(
                  syntheticBalance.balance.toExact(),
                  false,
                  0,
                  DECIMAL_DIGITS_COUNT,
                )}`}
              />
              <NumericalInput
                label="Remove Collateral"
                placeholder=""
                isError={formData.errors?.controls.includes('collateral')}
                value={
                  collateralToggleChecked
                    ? getMaxRemovalCollateral(formData.synthetic.value)
                    : ''
                }
                onUserInput={() => null}
                suffix={
                  <InputActionsWrapper>
                    <Flag flag={collateralToken.symbol} />
                    {collateralToken.symbol}
                  </InputActionsWrapper>
                }
                onToggleChange={() => {
                  setCollateralToggleChecked(!collateralToggleChecked);
                  if (!collateralToggleChecked) {
                    setMaxCollateral();
                  } else {
                    setNoCollateral();
                  }
                }}
                inputDisabled
                inputHidePlaceHolder="ℹ️ You can also remove a portion of your collateral at the same time."
              />
            </FieldSet>

            <ErrorMessageWrapper style={{ marginBottom: '35px' }}>
              {formData.errors?.message ? (
                <ErrorMessage>{formData.errors?.message}</ErrorMessage>
              ) : (
                ''
              )}
            </ErrorMessageWrapper>
            <LoanPreviewPositionInfoWrapper>
              {shouldDisplay && formData.calculations && (
                <LoanRatioAndLiquidationPriceInfo
                  loanCalculations={formData.calculations}
                  liquidationTitle={`New ${currentPairData.inversePair} liquidation price`}
                  healthRatioTitle="New health ratio"
                />
              )}
            </LoanPreviewPositionInfoWrapper>
            <SubmitButton
              type="success"
              size="s"
              disabled={formData.isDisabled}
              onClick={() => {
                handleSubmit(isApproval);
              }}
            >
              {isApproval
                ? 'Approve'
                : collateralToggleChecked
                ? 'Redeem'
                : 'Repay'}
            </SubmitButton>
          </>
        ) : (
          renderWelcomeCard()
        )}
      </ContentWrapper>
      {shouldDisplay && skipWelcome && !formData.errors && (
        <FeesInfo networkFee={networkFee} />
      )}
    </>
  );
};

export default RepayPanel;
