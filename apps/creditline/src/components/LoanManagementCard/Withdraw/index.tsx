import React, { useMemo, useState } from 'react';

import { FixBigNumber } from '@jarvisnetwork/core-sdk';
import { Button, Flag, styled } from '@jarvisnetwork/ui';

import { SliderInput } from '@jarvisnetwork/toolkit';

import {
  ContentWrapper,
  FieldSet,
  InputActionsWrapper,
  ErrorMessage,
  SubmitButton,
  ErrorMessageWrapper,
  LoanPreviewPositionInfoWrapper,
} from '../styles';
import { FormCalculations, FormFields } from '../interfaces';

import { ERROR_TYPE, PANEL_FLOW_STEPS, PANEL_TYPE } from '@/enums';

import WelcomeCard from '@/components/WelcomeCard';
import { NumericalInput } from '@/components/NumericalInput';

import {
  shouldSkipWelcomeCard,
  validateAmount,
  calcHealthRatio,
  calcCollateralAmountFromRatioWithoutFee,
} from '@/utils/loanManagementHelpers';
import { useCurrentPairBalances } from '@/hooks/useCurrentPairBalances';
import { useCurrentLoanData } from '@/hooks/useCurrentLoanData';
import { ApprovalState, useApproval } from '@/hooks/useApproval';
import { DECIMAL_DIGITS_COUNT } from '@/config/app';

import { LoanRatioAndLiquidationPriceInfo } from '@/components/LoanRatioAndLiquidationPriceInfo';
import { useWithdrawLoan } from '@/hooks/useWithdrawLoan';
import { useNativeCurrency } from '@/components/NetworkCurrenciesProvider';
import { FeesInfo } from '@/components/FeesInfo';
import { numberFormat } from '@/utils/formatNumber';

const initialFormState: FormFields = {
  collateral: {
    value: '',
  },
  synthetic: {
    value: '',
    isMaxAvailable: false,
  },
  isDisabled: true,
  errors: null,
  calculations: null,
};

const SliderWrapper = styled.div`
  margin-top: 32px;
  margin-bottom: 8px;
`;

const CollateralValue = styled.div`
  font-size: ${props => props.theme.font.sizes.l};
  display: flex;
  justify-content: space-between;
  margin-bottom: 16px;
`;

const SliderInfoValue = styled.div`
  font-size: ${props => props.theme.font.sizes.s};
  display: flex;
  justify-content: space-between;
  margin-top: 16px;
`;

const WithdrawPanel = () => {
  const { collateralBalance, syntheticBalance } = useCurrentPairBalances();
  const nativeCurrency = useNativeCurrency();
  const {
    currentPairData,
    pricePair,
    syntheticToken,
    collateralToken,
    loanPositionData,
    creditLineContract,
  } = useCurrentLoanData();

  const [step, setStep] = useState(PANEL_FLOW_STEPS.WELCOME);
  const [skipWelcome, setSkipWelcome] = useState(true);

  const [formData, setFormData] = useState<FormFields>(initialFormState);
  const [networkFee, setNetworkFee] = useState('');
  const { approvalState, approvalEstimatedNetworkFee, approve } = useApproval(
    formData.collateral.value
      ? FixBigNumber.toWei(formData.collateral.value, collateralToken?.decimals)
      : undefined,
    collateralToken,
    currentPairData?.address,
  );
  const { withdrawEstimatedNetworkFee, withdraw } = useWithdrawLoan(
    formData.collateral.value
      ? FixBigNumber.toWei(formData.collateral.value, collateralToken?.decimals)
      : undefined,
    creditLineContract,
  );

  useMemo(async () => {
    let fee;
    if (
      approvalState !== ApprovalState.APPROVED &&
      approvalState !== ApprovalState.UNKNOWN
    ) {
      fee = await approvalEstimatedNetworkFee();
      setNetworkFee(fee);
      return;
    }
    fee = await withdrawEstimatedNetworkFee();
    setNetworkFee(fee);
  }, [approvalState, pricePair]);

  useMemo(() => {
    if (
      !collateralToken ||
      !loanPositionData ||
      !collateralBalance ||
      !currentPairData ||
      !pricePair
    )
      return;

    const collateralAmount =
      +formData.collateral.value > 0
        ? loanPositionData.collateralAmount.subtract(
            FixBigNumber.toWei(
              formData.collateral.value,
              collateralToken.decimals,
            ),
          )
        : undefined;

    const { syntheticAmount } = loanPositionData;

    // eslint-disable-next-line prefer-const
    let { calculations, errors } = validateAmount(
      collateralAmount,
      syntheticAmount,
      currentPairData.minSponsorTokens,
      currentPairData.collateralRequirement,
      pricePair,
      PANEL_TYPE.REMOVE,
    );
    if (
      formData.collateral.value &&
      collateralAmount &&
      (loanPositionData?.collateralAmount.lessThan(formData.collateral.value) ||
        !calculations?.healthRatio)
    ) {
      errors = {
        type: ERROR_TYPE.BALANCE,
        message: `Insufficient collateral in position`,
        controls: ['collateral'],
      };
      if (collateralAmount?.lessThan(0)) {
        calculations = {
          ...calculations,
          healthRatio: FixBigNumber.toWei(0),
        } as FormCalculations;
      }
    }
    setFormData({
      ...formData,
      isDisabled: !!errors || !collateralAmount,
      errors,
      calculations,
    });
  }, [pricePair, formData.collateral.value]);

  useMemo(() => {
    if (syntheticToken?.symbol && collateralToken?.symbol) {
      setSkipWelcome(
        shouldSkipWelcomeCard(
          step,
          PANEL_TYPE.REMOVE,
          syntheticToken.symbol,
          collateralToken.symbol,
        ),
      );
    }
  }, [syntheticToken, collateralToken, step]);

  const renderWelcomeCard = () => {
    let doNotShowAgain = false;

    const welcomeParams = {
      onNext: () => {
        if (doNotShowAgain) {
          localStorage.setItem(
            `jarvis-creditLine/${syntheticToken?.symbol}${collateralToken?.symbol}/welcome-${PANEL_TYPE.REMOVE}-accepted`,
            `true`,
          );
        }
        setStep(PANEL_FLOW_STEPS.FORM);
      },
      onCheckboxChange: () => {
        doNotShowAgain = !doNotShowAgain;
      },
      content: () => (
        <>
          <strong>Remove collateral</strong>
          <p>You can remove a portion of your available collateral.</p>
          <br />
          <p>
            This action decreases your Health Ratio, increasing the risk of
            liquidation.
          </p>
        </>
      ),
    };
    return <WelcomeCard {...welcomeParams} />;
  };

  if (
    !syntheticToken ||
    !collateralToken ||
    !currentPairData ||
    !loanPositionData ||
    !pricePair ||
    !collateralBalance ||
    !syntheticBalance ||
    !nativeCurrency
  )
    return <ContentWrapper />;

  const getMaxCollateralToWithdraw = () => {
    const requiredCollateralAmount = calcCollateralAmountFromRatioWithoutFee(
      currentPairData.collateralRequirement.multiply(100),
      loanPositionData.syntheticAmount,
      pricePair,
    );
    return loanPositionData.collateralAmount.subtract(
      requiredCollateralAmount!,
    );
  };

  const onUserCollateralInput = (userInput: string) => {
    setFormData({
      ...formData,
      collateral: {
        value: userInput,
      },
    });
  };

  const onCollateralMaxClick = () => {
    setFormData({
      ...formData,
      collateral: {
        ...formData.collateral,
        value: getMaxCollateralToWithdraw().toExact(DECIMAL_DIGITS_COUNT),
      },
    });
  };

  const getCurrentHealthRatio = () =>
    calcHealthRatio(
      loanPositionData.collateralAmount,
      loanPositionData.syntheticAmount,
      pricePair,
    );

  const handleHealthRatioSlider = (newHealthRatio: string) => {
    const requiredCollateralAmount = calcCollateralAmountFromRatioWithoutFee(
      FixBigNumber.toWei(newHealthRatio),
      loanPositionData.syntheticAmount,
      pricePair,
    );
    const inputCollateralAmount = loanPositionData.collateralAmount.subtract(
      requiredCollateralAmount!,
    );
    setFormData({
      ...formData,
      collateral: {
        ...formData.collateral,
        value: inputCollateralAmount.greaterThan(0)
          ? inputCollateralAmount.toExact(DECIMAL_DIGITS_COUNT)
          : '',
      },
    });
  };

  const isApproval =
    approvalState !== ApprovalState.APPROVED &&
    approvalState !== ApprovalState.UNKNOWN;

  return (
    <>
      <ContentWrapper>
        {skipWelcome ? (
          <>
            <FieldSet>
              <NumericalInput
                label="Remove"
                placeholder="0"
                isError={formData.errors?.controls.includes('collateral')}
                value={formData.collateral.value}
                onUserInput={onUserCollateralInput}
                suffix={
                  <InputActionsWrapper>
                    <Button size="s" onClick={onCollateralMaxClick}>
                      MAX
                    </Button>
                    <Flag flag={collateralToken.symbol} />{' '}
                    {collateralToken.symbol}
                  </InputActionsWrapper>
                }
                balanceValueInfo={`balance: ${numberFormat(
                  getMaxCollateralToWithdraw().toExact(),
                  false,
                  0,
                  DECIMAL_DIGITS_COUNT,
                )}`}
              />
            </FieldSet>
            <SliderWrapper>
              <CollateralValue>
                <div>Health Ratio</div>
                <div>
                  {formData.calculations?.healthRatio?.toFixed(2) ??
                    getCurrentHealthRatio()?.toFixed(2)}
                  %
                </div>
              </CollateralValue>
              <SliderInput
                minInput={
                  currentPairData.collateralRequirement
                    ?.multiply(100)
                    .toFixed(2) ?? '0'
                }
                maxInput={getCurrentHealthRatio()?.toFixed(2) ?? '100'}
                slideValue={
                  formData.calculations?.healthRatio?.toFixed(2) ??
                  getCurrentHealthRatio()!.toFixed(2)
                }
                onSlide={val => handleHealthRatioSlider(val.toString())}
              />
              <SliderInfoValue>
                <div>
                  Min:{' '}
                  {currentPairData.collateralRequirement
                    ?.multiply(100)
                    .toFixed(2)}
                  %
                </div>
                <div>Current: {getCurrentHealthRatio()?.toFixed(2)}%</div>
              </SliderInfoValue>
            </SliderWrapper>
            <ErrorMessageWrapper style={{ marginBottom: '35px' }}>
              {formData.errors ? (
                <ErrorMessage>{formData.errors?.message}</ErrorMessage>
              ) : (
                ''
              )}
            </ErrorMessageWrapper>
            <LoanPreviewPositionInfoWrapper>
              {formData.calculations && (
                <LoanRatioAndLiquidationPriceInfo
                  loanCalculations={formData.calculations}
                  liquidationTitle={`New ${currentPairData.inversePair} liquidation price`}
                  healthRatioTitle="New health ratio"
                />
              )}
            </LoanPreviewPositionInfoWrapper>
            <SubmitButton
              type="success"
              size="s"
              disabled={formData.isDisabled}
              onClick={() =>
                isApproval
                  ? approve()
                  : withdraw().then(() => setFormData(initialFormState))
              }
            >
              {isApproval ? 'Approve' : 'Withdraw'}
            </SubmitButton>
          </>
        ) : (
          renderWelcomeCard()
        )}
      </ContentWrapper>
      {+formData.collateral.value > 0 && !formData.errors && skipWelcome && (
        <FeesInfo networkFee={networkFee} />
      )}
    </>
  );
};

export default WithdrawPanel;
