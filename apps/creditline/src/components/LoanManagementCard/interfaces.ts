import { FixBigNumber } from '@jarvisnetwork/core-sdk';

import { ERROR_TYPE } from 'enums/panel';

export interface Balances {
  collateral: FixBigNumber;
}

export interface CardProps {
  title: string;
  className: string;
}

export interface FormCalculations {
  healthRatio: FixBigNumber | undefined;
  liquidationPrice: FixBigNumber | undefined;
}

export interface FormField {
  value: string;
  isMaxAvailable?: boolean;
}

interface FormErros {
  type: ERROR_TYPE;
  message: string;
  controls: string[];
}

export interface FormFields {
  collateral: FormField;
  synthetic: FormField;
  isDisabled: boolean;
  errors: null | FormErros;
  calculations: null | FormCalculations;
}

export interface CurrentPricePairSide {
  name: string;
  symbol: string;
  price: FixBigNumber;
}

export interface CurrentTransactionStatusCheck {
  hash?: string;
  updatedAt: number;
  isVisible: boolean;
  details: {
    collateral: CurrentPricePairSide;
    borrow: CurrentPricePairSide;
    success?: boolean;
    error?: boolean;
    isCancelled?: boolean;
  };
}

export interface RenderPageProps {
  redirectTo: () => void;
}
