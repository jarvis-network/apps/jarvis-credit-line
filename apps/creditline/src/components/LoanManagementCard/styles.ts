import {
  Button,
  CardFooter,
  FormGroup as FormGroupCompoonent,
  styled,
} from '@jarvisnetwork/ui';

export const ExchangeCardWrapper = styled.div`
  width: 100%;
  height: auto;
  background: ${params => params.theme.background.secondary};
`;

export const CardFooterWrapper = styled(CardFooter)``;

export const CardFooterContent = styled.div`
  padding: 16px;
  background: ${params => params.theme.background.secondary};
  border-bottom-left-radius: ${params => params.theme.borderRadius.m};
  border-bottom-right-radius: ${params => params.theme.borderRadius.m};
  color: ${params => params.theme.text.medium};
  display: flex;
  flex-direction: column;
  gap: 16px;

  .row {
    display: flex;
    justify-content: space-between;
  }

  .price {
    color: ${params => params.theme.text.secondary};
  }
`;

export const IconWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  img {
    width: 129px;
    height: 129px;
  }
`;

export const ContentWrapper = styled.div`
  padding: 16px;
  min-height: 585px;
  display: flex;
  flex-direction: column;
  background: ${props => props.theme.background.secondary};
`;

export const TextWrapper = styled.div``;

export const SubmitButton = styled(Button)`
  transition: none;
  width: 100%;
  text-align: center;
  margin-top: auto;
  padding: 19px 0;
`;

export const InputActionsWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 5px;
  padding-right: 10px !important;

  button {
    background: transparent;
  }
`;

export const InputWrapper = styled.div`
  height: 60px;

  .input {
    &.invalid .group {
      border: 1px solid red;
    }

    .group {
      overflow: hidden;

      & > :first-child {
        padding: 0;
        border-radius: unset;

        input {
          background: transparent;
          padding-left: 10px;
        }
      }
    }
  }
`;

export const FieldSet = styled.div`
  margin-top: 16px;
  gap: 32px;
  display: flex;
  flex-direction: column;
`;

export const FormGroupStyled = styled(FormGroupCompoonent)`
  margin-bottom: 0px;

  .input {
    margin-top: 0;
  }
`;

export const InfoLine = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${props => props.theme.text.secondary};
  font-size: ${props => props.theme.font.sizes.s};
  text-align: center;

  .label {
    margin-right: auto;
  }

  button {
    background: transparent;
  }
`;

export const ErrorMessageWrapper = styled.div`
  margin: 8px 0;
  height: 36px;
`;

export const LoanPreviewPositionInfoWrapper = styled.div`
  margin: 16px 0;
  height: 152px;
`;

export const ErrorMessage = styled.p`
  background: ${props => props.theme.tooltip.error.background};
  color: ${props => props.theme.tooltip.error.text};
  padding: 10px 0;
  border-radius: ${props => props.theme.borderRadius.s};
  text-align: center;
  font-size: ${props => props.theme.font.sizes.s};
`;
