import { FixBigNumber } from '@jarvisnetwork/core-sdk';

import styled from '@emotion/styled';

import { PANEL_TYPE } from 'enums/panel';

import { BackButtonWrapper, LoanInfo, LoanOverviewWrapper } from './styles';

import BackButton from '@/components/BackButton';
import LoanHeading from '@/components/LoanHeading';
import LoanInfoItem from '@/components/LoanInfoItem';
import { DECIMAL_DIGITS_COUNT } from '@/config/app';
import { useCurrentLoanData } from '@/hooks/useCurrentLoanData';
import { calcHealthRatioStyles } from '@/utils/loanManagementHelpers';
import { numberFormat } from '@/utils/formatNumber';
import { usePanelType } from '@/state/app/hooks';

interface LoanOverviewProps {
  showBackButton: boolean;
  showLoanHeading: boolean;
  onBackClick: () => void;
}

const PlaceholderDiv = styled.div`
  display: flex;
  gap: 8px;
`;

const LoanOverview = ({
  showBackButton,
  showLoanHeading,
  onBackClick,
}: LoanOverviewProps) => {
  const panelType = usePanelType();
  const {
    currentPairData,
    pricePair,
    syntheticToken,
    collateralToken,
    loanPositionData,
  } = useCurrentLoanData();

  if (!currentPairData || !pricePair || !syntheticToken || !collateralToken) {
    return <div />;
  }

  const { feePercentage: feeInfo, collateralRequirement } = currentPairData;
  const collateralIcon = collateralToken.symbol;
  const syntheticIcon = syntheticToken.symbol;
  const renderHealthRatio = () => {
    if (panelType === PANEL_TYPE.CREATE) return <div>-</div>;
    if (!loanPositionData?.healthRatio)
      return (
        <PlaceholderDiv>
          <div className="placeholder placeholder-s" /> %
        </PlaceholderDiv>
      );
    const healthRatioSyle = calcHealthRatioStyles(loanPositionData.healthRatio);

    return (
      <span className={healthRatioSyle?.colorClass}>{`${numberFormat(
        loanPositionData.healthRatio?.toFixed(2),
        false,
      )}%`}</span>
    );
  };

  const renderLiquidationPrice = () => {
    if (panelType === PANEL_TYPE.CREATE) return <div>-</div>;
    if (
      !loanPositionData ||
      !pricePair ||
      !loanPositionData?.collateralAmount.greaterThan(0) ||
      !loanPositionData?.syntheticAmount.greaterThan(0)
    )
      return (
        <>
          <PlaceholderDiv>
            <div className="placeholder placeholder-s" />
          </PlaceholderDiv>
        </>
      );

    const percent = FixBigNumber.toWei(1)
      .divide(pricePair)
      .subtract(loanPositionData.liquidationPrice)
      .divide(FixBigNumber.toWei(1).divide(pricePair))
      .multiply(100);
    return (
      <>
        <div>
          {numberFormat(
            loanPositionData.liquidationPrice.toFixed(),
            false,
            0,
            DECIMAL_DIGITS_COUNT,
          )}
        </div>
        <div className="secondary">
          (
          {FixBigNumber.toWei(1)
            .divide(pricePair)
            .greaterThan(loanPositionData.liquidationPrice)
            ? `${numberFormat(percent.toFixed(2), false)}% from current price`
            : 'Bellow liquidation price'}
          )
        </div>
      </>
    );
  };

  return (
    <LoanOverviewWrapper>
      {showBackButton ? (
        <BackButtonWrapper>
          <BackButton onBackButtonClick={onBackClick} className="" />
        </BackButtonWrapper>
      ) : (
        ''
      )}
      {showLoanHeading && collateralIcon && syntheticIcon ? (
        <LoanHeading className="heading" />
      ) : (
        ''
      )}

      <LoanInfo>
        <LoanInfoItem
          icon="debtIcon"
          title="Debt"
          tooltip="The current debt you have in synthetic tokens"
        >
          {loanPositionData?.syntheticAmount.equalTo(0) ||
          !loanPositionData?.syntheticAmount ? (
            panelType === PANEL_TYPE.CREATE ? (
              <>
                <div>-</div>
                <div className="secondary">-</div>
              </>
            ) : (
              <>
                <PlaceholderDiv style={{ marginBottom: '4px' }}>
                  <div className="placeholder placeholder-s" /> {syntheticIcon}
                </PlaceholderDiv>
                <PlaceholderDiv className="secondary">
                  <div className="placeholder placeholder-s" /> {collateralIcon}
                </PlaceholderDiv>
              </>
            )
          ) : (
            <>
              <div>
                {numberFormat(
                  loanPositionData.syntheticAmount.toFixed(
                    DECIMAL_DIGITS_COUNT,
                  ),
                  false,
                  0,
                  DECIMAL_DIGITS_COUNT,
                ) ?? '-'}{' '}
                {syntheticIcon}
              </div>

              <div className="secondary">
                {pricePair
                  ? `~ ${numberFormat(
                      loanPositionData?.syntheticAmount
                        .multiply(pricePair ?? 0)
                        .toExact(),
                      false,
                      0,
                      DECIMAL_DIGITS_COUNT,
                    )} ${collateralIcon}`
                  : '-'}
              </div>
            </>
          )}
        </LoanInfoItem>

        <LoanInfoItem
          icon="collateralIcon"
          title="Collateral"
          tooltip="The current amount of collateral locked in the protocol"
        >
          {loanPositionData?.collateralAmount.equalTo(0) ||
          !loanPositionData?.collateralAmount ||
          !pricePair ? (
            panelType === PANEL_TYPE.CREATE ? (
              <>
                <div>-</div>

                <div className="secondary">-</div>
              </>
            ) : (
              <>
                <PlaceholderDiv style={{ marginBottom: '4px' }}>
                  <div className="placeholder placeholder-s" /> {collateralIcon}
                </PlaceholderDiv>
                <PlaceholderDiv className="secondary">
                  <div className="placeholder placeholder-s" /> {syntheticIcon}
                </PlaceholderDiv>
              </>
            )
          ) : (
            <>
              <div>
                {numberFormat(
                  loanPositionData?.collateralAmount.toExact(),
                  false,
                  0,
                  DECIMAL_DIGITS_COUNT,
                )}{' '}
                {collateralIcon}
              </div>

              <div className="secondary">
                ~{' '}
                {numberFormat(
                  loanPositionData?.collateralAmount
                    .multiply(FixBigNumber.toWei(1).divide(pricePair) ?? 0)
                    .toExact(),
                  false,
                  0,
                  DECIMAL_DIGITS_COUNT,
                )}{' '}
                {syntheticIcon}
              </div>
            </>
          )}
        </LoanInfoItem>

        <LoanInfoItem
          icon="collateralRatioIcon"
          title="Health Ratio"
          tooltip="The current ratio of collaterals to tokens in your position"
        >
          {renderHealthRatio()}
        </LoanInfoItem>

        <LoanInfoItem
          icon="liquidationRatioIcon"
          title="Liquidation"
          tooltip="If your position health ratio falls below this ratio, your position can be liquidated"
        >
          <div>
            {collateralRequirement ? (
              `${collateralRequirement.multiply(100).toFixed(2)}%`
            ) : (
              <PlaceholderDiv>
                <div className="placeholder placeholder-s" /> %
              </PlaceholderDiv>
            )}
          </div>
        </LoanInfoItem>

        <LoanInfoItem
          icon="jFIATCollateralPriceIcon"
          title={`${currentPairData.inversePair} Price`}
          tooltip="The current market price for the given pair"
        >
          <div>
            {pricePair ? (
              FixBigNumber.toWei(1)
                .divide(pricePair)
                .toFixed(DECIMAL_DIGITS_COUNT)
            ) : (
              <PlaceholderDiv>
                <div className="placeholder placeholder-s" />
              </PlaceholderDiv>
            )}
          </div>
        </LoanInfoItem>

        <LoanInfoItem
          icon="liquidationPriceIcon"
          title="Liquidation Price"
          tooltip="The price at which your position will be below the liquidation ratio"
        >
          {renderLiquidationPrice()}
        </LoanInfoItem>

        <LoanInfoItem
          icon="originationFeeIcon"
          title="Origination Fee"
          tooltip="Percentage fee which is paid to the protocol on borrow operation"
        >
          <div>{feeInfo.multiply(100).toFixed(2)}%</div>
        </LoanInfoItem>

        <LoanInfoItem
          icon="liquidationPenaltyIcon"
          title="Liquidation fee"
          tooltip="A percentage fee derived from the leftover collateral after a liquidation has occurred"
        >
          <div>
            {currentPairData?.liquidationReward ? (
              `${collateralRequirement
                .multiply(100)
                .subtract(100)
                .multiply(currentPairData.liquidationReward)
                .toFixed(2)}%`
            ) : (
              <PlaceholderDiv>
                <div className="placeholder placeholder-s" /> %
              </PlaceholderDiv>
            )}
          </div>
        </LoanInfoItem>
      </LoanInfo>
    </LoanOverviewWrapper>
  );
};

export default LoanOverview;
