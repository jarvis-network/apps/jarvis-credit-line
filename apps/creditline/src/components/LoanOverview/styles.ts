import { Icon, styled } from '@jarvisnetwork/ui';

export const LoanOverviewWrapper = styled.div`
  height: 100%;
`;

export const LoanInfo = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 13px;
  justify-content: center;
`;

export const LoanInfoItemStyled = styled.div`
  background: ${props => props.theme.background.secondary};
  border-radius: ${props => props.theme.borderRadius.m};
  width: 48%;
  height: 129px;
  padding: 20px;
  display: flex;
  flex-direction: column;
  font-size: 22px;
  .data {
    margin-top: 12px;
  }
  .secondary {
    color: ${props => props.theme.text.secondary};
    font-size: ${props => props.theme.font.sizes.s};
  }
  .danger {
    color: ${props => props.theme.common.danger};
  }

  .warning {
    color: ${props => props.theme.common.warning};
  }

  .success {
    color: ${props => props.theme.common.success};
  }
`;

export const LoanInfoItemHeading = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 15px;
`;

export const BackButtonWrapper = styled.div`
  margin-bottom: 40px;

  Button {
    padding-left: 0;
  }
`;

export const IconWrapper = styled(Icon)`
  width: 24px;
  height: 24px;
`;

export const LoanInfoItemTitle = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  gap: 11px;
`;

export const HeaderLine = styled.div`
  display: flex;
  gap: 20px;
  margin-bottom: 20px;
  align-items: center;
  .title {
    font: var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-600)
      var(--unnamed-font-size-20) / var(--unnamed-line-spacing-26)
      var(--unnamed-font-family-krub);
    letter-spacing: var(--unnamed-character-spacing-0);
    color: var(--light-grey);
    text-align: left;
    font: normal normal 600 20px/26px Krub;
    letter-spacing: 0px;
    color: #bebebe;
    text-transform: capitalize;
  }
`;

export const LoanIconsWrapper = styled.div`
  position: relative;

  .debt-currency {
    position: absolute;
    margin-left: -15px;
  }

  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex - 1]}px) {
    margin-right: 10px;

    img {
      width: 21px;
    }

    .debt-currency {
      margin-left: -12px;
    }
  }
`;

export const SubInfo = styled.div`
  font-size: ${props => props.theme.font.sizes.s};
`;
