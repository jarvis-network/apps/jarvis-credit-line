/* eslint-disable react/require-default-props */
import React from 'react';
import { Emoji, styled } from '@jarvisnetwork/ui';

import { FormCalculations } from '../LoanManagementCard/interfaces';

import { calcHealthRatioStyles } from '@/utils/loanManagementHelpers';
import { DECIMAL_DIGITS_COUNT } from '@/config/app';
import { useUserCurrentLoanPosition } from '@/state/loan/hooks';
import { useCreditLinePairData } from '@/hooks/useCreditLinePairData';

const LoanInfoPreview = styled.div`
  text-align: center;
  font-size: 14px;
  color: ${props => props.theme.text.primary};
  background: ${props => props.theme.background.primary};
  border-radius: ${props => props.theme.borderRadius.m};
  padding: 16px 0px;
`;

const Item = styled.div`
  padding: 8px;
`;

const Title = styled.div``;

const Text = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 5px;
  font-size: 18px;
  margin-top: 6px;
  color: ${props => props.theme.text.primary};

  .emoji {
    font-size: 15px;
  }

  .danger {
    color: ${props => props.theme.common.danger};
  }

  .warning {
    color: ${props => props.theme.common.warning};
  }

  .success {
    color: ${props => props.theme.common.success};
  }
`;

const Chevron = styled.div`
  margin-top: -2px;
  color: ${props => props.theme.text.secondary};
`;

export const LoanRatioAndLiquidationPriceInfo = React.memo(
  ({
    loanCalculations,
    liquidationTitle,
    healthRatioTitle,
  }: {
    loanCalculations: FormCalculations;
    liquidationTitle: string;
    healthRatioTitle: string;
  }) => {
    const { liquidationPrice, healthRatio } = loanCalculations;

    const currentPairData = useCreditLinePairData();
    const healthRatioSyle = calcHealthRatioStyles(
      healthRatio,
      currentPairData?.collateralRequirement,
    );
    const userCurrentLoanPosition = useUserCurrentLoanPosition();

    const renderCurrentHealthRatio = () => {
      if (!userCurrentLoanPosition?.healthRatio) return null;
      const currentHealthRatioSyle = calcHealthRatioStyles(
        userCurrentLoanPosition.healthRatio,
        currentPairData?.collateralRequirement,
      );

      return (
        <>
          <Emoji
            emoji={currentHealthRatioSyle?.emoji || 'None'}
            className={currentHealthRatioSyle?.emoji}
          />
          <div className={currentHealthRatioSyle?.colorClass}>
            {`${userCurrentLoanPosition.healthRatio.toExact(2)}`}
          </div>
          <Chevron> {'>'} </Chevron>
        </>
      );
    };

    const renderCurrentLiquidationPrice = () => {
      if (!userCurrentLoanPosition?.liquidationPrice) return null;
      return (
        <>
          {`${userCurrentLoanPosition.liquidationPrice?.toFixed(
            DECIMAL_DIGITS_COUNT,
          )}`}
          <Chevron>{'>'}</Chevron>
        </>
      );
    };

    return (
      <LoanInfoPreview>
        <Item>
          <Title>{healthRatioTitle}</Title>

          <Text>
            {renderCurrentHealthRatio()}
            <Emoji
              emoji={healthRatioSyle?.emoji || 'None'}
              className={healthRatioSyle?.emoji}
            />
            <div className={healthRatioSyle?.colorClass}>
              {`${healthRatio ? `${healthRatio?.toFixed(2)}%` : '0%'}`}
            </div>
          </Text>
        </Item>

        <Item>
          <Title>{liquidationTitle}</Title>

          <Text>
            {renderCurrentLiquidationPrice()}
            {`${
              liquidationPrice
                ? liquidationPrice.greaterThan(0)
                  ? liquidationPrice.toFixed(DECIMAL_DIGITS_COUNT)
                  : '0'
                : '-'
            }`}
          </Text>
        </Item>
      </LoanInfoPreview>
    );
  },
);
