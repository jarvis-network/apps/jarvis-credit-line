import React from 'react';

import { Welcome } from '../Auth/Welcome';

import { AccountSummary } from '../AccountSummary';

import { AuthFlow } from '../AuthFlow';

import { ApplicationModal } from '@/state/app/reducer';
import {
  useCloseModal,
  useModalIsOpen,
  useToggleAccountSummaryModal,
} from '@/state/app/hooks';

export const Modals = () => {
  const isAuthModalOpen = useModalIsOpen(ApplicationModal.AUTH);
  const closeAuthModal = useCloseModal(ApplicationModal.AUTH);
  const isAccountSummaryModalOpen = useModalIsOpen(ApplicationModal.ACCOUNT);
  const toggleAccountSummaryModal = useToggleAccountSummaryModal();

  return (
    <>
      <AuthFlow
        appName="jarvis-credit-line"
        isAuthModalOpen={isAuthModalOpen}
        closeAuthModal={closeAuthModal}
        Welcome={Welcome}
      />
      <AccountSummary
        isAccountSummaryModalOpen={isAccountSummaryModalOpen}
        toggleAccountSummaryModal={toggleAccountSummaryModal}
      />
    </>
  );
};
