import React, { createContext, useContext, useMemo } from 'react';

import { Currency, NetworkId } from '@jarvisnetwork/core-sdk';
import {
  defaultSupportedNetworkId,
  isSupportedNetworkId,
} from '@jarvisnetwork/toolkit';
import { useNetwork } from 'wagmi';

import { NetworksTokens } from '@/config/tokens';

type NetworkCurrencies = Currency[] | undefined;

const NetworkCurrenciesContext = createContext<NetworkCurrencies>(undefined);

export const useNetworkCurrencies = () => useContext(NetworkCurrenciesContext);

export const useNativeCurrency = (): Currency | undefined => {
  const networkCurrencies = useNetworkCurrencies();
  return useMemo(() => {
    if (!networkCurrencies) return undefined;
    return networkCurrencies.find(currency => currency.isNative);
  }, [networkCurrencies]);
};

export const NetworkCurrenciesProvider = ({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}) => {
  const { chain } = useNetwork();
  const chainId = chain?.id ?? defaultSupportedNetworkId;
  const currenciesArray: Currency[] = useMemo(() => {
    if (!isSupportedNetworkId(chainId))
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      return Object.values(NetworksTokens[defaultSupportedNetworkId!]!);
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return Object.values(NetworksTokens[chainId as NetworkId]!);
  }, [chainId]);

  return (
    <NetworkCurrenciesContext.Provider value={currenciesArray}>
      {children}
    </NetworkCurrenciesContext.Provider>
  );
};
