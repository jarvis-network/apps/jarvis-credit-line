import Link from 'next/link';
import React, { ReactNode } from 'react';

export const NextLink: React.FC<{ to: string; children: ReactNode }> = ({
  to,
  children,
  ...props
}) => (
  <Link {...props} href={to}>
    <div>{children}</div>
  </Link>
);
