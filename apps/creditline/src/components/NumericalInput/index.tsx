/* eslint-disable react/require-default-props */
import React, { ChangeEvent, JSXElementConstructor, useState } from 'react';

import {
  Input,
  styled,
  FormGroup as FormGroupCompoonent,
} from '@jarvisnetwork/ui';

import { Toggle } from '@jarvisnetwork/toolkit';

const FormGroupStyled = styled(FormGroupCompoonent)`
  margin-bottom: 0px;

  .input {
    margin-top: 0;
  }
`;
const LabelWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 8px;
`;

const MaxValueWrapper = styled.div`
  color: ${props => props.theme.text.secondary};
  font-size: ${props => props.theme.font.sizes.m};
  padding: 6px 0;
  &.align-right {
    text-align: right;
  }
`;
const BalanceValueWrapper = styled.div`
  color: ${props => props.theme.text.secondary};
  font-size: ${props => props.theme.font.sizes.m};
  &.align-right {
    padding: 6px 0;
    text-align: right;
  }
`;

const InputHidePlaceHolderBox = styled.div<{ hasAmountInfo: boolean }>`
  padding: 16px;
  height: ${props => (props.hasAmountInfo ? '90.5px' : '60px')};
  display: flex;
  align-items: center;
  text-align: center;
  font-size: ${props => props.theme.font.sizes.m};
  border-radius: ${props => props.theme.borderRadius.s};
  background: ${props => props.theme.background.primary};
  color: ${props => props.theme.text.medium};
  border-radius: ${props => props.theme.borderRadius.m};
`;

const InputWrapper = styled.div`
  height: 60px;

  .input {
    &.invalid .group {
      border: 1px solid red;
    }

    .group {
      overflow: hidden;

      & > :first-child {
        padding: 0;
        border-radius: unset;

        input {
          background: transparent;
          padding-left: 10px;
        }
      }
    }
  }
`;

const InputLabel = styled.div``;

const inputRegex = /^\d*(?:\\[.])?\d*$/; // match escaped "." characters via in a non-capturing group

const escapeRegExp = (string: string): string =>
  string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
export type RenderFunction<Element = JSX.Element> = (props: any) => Element;

export const NumericalInput = React.memo(
  ({
    placeholder,
    label,
    value,
    maxValueInfo,
    balanceValueInfo,
    onUserInput,
    suffix,
    LabelComponent,
    onToggleChange,
    inputDisabled,
    inputHidePlaceHolder,
    isError,
  }: {
    placeholder: string;
    label: string;
    value: string;
    maxValueInfo?: string;
    balanceValueInfo?: string;
    inputHidePlaceHolder?: string;
    inputDisabled?: boolean;
    isError?: boolean;
    onUserInput: (val: string) => void;
    onToggleChange?: () => void;
    suffix?: React.ReactElement | JSXElementConstructor<any>;
    LabelComponent?: RenderFunction;
  }) => {
    const [toggled, setToggled] = useState(false);
    const enforcer = (nextUserInput: string) => {
      if (
        nextUserInput === '' ||
        inputRegex.test(escapeRegExp(nextUserInput))
      ) {
        onUserInput(nextUserInput);
      }
    };

    const onToggle = () => {
      setToggled(val => !val);
      if (!onToggleChange) return;
      onToggleChange();
    };
    const showInput = inputHidePlaceHolder ? toggled : true;
    const hasAmountInfo = !!maxValueInfo || !!balanceValueInfo;
    return (
      <FormGroupStyled>
        <LabelWrapper>
          {LabelComponent ? (
            <LabelComponent />
          ) : (
            <InputLabel className="label">{label}</InputLabel>
          )}
          {!!onToggleChange && <Toggle checked={toggled} onChange={onToggle} />}
          {showInput && balanceValueInfo && !onToggleChange && (
            <BalanceValueWrapper>{balanceValueInfo}</BalanceValueWrapper>
          )}
        </LabelWrapper>
        {showInput ? (
          <>
            {showInput && balanceValueInfo && !!onToggleChange && (
              <BalanceValueWrapper className="align-right">
                {balanceValueInfo}
              </BalanceValueWrapper>
            )}
            <InputWrapper>
              <Input
                placeholder={placeholder}
                className={`input${isError ? ` invalid` : ''}`}
                value={value}
                onChange={(event: ChangeEvent<HTMLInputElement>) =>
                  enforcer(event.target.value.replace(/,/g, '.'))
                }
                suffix={suffix as any}
                autoComplete="off"
                autoCorrect="off"
                pattern="^[0-9]*[.,]?[0-9]*$"
                inputMode="decimal"
                minLength={1}
                maxLength={79}
                spellCheck="false"
                type="text"
                disabled={inputDisabled}
              />
            </InputWrapper>
            {maxValueInfo && <MaxValueWrapper>{maxValueInfo}</MaxValueWrapper>}
          </>
        ) : (
          <InputHidePlaceHolderBox hasAmountInfo={hasAmountInfo}>
            {inputHidePlaceHolder}
          </InputHidePlaceHolderBox>
        )}
      </FormGroupStyled>
    );
  },
);
