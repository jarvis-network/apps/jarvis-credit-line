import React from 'react';

import { useRouter } from 'next/router';
import { Switcher } from '@jarvisnetwork/ui';

const PageSwitcher: React.FC = () => {
  const router = useRouter();
  const currentPage = router.pathname.toString().slice(1);
  const items = ['borrow', 'loans'];
  return (
    <Switcher
      items={items}
      selected={items.indexOf(currentPage === '' ? 'borrow' : currentPage)}
      onChange={value => {
        const url = value === 'borrow' ? '/' : value;
        router.push(url, undefined, { shallow: true });
      }}
    />
  );
};

export default PageSwitcher;
