import React, { createContext, useContext, useMemo } from 'react';

import { FixBigNumber, NetworkId } from '@jarvisnetwork/core-sdk';

import { useNetwork } from 'wagmi';

import { defaultSupportedNetworkId } from '@jarvisnetwork/toolkit';

import { useGetPrices } from '@/hooks/useGetPrices';
import { NetworkPricesFeed } from '@/config/priceFeedPairs';

type PriceFeed = {
  [key in string]: FixBigNumber;
};

const PriceFeedContext = createContext<PriceFeed>({});

export const usePricesFeed = () => useContext(PriceFeedContext);

export const usePriceFeed = (pair?: string): FixBigNumber | undefined => {
  const prices = useContext(PriceFeedContext);
  if (!pair) return undefined;
  return prices[pair];
};

export const PriceFeedProvider = ({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}) => {
  const { chain } = useNetwork();
  const chainId = chain?.id ?? defaultSupportedNetworkId;
  const prices = useGetPrices(NetworkPricesFeed[chainId as NetworkId]);
  const priceFeeds = useMemo(() => {
    if (!prices?.length) return {};
    const result: {
      [key in string]: FixBigNumber;
    } = {};
    for (const { pair, price } of prices) {
      if (!pair || !price) continue;
      result[pair] = price;
    }
    return result;
  }, [prices]);

  return (
    <PriceFeedContext.Provider value={priceFeeds}>
      {children}
    </PriceFeedContext.Provider>
  );
};
