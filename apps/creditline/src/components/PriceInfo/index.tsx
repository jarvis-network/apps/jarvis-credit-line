import React, { useMemo, useState } from 'react';

import { FixBigNumber } from '@jarvisnetwork/core-sdk';

import { IconButton } from '@jarvisnetwork/ui';

import { CurrentPricePairSide } from '../LoanManagementCard/interfaces';

import { InfoLine } from '../LoanManagementCard/styles';

import { useCurrentPair } from '../CurrentPairProvider';

import { usePriceFeed } from '../PriceFeedProvider';

import { DECIMAL_DIGITS_COUNT } from '@/config/app';

import { useCreditLinePairData } from '@/hooks/useCreditLinePairData';

interface CurrentPricePair {
  price: FixBigNumber | undefined;
  isReverse: boolean;
  collateral: CurrentPricePairSide | null;
  synthetic: CurrentPricePairSide | null;
}

const initialPricePair: CurrentPricePair = {
  price: undefined,
  isReverse: false,
  collateral: null,
  synthetic: null,
};

export const PriceInfo = React.memo(() => {
  const { syntheticToken, collateralToken } = useCurrentPair();
  const currentPairData = useCreditLinePairData();
  const pricePair = usePriceFeed(currentPairData?.pair);

  const [currentPricePair, setCurrentPricePair] =
    useState<CurrentPricePair>(initialPricePair);
  const syntheticIcon = syntheticToken.symbol;
  const collateralIcon = collateralToken.symbol;

  useMemo(() => {
    if (!pricePair || !syntheticToken || !collateralToken) return;

    const reversedPrice = FixBigNumber.toWei(1).divide(pricePair);

    setCurrentPricePair({
      ...currentPricePair,
      price: pricePair,
      collateral: {
        name: collateralIcon,
        symbol: collateralIcon,
        price: pricePair,
      },
      synthetic: {
        name: syntheticIcon,
        symbol: syntheticIcon,
        price: reversedPrice,
      },
    });
  }, [collateralToken, pricePair, syntheticToken]);

  const renderPriceDisplay = (): string => {
    if (!currentPricePair.collateral || !currentPricePair.synthetic) return '-';
    return currentPricePair.isReverse
      ? `1 ${collateralIcon} = ${currentPricePair.synthetic.price.toFixed(
          DECIMAL_DIGITS_COUNT,
        )} ${syntheticIcon}`
      : `1 ${syntheticIcon} = ${currentPricePair.collateral.price.toFixed(
          DECIMAL_DIGITS_COUNT,
        )} ${collateralIcon}`;
  };
  const swapPair = () => {
    setCurrentPricePair({
      ...currentPricePair,
      isReverse: !currentPricePair.isReverse,
    });
  };

  return (
    <InfoLine>
      <span className="label">Price</span>
      <span>{renderPriceDisplay()}</span>
      <IconButton icon="switchIcon" onClick={() => swapPair()} size="s" />
    </InfoLine>
  );
});
