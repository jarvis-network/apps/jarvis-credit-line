import {
  RainbowKitProvider,
  darkTheme,
  lightTheme,
  midnightTheme,
  Theme,
} from '@rainbow-me/rainbowkit';
import { ReactNode } from 'react';
import '@rainbow-me/rainbowkit/styles.css';
import merge from 'lodash.merge';

import { styled, useTheme } from '@jarvisnetwork/ui';

import { chains } from '@/config/connectors';
import { useAppTheme } from '@/state/theme/hooks';

const Wrapper = styled.div`
  height: 0%;
`;
export const RainbowKit = ({ children }: { children: ReactNode }) => {
  const selectedTheme = useAppTheme();
  const theme = useTheme();

  const myCustomTheme: Partial<Theme> =
    selectedTheme === 'dark'
      ? merge(darkTheme(), {
          colors: {
            accentColor: theme.common.primary,
            accentColorForeground: theme.button.primary.active.text,
          },
          radii: {
            actionButton: theme.borderRadius.s,
            modal: theme.borderRadius.m,
          },
          fonts: {
            body: 'Krub, sans-serif',
          },
        } as Theme)
      : selectedTheme === 'dusk'
      ? merge(midnightTheme(), {
          colors: {
            accentColor: theme.common.primary,
            accentColorForeground: theme.button.primary.active.text,
            modalBackground: theme.background.primary,
          },
          radii: {
            actionButton: theme.borderRadius.s,
            modal: theme.borderRadius.m,
          },
          fonts: {
            body: 'Krub, sans-serif',
          },
        } as Theme)
      : merge(lightTheme(), {
          colors: {
            accentColor: theme.common.primary,
            accentColorForeground: theme.button.primary.active.text,
          },
          radii: {
            actionButton: theme.borderRadius.s,
            modal: theme.borderRadius.m,
          },
          fonts: {
            body: 'Krub, sans-serif',
          },
        } as Theme);

  return (
    <Wrapper className="test">
      <RainbowKitProvider
        showRecentTransactions={false}
        chains={chains}
        theme={myCustomTheme as Theme}
      >
        {children}
      </RainbowKitProvider>
    </Wrapper>
  );
};
