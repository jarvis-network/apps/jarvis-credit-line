import React from 'react';
import { SearchBar, styled } from '@jarvisnetwork/ui';

import { Currency, Token } from '@jarvisnetwork/core-sdk';

import { useNetworkCurrencies } from '../NetworkCurrenciesProvider';

import { CardLoading } from '../CollateralCard/CardLoading';

import { useGetCollateralTokensList } from '@/hooks/useGetCollateralTokensList';
import CollateralCard from '@/components/CollateralCard';

import { useSearchBar } from '@/hooks/useSearchBar';
import { useSelectedSynthetic } from '@/state/assets/hooks';
import { SyntheticToken } from '@/enums';

import { useSyntheticsCreditLinesDataWithUserLoanPosition } from '@/hooks/useActiveLoan';

const SearchContainer = styled.div`
  margin-top: 24px;
  @media (min-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    width: 30rem;
    margin-bottom: 42px;
  }
`;

const SearchInputContainer = styled.div`
  display: flex;
  max-width: 485px;
  margin-top: 24px;
  height: 50px;
`;

const SearchTitle = styled.div`
  font: normal normal normal 25px/32px 'Krub';
  font-weight: normal;
`;

const CardContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-item: center;
  gap: 32px;
  justify-content: center;
  margin-top: 32px;
`;

const SelectCollateral = () => {
  const assets = useNetworkCurrencies();
  const selectedSynthetic = useSelectedSynthetic();
  const collateralCurrencies = useGetCollateralTokensList(
    selectedSynthetic as SyntheticToken,
  );

  const userCreditLinesData =
    useSyntheticsCreditLinesDataWithUserLoanPosition(selectedSynthetic);
  const { searchBarProps, searchResult: collateralsFound } =
    useSearchBar<Currency>(
      ['symbol'],
      'Try "USDC"',
      collateralCurrencies.sort((a, b) =>
        b.symbol.toLowerCase().localeCompare(a.symbol.toLowerCase()),
      ),
    );

  return (
    <>
      <SearchContainer>
        <SearchTitle>
          Choose your Collateral to borrow <strong>{selectedSynthetic}</strong>
        </SearchTitle>
        <SearchInputContainer>
          <SearchBar {...searchBarProps} />
        </SearchInputContainer>
      </SearchContainer>
      <CardContainer>
        {collateralsFound?.length && userCreditLinesData?.length ? (
          collateralsFound.map(collateral => (
            <CollateralCard
              selectedSynthetic={
                assets.find(
                  asset => asset.symbol === selectedSynthetic,
                ) as Token
              }
              collateral={collateral as Token}
              creditLineData={userCreditLinesData.find(
                data => data.collateral === collateral.symbol,
              )}
              key={collateral.symbol}
            />
          ))
        ) : (
          <CardLoading />
        )}
      </CardContainer>
    </>
  );
};

export default SelectCollateral;
