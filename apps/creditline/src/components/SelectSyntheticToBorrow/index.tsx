import React from 'react';

import { SearchBar, styled } from '@jarvisnetwork/ui';

import { Currency } from '@jarvisnetwork/core-sdk';

import BorrowCard from '@/components/BorrowCard';
import { BorrowCardLoading } from '@/components/BorrowCard/BorrowCardLoading';
import { useGetSyntheticTokens } from '@/hooks/useGetSyntheticTokens';
import { useSearchBar } from '@/hooks/useSearchBar';

const SearchContainer = styled.div`
  margin-top: 24px;
  @media (min-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    width: 30rem;
    margin-bottom: 42px;
  }
`;

const SearchInputContainer = styled.div`
  display: flex;
  max-width: 485px;
  margin-top: 24px;
  height: 50px;
`;

const SearchTitle = styled.div`
  font: normal normal normal 25px/32px 'Krub';
`;

const CardContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-item: center;
  gap: 24px;
  justify-content: center;
  margin-top: 32px;
`;

const LoaderWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

const SelectSyntheticToBorrow = () => {
  const syntheticTokens = useGetSyntheticTokens();

  const { searchBarProps, searchResult: SyntheticsFound } =
    useSearchBar<Currency>(['symbol'], 'Try "jEUR"', syntheticTokens);

  return (
    <>
      <SearchContainer>
        <SearchTitle>Choose jFIAT to borrow</SearchTitle>
        <SearchInputContainer>
          <SearchBar {...searchBarProps} />
        </SearchInputContainer>
      </SearchContainer>
      <CardContainer>
        {SyntheticsFound?.length ? (
          SyntheticsFound.map(token => (
            <BorrowCard token={token} key={token.symbol} />
          ))
        ) : (
          <BorrowCardLoading />
        )}
      </CardContainer>
    </>
  );
};

export default SelectSyntheticToBorrow;
