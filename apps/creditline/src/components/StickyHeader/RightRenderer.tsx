import React, { useEffect, useState } from 'react';
import {
  useWindowSize,
  styled,
  Skeleton,
  NetworkImageDropdownItem,
  ThemeNameType,
  AccountSummary,
} from '@jarvisnetwork/ui';
import { useAccount, useNetwork, useSwitchNetwork } from 'wagmi';

import { NetworkId } from '@jarvisnetwork/core-sdk';
import {
  defaultSupportedNetworkId,
  formatWalletAddress,
  isSupportedNetworkId,
  NETWORKS,
  supportedNetworkIds,
} from '@jarvisnetwork/toolkit';
import { useRouter } from 'next/router';

import { useDispatch } from 'react-redux';

import { useRedirectToPreviousPage } from '@/hooks/useRedirectTo';
import { setSelectedSynthetic } from '@/state/assets/reducer';

const Container = styled.div`
  height: 40px;
  width: 100%;

  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    width: 300px;
  }
`;

interface RightRendererProps {
  isUnsupportedNetworkId: boolean;
  setIsUnsupportedNetworkId: (isUnsupportedNetworkId: boolean) => void;
  toggleAuthModal: () => void;
  toggleAccountSummaryModal: () => void;
  setTheme: (setTheme: ThemeNameType) => void;
}

export const RightRenderer = ({
  isUnsupportedNetworkId,
  setIsUnsupportedNetworkId,
  toggleAuthModal,
  toggleAccountSummaryModal,
  setTheme,
}: RightRendererProps): JSX.Element => {
  const router = useRouter();
  const dispatch = useDispatch();
  const redirectToPreviousPage = useRedirectToPreviousPage();
  const { innerWidth } = useWindowSize();
  const { address } = useAccount({
    onDisconnect() {
      if (!!router.query.tokenA || router.query.tokenA) {
        redirectToPreviousPage();
      }
      dispatch(setSelectedSynthetic(undefined));
    },
  });
  const { switchNetwork } = useSwitchNetwork({
    onSuccess() {
      if (!!router.query.tokenA || router.query.tokenA) {
        redirectToPreviousPage();
      }
      dispatch(setSelectedSynthetic(undefined));
    },
  });
  const { chain } = useNetwork();
  const chainId = chain?.id;
  const [desiredChainId, setDesiredChainId] = useState<number>(
    defaultSupportedNetworkId!,
  );
  useEffect(() => {
    if (chainId) {
      if (isSupportedNetworkId(chainId)) {
        setIsUnsupportedNetworkId(false);
        setDesiredChainId(chainId);
      } else {
        setIsUnsupportedNetworkId(true);
      }
    }
  }, [chainId]);

  const networkList: NetworkImageDropdownItem[] = supportedNetworkIds.map(
    (key: number) => ({
      networkId: key,
      iconName: NETWORKS[key as NetworkId]?.iconName
        ? NETWORKS[key as NetworkId]!.iconName
        : 'Eth',
      testnetIdentifier: NETWORKS[key as NetworkId]?.testnetIdentifier,
      onClick: () => {
        switchNetwork?.(key);
      },
    }),
  );

  const settings = [
    {
      name: 'Learn',
      key: 'learn',
      onClick: () =>
        window.open('https://learn.jarvis.network/', '_blank', 'noopener'),
    },
    {
      name: 'Telegram',
      key: 'Telegram',
      onClick: () =>
        window.open('https://t.me/jarvisnetwork', '_blank', 'noopener'),
    },
    {
      name: 'Twitter',
      key: 'Twitter',
      onClick: () =>
        window.open('https://twitter.com/Jarvis_Network', '_blank', 'noopener'),
    },
    {
      name: 'Discord',
      key: 'Discord',
      onClick: () =>
        window.open(
          'https://discord.com/invite/YSPpBWH6fa',
          '_blank',
          'noopener',
        ),
    },
  ];

  const handleLogin = () => {
    toggleAuthModal();
  };

  const content = (
    <AccountSummary
      onAccount={() => toggleAccountSummaryModal()}
      wallet={address ? formatWalletAddress(address) : ''}
      onThemeChange={theme => setTheme(theme)}
      settingMenu={settings}
      networkList={networkList}
      isUnsupportedNetworkId={isUnsupportedNetworkId}
      selectedNetworkId={desiredChainId}
      contentOnTop={innerWidth <= 1024}
      onLogin={handleLogin}
    />
  );

  return (
    <Container>
      <Skeleton>{content}</Skeleton>
    </Container>
  );
};
