import React, { ReactNode } from 'react';
import { styled, OnDesktop, useTheme } from '@jarvisnetwork/ui';
import Image from 'next/image';

import { useRouter } from 'next/router';

import { useDispatch } from 'react-redux';

import { RightRenderer } from './RightRenderer';

import darkLogo from '@/images/jarvis-dark-logo.svg';
import lightLogo from '@/images/jarvis-logo.svg';

import { NextLink } from '@/components/NextLink';
import { Banner } from '@/components/Banner';
import PageSwitcher from '@/components/PageSwitcher';
import { useReduxSelector } from '@/state/useReduxSelector';
import { setIsUnsupportedNetworkId } from '@/state/app/reducer';
import { setTheme } from '@/state/theme/reducer';
import {
  useToggleAccountSummaryModal,
  useToggleAuthenticationModal,
} from '@/state/app/hooks';

const Container = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const HeaderContainer = styled.div<{ isAuthModalVisible: boolean }>`
  position: absolute;
  z-index: 90;
  top: 0;
  bottom: auto;
  left: 0;
  right: 0;
  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    ${props => (props.isAuthModalVisible ? 'display: none;' : '')}
    position: fixed;
    top: auto;
    bottom: 0;
    left: 0;
    right: 0;
    background: ${props => props.theme.background.primary};
    border-top: 1px solid ${props => props.theme.border.primary};
    z-index: 100;
  }
`;
const LogoContainer = styled.div`
  display: flex;
  @media screen and (max-width: 370px) {
    display: none;
  }
`;
const CustomHeader = styled.div`
  padding: 0px 20vw;
  height: 90px;
  align-items: center;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  justify-content: space-between;
  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    grid-template-columns: auto auto;
    height: 52px;
    padding: 0px 3vw;
  }
`;

const Content = styled.div`
  overflow-y: auto;
  overflow-x: hidden;
  flex: 1;

  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    padding-bottom: 13vh;
  }
`;

const MiddleSectionContainer = styled.div``;

const RightSectionContainer = styled.div`
  display: grid;
  grid-template-columns: auto auto;
  justify-content: end;
`;

export const StickyHeader = ({ children }: { children: ReactNode }) => {
  const dispatch = useDispatch();
  const { name: theme } = useTheme();
  const toggleAuthModal = useToggleAuthenticationModal();
  const toggleAccountSummaryModal = useToggleAccountSummaryModal();
  const { isUnsupportedNetworkId } = useReduxSelector(state => state.app);
  const renderLogo = theme === 'light' ? darkLogo : lightLogo;
  const router = useRouter();
  const currentPage = router.pathname;
  const pageSwitcherVisibleRoutes = ['/', '/loans'];
  const pageSwitcherVisible = pageSwitcherVisibleRoutes.includes(currentPage);
  return (
    <Container>
      <HeaderContainer isAuthModalVisible={false}>
        <Banner
          message="Credit Lines v2 have not been audited. Their code has been reviewed internally."
          type="Warning"
          emoji="⚠️"
        />
        <CustomHeader>
          <LogoContainer>
            <NextLink to="/">
              <Image
                alt="Logo"
                src={renderLogo}
                width={23}
                height={30}
                className="header-logo"
                priority
              />
            </NextLink>
          </LogoContainer>
          <OnDesktop>
            <MiddleSectionContainer>
              {pageSwitcherVisible ? <PageSwitcher /> : ''}
            </MiddleSectionContainer>
          </OnDesktop>
          <RightSectionContainer>
            <RightRenderer
              setIsUnsupportedNetworkId={val =>
                dispatch(setIsUnsupportedNetworkId(val))
              }
              isUnsupportedNetworkId={isUnsupportedNetworkId}
              toggleAuthModal={toggleAuthModal}
              toggleAccountSummaryModal={toggleAccountSummaryModal}
              setTheme={selectedTheme => dispatch(setTheme(selectedTheme))}
            />
          </RightSectionContainer>
        </CustomHeader>
      </HeaderContainer>
      <Content className="content">{children}</Content>
    </Container>
  );
};
