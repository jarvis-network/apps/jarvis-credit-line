import React, { useEffect, useMemo, useState } from 'react';

import { useDispatch } from 'react-redux';

import { LoadingModal, LoadingModalStatus, styled } from '@jarvisnetwork/ui';

import { useBlockNumber, useNetwork, useProvider } from 'wagmi';

import { defaultSupportedNetworkId } from '@jarvisnetwork/toolkit';

import { getMessage } from '../LoadingTransactionModal/helpers';

import { ApproveTokenTransactionContentModal } from '../LoadingTransactionModal/ApproveTokenContent';

import { CreateLoanModalContent } from '../LoadingTransactionModal/CreateLoanContent';

import { RedeemModalContent } from '../LoadingTransactionModal/RedeemContent';

import { RepayModalContent } from '../LoadingTransactionModal/RepayContent';

import { DepositModalContent } from '../LoadingTransactionModal/DepositContent';

import { WithdrawModalContent } from '../LoadingTransactionModal/WithdrawContent';

import { CloseModalContent } from '../LoadingTransactionModal/CloseLoanContent';

import { SuccessModalContent } from '../LoadingTransactionModal/SuccessContent';

import { TransactionType } from '@/types';

import { TRANSACTION_STATUSES } from '@/components/BorrowPage/constants';
import { useLastTransaction } from '@/state/transactions/hooks';
import {
  useModalIsOpen,
  useSetPanelType,
  useToggleTransactionModal,
} from '@/state/app/hooks';
import { finalizeTransaction } from '@/state/transactions/reducer';
import { useRedirectToHome } from '@/hooks/useRedirectTo';
import { setSelectedSynthetic } from '@/state/assets/reducer';
import { ApplicationModal } from '@/state/app/reducer';
import { PANEL_TYPE } from '@/enums';

const LoadingModalWrapper = styled.div`
  color: ${props => props.theme.text.primary};
`;
const ModalContentWrapperStyled = styled.div`
  width: calc(100% - 2rem);
  margin: auto 16px 16px 16px;
  background: ${props => props.theme.background.primary};
  border-radius: ${props => props.theme.borderRadius.l};
  padding: 1rem;
`;

const TransactionModalHandler = () => {
  const dispatch = useDispatch();
  const redirectToHomePanel = useSetPanelType(PANEL_TYPE.HOME);
  const isOpen = useModalIsOpen(ApplicationModal.TRANSACTION);
  const toggleTransactionModal = useToggleTransactionModal();
  const { chain } = useNetwork();

  const chainId = chain?.id ?? defaultSupportedNetworkId;

  const provider = useProvider({ chainId });
  const { data: blockNumber } = useBlockNumber({
    watch: true,
  });
  const redirectToHome = useRedirectToHome();
  const [txStatus, setTxStatus] = useState<LoadingModalStatus>(
    LoadingModalStatus.pending,
  );
  const [txCompleted, setTxCompleted] = useState(false);

  const lastTransaction = useLastTransaction();

  useEffect(() => {
    if (isOpen) {
      setTxStatus(LoadingModalStatus.pending);
      setTxCompleted(false);
    }
  }, [isOpen]);

  useMemo(() => {
    if (!provider || !lastTransaction || txCompleted) {
      return;
    }

    provider
      .getTransactionReceipt(lastTransaction.hash)
      .then(receipt => {
        if (!receipt) {
          return;
        }
        if (
          receipt.status === TRANSACTION_STATUSES.ERROR ||
          receipt.status === TRANSACTION_STATUSES.SUCCESS
        ) {
          setTxStatus(receipt.status);
          setTxCompleted(true);

          dispatch(
            finalizeTransaction({
              hash: lastTransaction.hash,
              receipt: {
                blockHash: receipt.blockHash,
                blockNumber: receipt.blockNumber,
                contractAddress: receipt.contractAddress,
                from: receipt.from,
                status: receipt.status,
                to: receipt.to,
                transactionHash: receipt.transactionHash,
                transactionIndex: receipt.transactionIndex,
              },
            }),
          );
        }
      })
      .catch(err => {
        console.error('Erro: ', err);
      });
  }, [
    txCompleted,
    provider,
    lastTransaction,
    isOpen,
    dispatch,
    blockNumber,
    redirectToHome,
  ]);

  const closeModal = () => {
    toggleTransactionModal();
    if (txStatus === LoadingModalStatus.success)
      if (lastTransaction?.info.type === TransactionType.CLOSE) {
        dispatch(setSelectedSynthetic(undefined));
        redirectToHome();
      } else if (lastTransaction?.info.type !== TransactionType.APPROVAL) {
        redirectToHomePanel();
      }
    setTxStatus(LoadingModalStatus.pending);
    setTxCompleted(false);
  };

  if (!isOpen || !lastTransaction) {
    return null;
  }

  const { info } = lastTransaction;

  const renderTransactionModalContent = () => {
    if (txStatus === LoadingModalStatus.success) {
      return <SuccessModalContent type={info.type} />;
    }
    if (
      info.type === TransactionType.CREATE_LOAN ||
      info.type === TransactionType.BORROW
    ) {
      return (
        <CreateLoanModalContent
          synthetic={{
            value: info.syntheticAmount,
            symbol: info.syntheticSymbol,
          }}
          collateral={{
            value: info.collateralAmount,
            symbol: info.collateralSymbol,
          }}
        />
      );
    }
    if (info.type === TransactionType.APPROVAL) {
      return (
        <ApproveTokenTransactionContentModal tokenSymbol={info.tokenSymbol} />
      );
    }
    if (info.type === TransactionType.REDEEM) {
      return (
        <RedeemModalContent
          synthetic={{
            value: info.syntheticAmount,
            symbol: info.syntheticSymbol,
          }}
          collateral={{
            value: info.collateralAmount,
            symbol: info.collateralSymbol,
          }}
        />
      );
    }
    if (info.type === TransactionType.CLOSE) {
      return (
        <CloseModalContent
          synthetic={{
            value: info.syntheticAmount,
            symbol: info.syntheticSymbol,
          }}
          collateral={{
            value: info.collateralAmount,
            symbol: info.collateralSymbol,
          }}
        />
      );
    }
    if (info.type === TransactionType.REPAY) {
      return (
        <RepayModalContent
          synthetic={{
            value: info.syntheticAmount,
            symbol: info.syntheticSymbol,
          }}
        />
      );
    }
    if (info.type === TransactionType.DEPOSIT) {
      return (
        <DepositModalContent
          collateral={{
            value: info.collateralAmount,
            symbol: info.collateralSymbol,
          }}
        />
      );
    }
    if (info.type === TransactionType.WITHDRAW) {
      return (
        <WithdrawModalContent
          collateral={{
            value: info.collateralAmount,
            symbol: info.collateralSymbol,
          }}
        />
      );
    }
    return <div>-</div>;
  };

  return (
    <LoadingModalWrapper>
      <LoadingModal
        title={lastTransaction.info.type}
        onClose={closeModal}
        text={getMessage(txStatus)}
        status={txStatus}
        isOverlayCloseActivated={
          txStatus === LoadingModalStatus.error ||
          txStatus === LoadingModalStatus.success
        }
      >
        <ModalContentWrapperStyled>
          {renderTransactionModalContent()}
        </ModalContentWrapperStyled>
      </LoadingModal>
    </LoadingModalWrapper>
  );
};

export default TransactionModalHandler;
