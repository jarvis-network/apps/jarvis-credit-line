import { CurrencyWithBalance } from '@jarvisnetwork/core-sdk';

import React, { useMemo, useContext, createContext } from 'react';

import { useNetworkCurrencies } from '../NetworkCurrenciesProvider';

import { useCurrenciesBalance } from '@/hooks/useCurrenciesBalance';

const UserCurrenciesBalanceContext = createContext<CurrencyWithBalance[]>([]);

export const useAllUserCurrenciesBalance = () =>
  useContext(UserCurrenciesBalanceContext);

export const useUserCurrenciesBalance = (tokensSymbol: string[] | []) => {
  if (!tokensSymbol || !tokensSymbol.length) return undefined;
  const allCurrenciesBalance = useContext(UserCurrenciesBalanceContext);
  return useMemo(
    () =>
      allCurrenciesBalance.filter(({ currency }) =>
        tokensSymbol.includes(currency.symbol as never),
      ),
    [allCurrenciesBalance, tokensSymbol],
  );
};

export const useUserCurrencyBalance = (tokenSymbol?: string) => {
  const allCurrenciesBalance = useContext(UserCurrenciesBalanceContext);
  if (!tokenSymbol) return { currency: undefined, balance: undefined };
  return allCurrenciesBalance.find(
    ({ currency }) => currency.symbol === tokenSymbol,
  );
};

export const UserCurrenciesBalanceProvider = ({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}) => {
  const currenciesArrayList = useNetworkCurrencies();
  const balances = useCurrenciesBalance(currenciesArrayList);
  const userBalances = useMemo(() => {
    if (!balances) return [];
    return balances;
  }, [balances]);

  return (
    <UserCurrenciesBalanceContext.Provider value={userBalances}>
      {children}
    </UserCurrenciesBalanceContext.Provider>
  );
};
