import { Checkbox } from '@jarvisnetwork/ui';

import {
  IconWrapper,
  SubmitButton,
  TextWrapper,
} from '@/components/LoanManagementCard/styles';

const WelcomeCard = ({
  onNext,
  onCheckboxChange,
  content,
}: {
  onNext: () => void;
  onCheckboxChange: () => void;
  content: () => JSX.Element;
}) => (
  <>
    <IconWrapper>
      <img src="/images/girl-sculpture-2.svg" alt="sculpture" />
    </IconWrapper>
    <TextWrapper>
      {content()}
      <div>
        <Checkbox
          name="hide_next_time"
          onChange={() => {
            onCheckboxChange();
          }}
        >
          Do not show again
        </Checkbox>
      </div>
    </TextWrapper>

    <SubmitButton size="s" type="success" onClick={onNext}>
      Next
    </SubmitButton>
  </>
);

export default WelcomeCard;
