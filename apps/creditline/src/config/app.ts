export const backgroundMap = {
  light: '/images/light-mode-background.svg',
  dark: '/images/dark-mode-background.svg',
  dusk: '/images/night-mode-background.svg',
};

export type themeType = keyof typeof backgroundMap;

export const DECIMAL_DIGITS_COUNT = 5;
