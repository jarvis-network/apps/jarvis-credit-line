import { getDefaultWallets } from '@rainbow-me/rainbowkit';

import { jsonRpcProvider } from 'wagmi/providers/jsonRpc';
import { publicProvider } from 'wagmi/providers/public';
import {
  arbitrum,
  arbitrumGoerli,
  avalanche,
  avalancheFuji,
  bsc,
  gnosis,
  optimism,
  polygon,
  polygonMumbai,
} from 'wagmi/chains';
import { Chain, configureChains, createClient } from 'wagmi';
import {
  defaultSupportedNetworkId,
  supportedNetworkIds,
} from '@jarvisnetwork/toolkit';

const chainIdToConnectors: { [key: number]: Chain } = {
  10: optimism,
  56: bsc,
  100: gnosis,
  137: polygon,
  42161: arbitrum,
  43113: avalancheFuji,
  43114: avalanche,
  80001: polygonMumbai,
  421613: arbitrumGoerli,
};

const supportedChains = [
  chainIdToConnectors[defaultSupportedNetworkId],
  ...supportedNetworkIds
    .filter(id => id !== defaultSupportedNetworkId)
    .map((chainId: number) => chainIdToConnectors[chainId])
    .filter(val => !!val),
];

export const { chains, provider } = configureChains(supportedChains, [
  jsonRpcProvider({
    rpc: chain => {
      if (chain && chain.id === 10) {
        return {
          http: 'https://endpoints.omniatech.io/v1/op/mainnet/public',
        };
      }
      if (chain && chain.id === 137) {
        return {
          http: 'https://polygon-rpc.com',
        };
      }
      if (chain && chain.id === 56) {
        return {
          http: 'https://endpoints.omniatech.io/v1/bsc/mainnet/public',
        };
      }
      if (chain && chain.id === 42161) {
        return {
          http: 'https://endpoints.omniatech.io/v1/arbitrum/one/public',
        };
      }
      if (chain && chain.id === 80001) {
        return {
          http: 'https://endpoints.omniatech.io/v1/matic/mumbai/public',
        };
      }
      return {
        http: chain.rpcUrls.default.http[0],
      };
    },
  }),
  publicProvider(),
]);

const { connectors } = getDefaultWallets({
  appName: 'Jarvis Money',
  chains,
});

export const wagmiAppClient = createClient({
  autoConnect: true,
  connectors,
  provider,
});
