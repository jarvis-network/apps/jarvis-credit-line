/* eslint-disable camelcase */
import { ChainId, FixBigNumber, NetworkId } from '@jarvisnetwork/core-sdk';

import { BasePairInfo } from './pairsInfo';

import { BSC_CREDIT_LINE } from './network/bsc';

import { POLYGON_CREDIT_LINE } from './network/polygon';

import { MUMBAI_CREDIT_LINE } from './network/mumbai';
import { ARBITRUM_CREDIT_LINE } from './network/arbitrum';
import { OPTIMISM_CREDIT_LINE } from './network/optimism';

import { Collateral, SyntheticToken } from '@/enums';

import { PairLike } from '@/types';

export type CreditLineData = BasePairInfo & {
  address: string;
  collateralRequirement: FixBigNumber;
  feePercentage: FixBigNumber;
  liquidationReward: FixBigNumber;
  minSponsorTokens: FixBigNumber;
  capMint?: FixBigNumber;
  globalPosition?: {
    totalCollateral: FixBigNumber;
    totalBorrowed: FixBigNumber;
  };
};

export type ContractListInterface = {
  CreditLineRegistry: string;
  CreditLine: {
    [key in PairLike<
      SyntheticToken,
      Collateral | SyntheticToken,
      '_'
    >]?: CreditLineData;
  };
};

export const CREDIT_LINE: {
  [chainId in NetworkId]?: ContractListInterface;
} = {
  [ChainId.arbitrum]: { ...ARBITRUM_CREDIT_LINE },
  [ChainId.optimism]: { ...OPTIMISM_CREDIT_LINE },
  [ChainId.polygon]: { ...POLYGON_CREDIT_LINE },
  [ChainId.mumbai]: { ...MUMBAI_CREDIT_LINE },
  [ChainId.bsc]: { ...BSC_CREDIT_LINE },
};
