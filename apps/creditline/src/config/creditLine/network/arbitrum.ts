/* eslint-disable camelcase */
import { FixBigNumber } from '@jarvisnetwork/core-sdk';

import { ContractListInterface } from '..';
import {
  jEUR_USDC_BASE_INFO,
  jEUR_WBTC_BASE_INFO,
  jEUR_WETH_BASE_INFO,
  jEUR_wstETH_BASE_INFO,
} from '../pairsInfo';

export const ARBITRUM_CREDIT_LINE: ContractListInterface = {
  CreditLineRegistry: '0x58741E9137a8aF31955D42AEc99a1aD4771EeC23',
  CreditLine: {
    jEUR_USDC: {
      ...jEUR_USDC_BASE_INFO,
      address: '0x5e74fD5bb430c67Fb3b6c318416B3Eb025109bf4',
      collateralRequirement: FixBigNumber.toWei(1.05),
      feePercentage: FixBigNumber.toWei(0.005),
      minSponsorTokens: FixBigNumber.toWei(5),
      liquidationReward: FixBigNumber.toWei(0.5),
    },
    jEUR_WBTC: {
      ...jEUR_WBTC_BASE_INFO,
      address: '0x8Fb089E07F0802569DAD80AE93963EA0F4b13450',
      collateralRequirement: FixBigNumber.toWei(1.25),
      feePercentage: FixBigNumber.toWei(0.005),
      minSponsorTokens: FixBigNumber.toWei(5),
      liquidationReward: FixBigNumber.toWei(0.2),
    },
    jEUR_WETH: {
      ...jEUR_WETH_BASE_INFO,
      address: '0xF2fC44feACDe3D4F098d86580063a73BD5F25EA7',
      collateralRequirement: FixBigNumber.toWei(1.2),
      feePercentage: FixBigNumber.toWei(0.005),
      minSponsorTokens: FixBigNumber.toWei(5),
      liquidationReward: FixBigNumber.toWei(0.1),
    },
    jEUR_wstETH: {
      ...jEUR_wstETH_BASE_INFO,
      address: '0x78DCc3160de771aE43F100e985779EA6F560b266',
      collateralRequirement: FixBigNumber.toWei(1.2),
      feePercentage: FixBigNumber.toWei(0.005),
      minSponsorTokens: FixBigNumber.toWei(5),
      liquidationReward: FixBigNumber.toWei(0.1),
    },
  },
};
