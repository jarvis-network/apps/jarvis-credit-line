/* eslint-disable camelcase */
import { FixBigNumber } from '@jarvisnetwork/core-sdk';

import { ContractListInterface } from '..';
import {
  jCHF_USDC_BASE_INFO,
  jEUR_USDC_BASE_INFO,
  jGBP_USDC_BASE_INFO,
} from '../pairsInfo';

export const MUMBAI_CREDIT_LINE: ContractListInterface = {
  CreditLineRegistry: '0x2ff9a432E0985803E3Bb8167935c20BBBB2dEE66',
  CreditLine: {
    jEUR_USDC: {
      ...jEUR_USDC_BASE_INFO,
      address: '0xd4b88233C0b7Ac5B0b1f4BB6B352Dd2e485Cb8Ad',
      collateralRequirement: FixBigNumber.toWei(1.05),
      feePercentage: FixBigNumber.toWei(0.00165),
      minSponsorTokens: FixBigNumber.toWei(10),
      liquidationReward: FixBigNumber.toWei(0.6),
    },
    jGBP_USDC: {
      ...jGBP_USDC_BASE_INFO,
      address: '0x5eB5c5eF8DF8C3DF29b4A9B9e632e6e4dd1b1f42',
      collateralRequirement: FixBigNumber.toWei(1.05),
      feePercentage: FixBigNumber.toWei(0.00165),
      minSponsorTokens: FixBigNumber.toWei(10),
      liquidationReward: FixBigNumber.toWei(0.6),
    },
    jCHF_USDC: {
      ...jCHF_USDC_BASE_INFO,
      address: '0x7B043c82F2f62240a86cdA379f6894655Ea9969b',
      collateralRequirement: FixBigNumber.toWei(1.05),
      feePercentage: FixBigNumber.toWei(0.00165),
      minSponsorTokens: FixBigNumber.toWei(10),
      liquidationReward: FixBigNumber.toWei(0.6),
    },
  },
};
