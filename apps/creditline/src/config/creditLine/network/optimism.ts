/* eslint-disable camelcase */
import { FixBigNumber } from '@jarvisnetwork/core-sdk';

import { ContractListInterface } from '..';
import {
  jEUR_OP_BASE_INFO,
  jEUR_USDC_BASE_INFO,
  jEUR_WBTC_BASE_INFO,
  jEUR_WETH_BASE_INFO,
  jEUR_wstETH_BASE_INFO,
} from '../pairsInfo';

export const OPTIMISM_CREDIT_LINE: ContractListInterface = {
  CreditLineRegistry: '0xBD8FdDa057de7e0162b7A386BeC253844B5E07A5',
  CreditLine: {
    jEUR_USDC: {
      ...jEUR_USDC_BASE_INFO,
      address: '0xA3d49d2Fc84497F4eb5A1A3EA54380eceF8a7eF7',
      collateralRequirement: FixBigNumber.toWei(1.05),
      feePercentage: FixBigNumber.toWei(0.005),
      minSponsorTokens: FixBigNumber.toWei(10),
      liquidationReward: FixBigNumber.toWei(0.5),
    },
    jEUR_WBTC: {
      ...jEUR_WBTC_BASE_INFO,
      address: '0xE53dB5B21262F142F0d78Bb31B85c4b1Ee9faAbD',
      collateralRequirement: FixBigNumber.toWei(1.25),
      feePercentage: FixBigNumber.toWei(0.005),
      minSponsorTokens: FixBigNumber.toWei(5),
      liquidationReward: FixBigNumber.toWei(0.2),
    },
    jEUR_WETH: {
      ...jEUR_WETH_BASE_INFO,
      address: '0x3d8DA6b03DB2B50dA1850c1D237b73092f7c977d',
      collateralRequirement: FixBigNumber.toWei(1.2),
      feePercentage: FixBigNumber.toWei(0.005),
      minSponsorTokens: FixBigNumber.toWei(5),
      liquidationReward: FixBigNumber.toWei(0.2),
    },
    jEUR_wstETH: {
      ...jEUR_wstETH_BASE_INFO,
      address: '0x20981e320761BEB3b49F2fdEBb075b4d917bbAAc',
      collateralRequirement: FixBigNumber.toWei(1.2),
      feePercentage: FixBigNumber.toWei(0.005),
      minSponsorTokens: FixBigNumber.toWei(5),
      liquidationReward: FixBigNumber.toWei(0.2),
    },
    jEUR_OP: {
      ...jEUR_OP_BASE_INFO,
      address: '0x316E8d1C8aC44ce235c5e7c4a9c1d476F4B83456',
      collateralRequirement: FixBigNumber.toWei(1.5),
      feePercentage: FixBigNumber.toWei(0.005),
      minSponsorTokens: FixBigNumber.toWei(5),
      liquidationReward: FixBigNumber.toWei(0.2),
    },
  },
};
