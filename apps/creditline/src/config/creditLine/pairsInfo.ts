/* eslint-disable camelcase */
import { Collateral, SyntheticToken } from '@/enums';

export type BasePairInfo = {
  pair: string;
  inversePair: string;
  synthetic: SyntheticToken;
  collateral: Collateral | SyntheticToken;
  syntheticDollarPair: string;
  collateralDollarPair: string;
};

export const jEUR_USDC_BASE_INFO = {
  pair: 'EURUSD',
  inversePair: 'USDEUR',
  synthetic: SyntheticToken.jEUR,
  collateral: Collateral.USDC,
  syntheticDollarPair: 'EURUSD',
  collateralDollarPair: 'USDCUSD',
};
export const jEUR_BUSD_BASE_INFO = {
  pair: 'EURUSD',
  inversePair: 'USDEUR',
  synthetic: SyntheticToken.jEUR,
  collateral: Collateral.BUSD,
  syntheticDollarPair: 'EURUSD',
  collateralDollarPair: 'BUSDUSD',
};
export const jEUR_WBNB_BASE_INFO = {
  pair: 'EURBNB',
  inversePair: 'BNBEUR',
  synthetic: SyntheticToken.jEUR,
  collateral: Collateral.WBNB,
  syntheticDollarPair: 'EURUSD',
  collateralDollarPair: 'BNBUSD',
};
export const jEUR_WBTC_BASE_INFO = {
  pair: 'EURBTC',
  inversePair: 'BTCEUR',
  synthetic: SyntheticToken.jEUR,
  collateral: Collateral.WBTC,
  syntheticDollarPair: 'EURUSD',
  collateralDollarPair: 'BTCUSD',
};

export const jBRL_WBTC_BASE_INFO = {
  pair: 'BRLBTC',
  inversePair: 'BTCBRL',
  synthetic: SyntheticToken.jBRL,
  collateral: Collateral.WBTC,
  syntheticDollarPair: 'BRLUSD',
  collateralDollarPair: 'BTCUSD',
};

export const jEUR_jBRL_BASE_INFO = {
  pair: 'EURBRL',
  inversePair: 'BRLEUR',
  synthetic: SyntheticToken.jEUR,
  collateral: SyntheticToken.jBRL,
  syntheticDollarPair: 'EURUSD',
  collateralDollarPair: 'BRLUSD',
};

export const jEUR_jCAD_BASE_INFO = {
  pair: 'EURCAD',
  inversePair: 'CADEUR',
  synthetic: SyntheticToken.jEUR,
  collateral: SyntheticToken.jCAD,
  syntheticDollarPair: 'EURUSD',
  collateralDollarPair: 'CADUSD',
};
export const jEUR_jCHF_BASE_INFO = {
  pair: 'EURCHF',
  inversePair: 'CHFEUR',
  synthetic: SyntheticToken.jEUR,
  collateral: SyntheticToken.jCHF,
  syntheticDollarPair: 'EURUSD',
  collateralDollarPair: 'CHFUSD',
};
export const jEUR_jGOLD_BASE_INFO = {
  pair: 'EURXAU',
  inversePair: 'XAUEUR',
  synthetic: SyntheticToken.jEUR,
  collateral: SyntheticToken.jGOLD,
  syntheticDollarPair: 'EURUSD',
  collateralDollarPair: 'XAUUSD',
};
export const jBRL_jGOLD_BASE_INFO = {
  pair: 'BRLXAU',
  inversePair: 'XAUBRL',
  synthetic: SyntheticToken.jBRL,
  collateral: SyntheticToken.jGOLD,
  syntheticDollarPair: 'BRLUSD',
  collateralDollarPair: 'XAUUSD',
};
export const jEUR_jGBP_BASE_INFO = {
  pair: 'EURGBP',
  inversePair: 'GBPEUR',
  synthetic: SyntheticToken.jEUR,
  collateral: SyntheticToken.jGBP,
  syntheticDollarPair: 'EURUSD',
  collateralDollarPair: 'GBPUSD',
};

export const jEUR_OP_BASE_INFO = {
  pair: 'EUROP',
  inversePair: 'OPEUR',
  synthetic: SyntheticToken.jEUR,
  collateral: Collateral.OP,
  syntheticDollarPair: 'EURUSD',
  collateralDollarPair: 'OPUSD',
};
export const jEUR_WMATIC_BASE_INFO = {
  pair: 'EURMATIC',
  inversePair: 'MATICEUR',
  synthetic: SyntheticToken.jEUR,
  collateral: Collateral.WMATIC,
  syntheticDollarPair: 'EURUSD',
  collateralDollarPair: 'MATICUSD',
};
export const jBRL_WMATIC_BASE_INFO = {
  pair: 'BRLMATIC',
  inversePair: 'MATICBRL',
  synthetic: SyntheticToken.jBRL,
  collateral: Collateral.WMATIC,
  syntheticDollarPair: 'BRLUSD',
  collateralDollarPair: 'MATICUSD',
};
export const jEUR_jMXN_BASE_INFO = {
  pair: 'EURMXN',
  inversePair: 'MXNEUR',
  synthetic: SyntheticToken.jEUR,
  collateral: SyntheticToken.jMXN,
  syntheticDollarPair: 'EURUSD',
  collateralDollarPair: 'MXNUSD',
};
export const jBRL_jMXN_BASE_INFO = {
  pair: 'BRLMXN',
  inversePair: 'MXNBRL',
  synthetic: SyntheticToken.jBRL,
  collateral: SyntheticToken.jMXN,
  syntheticDollarPair: 'BRLUSD',
  collateralDollarPair: 'MXNUSD',
};
export const jBRL_jEUR_BASE_INFO = {
  pair: 'BRLEUR',
  inversePair: 'EURBRL',
  synthetic: SyntheticToken.jBRL,
  collateral: SyntheticToken.jEUR,
  syntheticDollarPair: 'BRLUSD',
  collateralDollarPair: 'EURUSD',
};
export const jBRL_WBNB_BASE_INFO = {
  pair: 'BRLBNB',
  inversePair: 'BNBBRL',
  synthetic: SyntheticToken.jBRL,
  collateral: Collateral.WBNB,
  syntheticDollarPair: 'BRLUSD',
  collateralDollarPair: 'BNBUSD',
};
export const jBRL_jCHF_BASE_INFO = {
  pair: 'BRLCHF',
  inversePair: 'CHFBRL',
  synthetic: SyntheticToken.jBRL,
  collateral: SyntheticToken.jCHF,
  syntheticDollarPair: 'BRLUSD',
  collateralDollarPair: 'CHFUSD',
};
export const jBRL_jSGD_BASE_INFO = {
  pair: 'BRLSGD',
  inversePair: 'SGDBRL',
  synthetic: SyntheticToken.jBRL,
  collateral: SyntheticToken.jSGD,
  syntheticDollarPair: 'BRLUSD',
  collateralDollarPair: 'SGDUSD',
};
export const jBRL_jCAD_BASE_INFO = {
  pair: 'BRLCAD',
  inversePair: 'CADBRL',
  synthetic: SyntheticToken.jBRL,
  collateral: SyntheticToken.jCAD,
  syntheticDollarPair: 'BRLUSD',
  collateralDollarPair: 'CADUSD',
};
export const jBRL_jGBP_BASE_INFO = {
  pair: 'BRLGBP',
  inversePair: 'GBPBRL',
  synthetic: SyntheticToken.jBRL,
  collateral: SyntheticToken.jGBP,
  syntheticDollarPair: 'BRLUSD',
  collateralDollarPair: 'GBPUSD',
};
export const jEUR_jSGD_BASE_INFO = {
  pair: 'EURSGD',
  inversePair: 'SGDEUR',
  synthetic: SyntheticToken.jEUR,
  collateral: SyntheticToken.jSGD,
  syntheticDollarPair: 'EURUSD',
  collateralDollarPair: 'SGDUSD',
};
export const jGOLD_USDC_BASE_INFO = {
  pair: 'XAUUSD',
  inversePair: 'USDXAU',
  synthetic: SyntheticToken.jGOLD,
  collateral: Collateral.USDC,
  syntheticDollarPair: 'XAUUSD',
  collateralDollarPair: 'USDCUSD',
};
export const jBRL_USDC_BASE_INFO = {
  pair: 'BRLUSD',
  inversePair: 'USDBRL',
  synthetic: SyntheticToken.jBRL,
  collateral: Collateral.USDC,
  syntheticDollarPair: 'BRLUSD',
  collateralDollarPair: 'USDCUSD',
};

export const jBRL_BUSD_BASE_INFO = {
  pair: 'BRLUSD',
  inversePair: 'USDBRL',
  synthetic: SyntheticToken.jBRL,
  collateral: Collateral.BUSD,
  syntheticDollarPair: 'BRLUSD',
  collateralDollarPair: 'BUSDUSD',
};

export const jEUR_WETH_BASE_INFO = {
  pair: 'EURETH',
  inversePair: 'ETHEUR',
  synthetic: SyntheticToken.jEUR,
  collateral: Collateral.WETH,
  syntheticDollarPair: 'EURUSD',
  collateralDollarPair: 'ETHUSD',
};
export const jEUR_wstETH_BASE_INFO = {
  pair: 'EURETH',
  inversePair: 'WSTETHEUR',
  synthetic: SyntheticToken.jEUR,
  collateral: Collateral.wstETH,
  syntheticDollarPair: 'EURUSD',
  collateralDollarPair: 'WSTETHUSD',
};

export const jBRL_wstETH_BASE_INFO = {
  pair: 'BRLETH',
  inversePair: 'WSTETHBRL',
  synthetic: SyntheticToken.jBRL,
  collateral: Collateral.wstETH,
  syntheticDollarPair: 'BRLUSD',
  collateralDollarPair: 'WSTETHUSD',
};

export const jCAD_wstETH_BASE_INFO = {
  pair: 'CADETH',
  inversePair: 'WSTETHCAD',
  synthetic: SyntheticToken.jCAD,
  collateral: Collateral.wstETH,
  syntheticDollarPair: 'CADUSD',
  collateralDollarPair: 'WSTETHUSD',
};

export const jCHF_wstETH_BASE_INFO = {
  pair: 'CHFETH',
  inversePair: 'WSTETHCHF',
  synthetic: SyntheticToken.jCHF,
  collateral: Collateral.wstETH,
  syntheticDollarPair: 'CHFUSD',
  collateralDollarPair: 'WSTETHUSD',
};
export const jGOLD_wstETH_BASE_INFO = {
  pair: 'XAUETH',
  inversePair: 'WSTETHXAU',
  synthetic: SyntheticToken.jGOLD,
  collateral: Collateral.wstETH,
  syntheticDollarPair: 'XAUUSD',
  collateralDollarPair: 'WSTETHUSD',
};
export const jBRL_WETH_BASE_INFO = {
  pair: 'BRLETH',
  inversePair: 'ETHBRL',
  synthetic: SyntheticToken.jBRL,
  collateral: Collateral.WETH,
  syntheticDollarPair: 'BRLUSD',
  collateralDollarPair: 'ETHUSD',
};

export const jUSD_WETH_BASE_INFO = {
  pair: 'USDETH',
  inversePair: 'ETHUSD',
  synthetic: SyntheticToken.jUSD,
  collateral: Collateral.WETH,
  syntheticDollarPair: 'USDCUSD',
  collateralDollarPair: 'ETHUSD',
};

export const jGBP_USDC_BASE_INFO = {
  pair: 'GBPUSD',
  inversePair: 'USDGBP',
  synthetic: SyntheticToken.jGBP,
  collateral: Collateral.USDC,
  syntheticDollarPair: 'GBPUSD',
  collateralDollarPair: 'USDCUSD',
};

export const jCHF_USDC_BASE_INFO = {
  pair: 'CHFUSD',
  inversePair: 'USDCHF',
  synthetic: SyntheticToken.jCHF,
  collateral: Collateral.USDC,
  syntheticDollarPair: 'CHFUSD',
  collateralDollarPair: 'USDCUSD',
};

export const jCHF_BUSD_BASE_INFO = {
  pair: 'CHFUSD',
  inversePair: 'USDCHF',
  synthetic: SyntheticToken.jCHF,
  collateral: Collateral.BUSD,
  syntheticDollarPair: 'CHFUSD',
  collateralDollarPair: 'BUSDUSD',
};
export const jCHF_jEUR_BASE_INFO = {
  pair: 'CHFEUR',
  inversePair: 'EURCHF',
  synthetic: SyntheticToken.jCHF,
  collateral: SyntheticToken.jEUR,
  syntheticDollarPair: 'CHFUSD',
  collateralDollarPair: 'EURUSD',
};
export const jCHF_WBNB_BASE_INFO = {
  pair: 'CHFBNB',
  inversePair: 'BNBCHF',
  synthetic: SyntheticToken.jCHF,
  collateral: Collateral.WBNB,
  syntheticDollarPair: 'CHFUSD',
  collateralDollarPair: 'BNBUSD',
};
export const jCHF_WETH_BASE_INFO = {
  pair: 'CHFETH',
  inversePair: 'ETHCHF',
  synthetic: SyntheticToken.jCHF,
  collateral: Collateral.WETH,
  syntheticDollarPair: 'CHFUSD',
  collateralDollarPair: 'ETHUSD',
};
export const jCHF_jBRL_BASE_INFO = {
  pair: 'CHFBRL',
  inversePair: 'BRLCHF',
  synthetic: SyntheticToken.jCHF,
  collateral: SyntheticToken.jBRL,
  syntheticDollarPair: 'CHFUSD',
  collateralDollarPair: 'BRLUSD',
};
export const jCHF_WBTC_BASE_INFO = {
  pair: 'CHFBTC',
  inversePair: 'BTCCHF',
  synthetic: SyntheticToken.jCHF,
  collateral: Collateral.WBTC,
  syntheticDollarPair: 'CHFUSD',
  collateralDollarPair: 'BTCUSD',
};

export const jCHF_jGBP_BASE_INFO = {
  pair: 'CHFGBP',
  inversePair: 'GBPCHF',
  synthetic: SyntheticToken.jCHF,
  collateral: SyntheticToken.jGBP,
  syntheticDollarPair: 'CHFUSD',
  collateralDollarPair: 'GBPUSD',
};

export const jMXN_USDC_BASE_INFO = {
  pair: 'MXNUSD',
  inversePair: 'USDMXN',
  synthetic: SyntheticToken.jMXN,
  collateral: Collateral.USDC,
  syntheticDollarPair: 'MXNUSD',
  collateralDollarPair: 'USDCUSD',
};

export const jMXN_jBRL_BASE_INFO = {
  pair: 'MXNBRL',
  inversePair: 'BRLMXN',
  synthetic: SyntheticToken.jMXN,
  collateral: SyntheticToken.jBRL,
  syntheticDollarPair: 'MXNUSD',
  collateralDollarPair: 'BRLUSD',
};

export const jMXN_jCAD_BASE_INFO = {
  pair: 'MXNCAD',
  inversePair: 'CADMXN',
  synthetic: SyntheticToken.jMXN,
  collateral: SyntheticToken.jCAD,
  syntheticDollarPair: 'MXNUSD',
  collateralDollarPair: 'CADUSD',
};
export const jMXN_jCHF_BASE_INFO = {
  pair: 'MXNCHF',
  inversePair: 'CHFMXN',
  synthetic: SyntheticToken.jMXN,
  collateral: SyntheticToken.jCHF,
  syntheticDollarPair: 'MXNUSD',
  collateralDollarPair: 'CHFUSD',
};
export const jMXN_jEUR_BASE_INFO = {
  pair: 'MXNEUR',
  inversePair: 'EURMXN',
  synthetic: SyntheticToken.jMXN,
  collateral: SyntheticToken.jEUR,
  syntheticDollarPair: 'MXNUSD',
  collateralDollarPair: 'EURUSD',
};
export const jMXN_jGBP_BASE_INFO = {
  pair: 'MXNGBP',
  inversePair: 'GBPMXN',
  synthetic: SyntheticToken.jMXN,
  collateral: SyntheticToken.jGBP,
  syntheticDollarPair: 'MXNUSD',
  collateralDollarPair: 'GBPUSD',
};
export const jMXN_jSGD_BASE_INFO = {
  pair: 'MXNSGD',
  inversePair: 'SGDMXN',
  synthetic: SyntheticToken.jMXN,
  collateral: SyntheticToken.jSGD,
  syntheticDollarPair: 'MXNUSD',
  collateralDollarPair: 'SGDUSD',
};
export const jMXN_jGOLD_BASE_INFO = {
  pair: 'MXNXAU',
  inversePair: 'XAUMXN',
  synthetic: SyntheticToken.jMXN,
  collateral: SyntheticToken.jGOLD,
  syntheticDollarPair: 'MXNUSD',
  collateralDollarPair: 'XAUUSD',
};

export const jMXN_WETH_BASE_INFO = {
  pair: 'MXNETH',
  inversePair: 'ETHMXN',
  synthetic: SyntheticToken.jMXN,
  collateral: Collateral.WETH,
  syntheticDollarPair: 'MXNUSD',
  collateralDollarPair: 'ETHUSD',
};
export const jMXN_wstETH_BASE_INFO = {
  pair: 'MXNETH',
  inversePair: 'WSTETHMXN',
  synthetic: SyntheticToken.jMXN,
  collateral: Collateral.wstETH,
  syntheticDollarPair: 'MXNUSD',
  collateralDollarPair: 'WSTETHUSD',
};
export const jMXN_WBTC_BASE_INFO = {
  pair: 'MXNBTC',
  inversePair: 'BTCMXN',
  synthetic: SyntheticToken.jMXN,
  collateral: Collateral.WBTC,
  syntheticDollarPair: 'MXNUSD',
  collateralDollarPair: 'BTCUSD',
};

export const jMXN_WMATIC_BASE_INFO = {
  pair: 'MXNMATIC',
  inversePair: 'MATICMXN',
  synthetic: SyntheticToken.jMXN,
  collateral: Collateral.WMATIC,
  syntheticDollarPair: 'MXNUSD',
  collateralDollarPair: 'MATICUSD',
};
