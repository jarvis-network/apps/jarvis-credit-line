/* eslint-disable camelcase */
import { ChainId, Currency, NetworkId } from '@jarvisnetwork/core-sdk';
import { NetworkPricesFeedMap } from '@jarvisnetwork/toolkit';

import { CreditLineData, CREDIT_LINE } from './creditLine';

import { NetworksTokens } from './tokens';

const extractPriceFeedPairs = (chainId: NetworkId) => [
  ...new Set([
    ...Object.values(NetworksTokens[chainId]!)!.map(
      (token: Currency) => token.pair,
    ),
    ...Object.values(CREDIT_LINE[chainId!]!.CreditLine!)!.reduce(
      (prev: string[], current: CreditLineData) => [
        ...prev,
        current.pair,
        current.syntheticDollarPair,
        current.collateralDollarPair,
      ],
      [],
    ),
  ]),
];

export const NetworkPricesFeed: NetworkPricesFeedMap = {
  [ChainId.polygon]: extractPriceFeedPairs(ChainId.polygon),
  [ChainId.mumbai]: extractPriceFeedPairs(ChainId.mumbai),
  [ChainId.optimism]: extractPriceFeedPairs(ChainId.optimism),
  [ChainId.bsc]: extractPriceFeedPairs(ChainId.bsc),
  [ChainId.arbitrum]: extractPriceFeedPairs(ChainId.arbitrum),
};
