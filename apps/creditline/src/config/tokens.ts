import {
  ChainId,
  jCHF,
  jEUR,
  jGBP,
  jGOLD,
  jBRL,
  jUSD,
  NetworkId,
  USDC,
  WNATIVE,
  NATIVE,
  OP,
  BUSD,
  Currency,
  WETH9,
  WBTC,
  jCAD,
  jMXN,
  jSGD,
  WSTETH,
} from '@jarvisnetwork/core-sdk';

import { Collateral, SyntheticToken } from '@/enums';

type NetworkTokenMap = {
  [chainId in NetworkId]?: {
    [key in Collateral | SyntheticToken]?: Currency;
  };
};

export const NetworksTokens: NetworkTokenMap = {
  [ChainId.optimism]: {
    [Collateral.USDC]: USDC[ChainId.optimism],
    [Collateral.WETH]: WETH9[ChainId.optimism],
    [Collateral.WBTC]: WBTC[ChainId.optimism],
    [Collateral.ETH]: NATIVE[ChainId.optimism],
    [Collateral.wstETH]: WSTETH[ChainId.optimism],
    [SyntheticToken.jEUR]: jEUR[ChainId.optimism],
    [Collateral.OP]: OP[ChainId.optimism],
  },
  [ChainId.arbitrum]: {
    [Collateral.USDC]: USDC[ChainId.arbitrum],
    [Collateral.ETH]: NATIVE[ChainId.arbitrum],
    [Collateral.WETH]: WETH9[ChainId.arbitrum],
    [Collateral.wstETH]: WSTETH[ChainId.arbitrum],
    [Collateral.WBTC]: WBTC[ChainId.arbitrum],
    [SyntheticToken.jEUR]: jEUR[ChainId.arbitrum],
  },
  [ChainId.bsc]: {
    [Collateral.BUSD]: BUSD[ChainId.bsc],
    [Collateral.BNB]: NATIVE[ChainId.bsc],
    [Collateral.WBNB]: WNATIVE[ChainId.bsc],
    [Collateral.WETH]: WETH9[ChainId.bsc],
    [Collateral.WBTC]: WBTC[ChainId.bsc],
    [SyntheticToken.jBRL]: jBRL[ChainId.bsc],
    [SyntheticToken.jCHF]: jCHF[ChainId.bsc],
    [SyntheticToken.jEUR]: jEUR[ChainId.bsc],
    [SyntheticToken.jGBP]: jGBP[ChainId.bsc],
  },
  [ChainId.polygon]: {
    [Collateral.USDC]: USDC[ChainId.polygon],
    [SyntheticToken.jEUR]: jEUR[ChainId.polygon],
    [SyntheticToken.jCHF]: jCHF[ChainId.polygon],
    [SyntheticToken.jGOLD]: jGOLD[ChainId.polygon],
    [SyntheticToken.jBRL]: jBRL[ChainId.polygon],
    [SyntheticToken.jGBP]: jGBP[ChainId.polygon],
    [SyntheticToken.jCAD]: jCAD[ChainId.polygon],
    [SyntheticToken.jMXN]: jMXN[ChainId.polygon],
    [SyntheticToken.jSGD]: jSGD[ChainId.polygon],
    [Collateral.WETH]: WETH9[ChainId.polygon],
    [Collateral.WBTC]: WBTC[ChainId.polygon],
    [Collateral.WMATIC]: WNATIVE[ChainId.polygon],
    [Collateral.wstETH]: WSTETH[ChainId.polygon],
    [Collateral.MATIC]: NATIVE[ChainId.polygon],
  },
  [ChainId.mumbai]: {
    [Collateral.USDC]: USDC[ChainId.mumbai],
    [SyntheticToken.jEUR]: jEUR[ChainId.mumbai],
    [SyntheticToken.jCHF]: jCHF[ChainId.mumbai],
    [SyntheticToken.jGBP]: jGBP[ChainId.mumbai],
    [Collateral.WMATIC]: WNATIVE[ChainId.mumbai],
    [Collateral.MATIC]: NATIVE[ChainId.mumbai],
  },
};
