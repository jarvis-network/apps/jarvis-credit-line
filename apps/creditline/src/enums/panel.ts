export enum PANEL_FLOW_STEPS {
  WELCOME = 'WELCOME',
  FORM = 'FORM',
}

export enum PANEL_TYPE {
  HOME = 'HOME',
  CREATE = 'CREATE',
  ADD_COLLATERAL = 'ADD',
  BORROW_MORE = 'BORROW',
  REMOVE = 'REMOVE',
  REPAY = 'REPAY',
  CLOSE = 'CLOSE',
}

export enum ERROR_TYPE {
  BALANCE = 'BALANCE',
  MIN_SPONSOR = 'MIN_SPONSOR',
  COLLATERAL_RATIO = ' COLLATERAL_RATIO',
}
