/* eslint-disable camelcase */

import { Currency, FixBigNumber, Token } from '@jarvisnetwork/core-sdk';

import { defaultSupportedNetworkId } from '@jarvisnetwork/toolkit';

import { useAccount, useNetwork, useProvider } from 'wagmi';

import { useMemo } from 'react';

import { BigNumber } from 'ethers';

import {
  useAllCreditLineDataAsArrayLinkToBorrowToken,
  useAllNetworkCreditLineDataAsArray,
  useCreditLineData,
} from '@/hooks/useCreditLineData';
import {
  useMultiCallCreditLineContract,
  useReadCreditLineContract,
} from '@/hooks/useContract';

import { CreditLineData } from '@/config/creditLine';
import { useNetworkCurrencies } from '@/components/NetworkCurrenciesProvider';

export interface CreditLineWithUserLoanPositionData extends CreditLineData {
  collateralAmount: FixBigNumber;
  tokensAmount: FixBigNumber;
}

export const useAllUserCreditLineWithUserLoanPositionData =
  (): CreditLineWithUserLoanPositionData[] => {
    const { address: account } = useAccount();
    const currencyArrayList = useNetworkCurrencies();
    const creditLineDataAsArray = useAllNetworkCreditLineDataAsArray();

    const { data: res } = useMultiCallCreditLineContract(
      creditLineDataAsArray?.map(({ address }) => ({
        address,
        functionName: 'getPositionData',
        args: [account],
      })),
      true,
    );

    return useMemo(() => {
      if (
        !creditLineDataAsArray?.length ||
        !res?.length ||
        !res.filter(r => !!r).length ||
        !account ||
        !currencyArrayList?.length
      )
        return undefined;

      const positions = res as {
        collateralAmount: BigNumber;
        tokensAmount: BigNumber;
      }[];
      return creditLineDataAsArray.map((data, index) => {
        const syntheticCurrency = currencyArrayList.find(
          currency => currency.symbol === data.synthetic,
        );
        const collateralCurrency = currencyArrayList.find(
          currency => currency.symbol === data.collateral,
        );

        return {
          ...data,
          collateralAmount: FixBigNumber.fromWei(
            positions[index].collateralAmount.toString() ?? 0,
            collateralCurrency?.decimals,
          ),
          tokensAmount: FixBigNumber.fromWei(
            positions[index].tokensAmount.toString(),
            syntheticCurrency?.decimals,
          ),
        };
      });
    }, [creditLineDataAsArray, account, currencyArrayList, res]);
  };

export const useSyntheticsCreditLinesDataWithUserLoanPosition = (
  synthetic: string,
): CreditLineWithUserLoanPositionData[] => {
  const { address: account } = useAccount();
  const currencyArrayList = useNetworkCurrencies();
  const creditLineDataAsArray =
    useAllCreditLineDataAsArrayLinkToBorrowToken(synthetic);

  const { data: res } = useMultiCallCreditLineContract(
    creditLineDataAsArray?.map(({ address }) => ({
      address,
      functionName: 'getPositionData',
      args: [account],
    })),
    true,
  );

  return useMemo(() => {
    if (
      !creditLineDataAsArray?.length ||
      !res?.length ||
      !res.filter(r => !!r).length ||
      !account ||
      !currencyArrayList?.length
    )
      return undefined;

    const positions = res as {
      collateralAmount: BigNumber;
      tokensAmount: BigNumber;
    }[];
    return creditLineDataAsArray.map((data, index) => {
      const syntheticCurrency = currencyArrayList.find(
        currency => currency.symbol === data.synthetic,
      );
      const collateralCurrency = currencyArrayList.find(
        currency => currency.symbol === data.collateral,
      );

      return {
        ...data,
        collateralAmount: FixBigNumber.fromWei(
          positions[index].collateralAmount.toString() ?? 0,
          collateralCurrency?.decimals,
        ),
        tokensAmount: FixBigNumber.fromWei(
          positions[index].tokensAmount.toString(),
          syntheticCurrency?.decimals,
        ),
      };
    });
  }, [creditLineDataAsArray, account, currencyArrayList, res]);
};

export const useUserLoanPositionDataFromPairAddresses = (
  synthetic: Token,
  collateral: Currency,
): CreditLineWithUserLoanPositionData => {
  const { address: account } = useAccount();
  const { chain } = useNetwork();
  const chainId = chain?.id ?? defaultSupportedNetworkId;
  const provider = useProvider({ chainId });
  const creditLineData = useCreditLineData(synthetic, collateral);
  const { data } = useReadCreditLineContract(
    synthetic,
    collateral,
    'getPositionData',
    [account],
    true,
  );

  return useMemo(() => {
    if (!creditLineData || !data || !chain || !account || !provider)
      return undefined;

    return {
      ...creditLineData,
      collateralAmount: FixBigNumber.fromWei(
        data.collateralAmount.toString(),
        collateral?.decimals,
      ),
      tokensAmount: FixBigNumber.fromWei(
        data.tokensAmount.toString(),
        synthetic?.decimals,
      ),
    };
  }, [creditLineData, chain, account, provider, data]);
};
