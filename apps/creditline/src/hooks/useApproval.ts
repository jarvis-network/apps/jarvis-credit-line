import { useCallback, useMemo, useState } from 'react';

import { MaxUint256 } from '@ethersproject/constants';
import { Currency, FixBigNumber, Token } from '@jarvisnetwork/core-sdk';

import { useAccount } from 'wagmi';

import {
  useReadERC20Contract,
  useTokenContract,
  useWriteERC20Contract,
} from './useContract';

import { useEstimatedNetworkFee } from './useEstimatedNetworkFee';

import { TransactionType } from '@/types';
import { DECIMAL_DIGITS_COUNT } from '@/config/app';
import { useTransactionAdder } from '@/state/transactions/hooks';
import { useToggleTransactionModal } from '@/state/app/hooks';

export enum ApprovalState {
  UNKNOWN = 'UNKNOWN',
  NOT_APPROVED = 'NOT_APPROVED',
  PENDING = 'PENDING',
  APPROVED = 'APPROVED',
}

const useApprovalStateForSpender = (
  amountToApprove: FixBigNumber | undefined,
  currency: Currency | undefined,
  spender: string | undefined,
): ApprovalState => {
  const { address: account } = useAccount();

  const { data: currentAllowance } = useReadERC20Contract(
    (currency as Token)?.address,
    'allowance',
    [account, spender],
    true,
  );

  return useMemo(() => {
    if (!amountToApprove || !spender || !currency) return ApprovalState.UNKNOWN;
    if (currency.isNative) return ApprovalState.APPROVED;
    // we might not have enough data to know whether or not we need to approve
    if (!currentAllowance) return ApprovalState.PENDING;

    return amountToApprove.lessThan(currentAllowance.toString())
      ? ApprovalState.APPROVED
      : ApprovalState.NOT_APPROVED;
  }, [amountToApprove, currentAllowance, spender, currency]);
};

export const useApproval = (
  amountToApprove: FixBigNumber | undefined,
  currency: Currency | undefined,
  spender: string | undefined,
) => {
  const toggleTransactionModal = useToggleTransactionModal();
  const addTransaction = useTransactionAdder();
  const [networkFee, setNetworkFee] = useState({ gasLimit: '0', fee: '0' });
  const token = currency?.isToken ? currency : undefined;

  const approvalState = useApprovalStateForSpender(
    amountToApprove,
    token,
    spender,
  );

  const tokenContract = useTokenContract(token?.address);

  const estimateGasFunction = () =>
    tokenContract?.estimateGas.approve(spender, MaxUint256);
  const approvalNetworkFee = useEstimatedNetworkFee(estimateGasFunction);

  const approvalEstimatedNetworkFee = useCallback(async () => {
    if (!amountToApprove) return networkFee.fee;
    const data = await approvalNetworkFee();
    if (!data) return networkFee.fee;
    setNetworkFee({
      gasLimit: data.gasLimit.toString(),
      fee: data.fee?.toFixed(DECIMAL_DIGITS_COUNT),
    });
    return data.fee.toFixed(DECIMAL_DIGITS_COUNT);
  }, [approvalState]);

  const { writeAsync } = useWriteERC20Contract(
    token?.address,
    'approve',
    [spender, MaxUint256],
    networkFee.gasLimit,
    !!networkFee.gasLimit,
  );

  const approve = useCallback(async () => {
    const logFailure = (error: Error | string) => {
      console.warn(`${token?.symbol || 'Token'} approval failed:`, error);
    };

    if (approvalState !== ApprovalState.NOT_APPROVED) {
      return logFailure('approve was called unnecessarily');
    }
    if (!token) {
      return logFailure('no token');
    }
    if (!tokenContract) {
      return logFailure('tokenContract is null');
    }
    if (!amountToApprove) {
      return logFailure('missing amount to approve');
    }
    if (!spender) {
      return logFailure('no spender');
    }
    const tx = await writeAsync();
    addTransaction(tx.hash, {
      type: TransactionType.APPROVAL,
      tokenAddress: token.address,
      tokenSymbol: token.symbol,
      spender,
    });
    toggleTransactionModal();
  }, [
    approvalState,
    token,
    tokenContract,
    amountToApprove,
    spender,
    toggleTransactionModal,
    addTransaction,
    writeAsync,
  ]);

  return { approvalState, approvalEstimatedNetworkFee, approve };
};
