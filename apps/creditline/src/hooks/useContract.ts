/* eslint-disable camelcase */
import { Contract } from '@ethersproject/contracts';

import {
  ICreditLine_Abi,
  SelfMintingRegistry_Abi,
  SynthereumChainlinkPriceFeed_Abi,
} from '@jarvisnetwork/synthereum-contracts/build/abi';

import {
  Currency,
  NetworkId,
  PRICE_FEED_ADDRESS,
  Token,
} from '@jarvisnetwork/core-sdk';

import { defaultSupportedNetworkId } from '@jarvisnetwork/toolkit';

import {
  useContractRead,
  useNetwork,
  useContract,
  useSigner,
  useProvider,
  erc20ABI,
  usePrepareContractWrite,
  useContractWrite,
  useContractReads,
} from 'wagmi';

import { BigNumber } from 'ethers';

import { ChainLinkAggregator_Abi } from 'abis/ChainLinkAggregator_Abi';

import { CREDIT_LINE } from '@/config/creditLine';
import { getValidPair } from '@/utils/getValidPair';

export type MultiCallData = {
  address?: string;
  functionName: string;
  args?: string[];
};

export const useReadCreditLineRegistry = (
  functionName: string,
  args?: string[],
) => {
  const { chain } = useNetwork();
  const chainId = chain?.id ?? defaultSupportedNetworkId;
  return useContractRead({
    address: CREDIT_LINE[chainId as NetworkId]
      ?.CreditLineRegistry as `0x${string}`,
    abi: SelfMintingRegistry_Abi,
    functionName,
    args,
  });
};

export const useTokenContract = (tokenAddress: string): Contract | null => {
  const { data: signer } = useSigner();
  const provider = useProvider();
  return useContract({
    address: tokenAddress as `0x${string}`,
    abi: erc20ABI,
    signerOrProvider: signer ?? provider,
  });
};

export const useReadERC20Contract = (
  tokenAddress?: string,
  functionName?:
    | 'symbol'
    | 'name'
    | 'allowance'
    | 'balanceOf'
    | 'decimals'
    | 'totalSupply',
  args?: string[],
  watch = false,
) =>
  useContractRead({
    address: tokenAddress as `0x${string}`,
    abi: erc20ABI,
    functionName,
    args: args as [`0x${string}`, `0x${string}`] | [`0x${string}`],
    watch,
  });
export const useMultiCallERC20Contract = (
  calls: MultiCallData[],
  watch = false,
  enabled = true,
) =>
  useContractReads({
    contracts: calls?.map(call => ({
      ...call,
      address: call.address as `0x${string}`,
      abi: erc20ABI,
    })),
    watch,
    enabled,
  });

export const useWriteERC20Contract = (
  tokenAddress?: string,
  functionName?: 'approve' | 'transfer' | 'transferFrom',
  args?: unknown[],
  gasLimit?: string,
  enable = true,
) => {
  const { config } = usePrepareContractWrite({
    address: tokenAddress as `0x${string}`,
    abi: erc20ABI,
    functionName,
    args: args as
      | [`0x${string}`, BigNumber]
      | readonly [`0x${string}`, `0x${string}`, BigNumber],
    overrides: {
      gasLimit: gasLimit ? BigNumber.from(gasLimit) : BigNumber.from('0'),
    },
    enabled: enable,
  });

  return useContractWrite(config);
};

export const useCreditLineContract = (
  synthetic: Token | null | undefined,
  collateral: Currency | null | undefined,
): Contract | null => {
  const { chain } = useNetwork();
  const chainId = chain?.id ?? defaultSupportedNetworkId;
  const { data: signer } = useSigner();
  const provider = useProvider();
  const pair = getValidPair(synthetic, collateral);
  return useContract({
    address: CREDIT_LINE[chainId as NetworkId]?.CreditLine[pair!]?.address,
    abi: ICreditLine_Abi,
    signerOrProvider: signer ?? provider,
  });
};

export const useReadCreditLineContract = (
  synthetic: Token | null | undefined,
  collateral: Currency | null | undefined,
  functionName?: string,
  args?: string[],
  watch = false,
) => {
  const { chain } = useNetwork();
  const chainId = chain?.id ?? defaultSupportedNetworkId;
  const pair = getValidPair(synthetic, collateral);
  return useContractRead({
    address: CREDIT_LINE[chainId as NetworkId]?.CreditLine[pair!]
      ?.address as `0x${string}`,
    abi: ICreditLine_Abi,
    functionName,
    args,
    watch,
  });
};

export const useMultiCallCreditLineContract = (
  calls: MultiCallData[],
  watch = false,
) =>
  useContractReads({
    contracts: calls?.map(call => ({
      ...call,
      address: call.address as `0x${string}`,
      abi: ICreditLine_Abi,
    })),
    watch,
  });

export const useWriteCreditLineContract = (
  creditLineAddress?: string,
  functionName?: string,
  args?: unknown[],
  gasLimit?: string,
  enable = true,
) => {
  const { config } = usePrepareContractWrite({
    address: creditLineAddress as `0x${string}`,
    abi: ICreditLine_Abi,
    functionName,
    args,
    overrides: {
      gasLimit: gasLimit ? BigNumber.from(gasLimit) : BigNumber.from('0'),
    },
    enabled: enable,
  });
  return useContractWrite(config);
};

export const useMultiCallChainLink = (
  calls: MultiCallData[],
  watch = false,
  chainId = defaultSupportedNetworkId as number,
  enabled = true,
) =>
  useContractReads({
    contracts: calls?.map(call => ({
      ...call,
      address: call.address as `0x${string}`,
      abi: ChainLinkAggregator_Abi,
      chainId,
    })),
    watch,
    enabled,
  });

export const useReadPriceFeed = (
  functionName?: string,
  args?: unknown[],
  watch = true,
  chainId = defaultSupportedNetworkId as number,
  enabled = true,
) =>
  useContractRead({
    address: PRICE_FEED_ADDRESS[chainId] as `0x${string}`,
    abi: SynthereumChainlinkPriceFeed_Abi,
    functionName,
    args,
    watch,
    chainId,
    enabled,
  });
