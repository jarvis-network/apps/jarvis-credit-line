import { FixBigNumber } from '@jarvisnetwork/core-sdk';
import { BigNumber, Contract } from 'ethers';
import { useCallback, useState } from 'react';

import { useEstimatedNetworkFee } from './useEstimatedNetworkFee';

import { useWriteCreditLineContract } from './useContract';

import { TransactionType } from '@/types';
import { useTransactionAdder } from '@/state/transactions/hooks';
import { useToggleTransactionModal } from '@/state/app/hooks';
import { DECIMAL_DIGITS_COUNT } from '@/config/app';
import { useCurrentPair } from '@/components/CurrentPairProvider';

export const useCreateLoan = (
  collateralAmount: FixBigNumber | undefined,
  syntheticAmount: FixBigNumber | undefined,
  creditLineContract: Contract | undefined | null,
  transactionType: TransactionType.BORROW | TransactionType.CREATE_LOAN,
) => {
  const toggleTransactionModal = useToggleTransactionModal();
  const addTransaction = useTransactionAdder();
  const { collateralToken, syntheticToken } = useCurrentPair();
  const [networkFee, setNetworkFee] = useState({ gasLimit: '0', fee: '0' });

  const estimateGasCreateFunction = () =>
    creditLineContract?.estimateGas.create(
      collateralAmount?.toFormat(),
      syntheticAmount?.toFormat(),
    );
  const estimateGasBorrowFunction = () =>
    creditLineContract?.estimateGas.create(
      collateralAmount?.toFormat(),
      syntheticAmount?.toFormat(),
    );

  const createNetworkFee = useEstimatedNetworkFee(estimateGasCreateFunction);

  const createEstimatedNetworkFee = useCallback(async () => {
    if (!syntheticAmount || !collateralAmount) return networkFee.fee;
    const data = await createNetworkFee();
    if (!data) return networkFee.fee;
    setNetworkFee({
      gasLimit: data.gasLimit.toString(),
      fee: data.fee?.toFixed(DECIMAL_DIGITS_COUNT),
    });

    return data.fee?.toFixed(DECIMAL_DIGITS_COUNT);
  }, [syntheticAmount, collateralAmount, createNetworkFee]);

  const borrowNetworkFee = useEstimatedNetworkFee(estimateGasBorrowFunction);

  const borrowEstimatedNetworkFee = useCallback(async () => {
    if (!syntheticAmount || !collateralAmount) return networkFee.fee;
    const data = await borrowNetworkFee();
    if (!data) return networkFee.fee;
    setNetworkFee({
      gasLimit: data.gasLimit.toString(),
      fee: data.fee?.toFixed(DECIMAL_DIGITS_COUNT),
    });

    return data.fee.toFixed(DECIMAL_DIGITS_COUNT);
  }, [syntheticAmount, collateralAmount, borrowNetworkFee]);

  const { writeAsync } = useWriteCreditLineContract(
    creditLineContract?.address,
    'create',
    [
      BigNumber.from(collateralAmount?.toFormat() ?? 0),
      BigNumber.from(syntheticAmount?.toFormat() ?? 0),
    ],
    networkFee.gasLimit,
    !!networkFee.gasLimit,
  );

  const create = useCallback(async () => {
    const logFailure = (error: Error | string) => {
      console.warn(`Create Loan failed:`, error);
    };
    if (!collateralToken) return logFailure('Collateral Token is null');
    if (!syntheticToken) return logFailure('Synthetic Token is null');
    if (!creditLineContract) {
      return logFailure('creditLineContract is null');
    }
    if (!collateralAmount) {
      return logFailure('collateralAmount amount undefined');
    }
    if (!syntheticAmount) {
      return logFailure('syntheticAmount amount undefined');
    }

    const tx = await writeAsync();

    addTransaction(tx.hash, {
      type: transactionType,
      pairAddress: creditLineContract.address,
      collateralSymbol: collateralToken.symbol,
      collateralAmount: collateralAmount.toExact(),
      syntheticSymbol: syntheticToken.symbol,
      syntheticAmount: syntheticAmount.toExact(),
    });
    toggleTransactionModal();
  }, [
    collateralToken,
    syntheticToken,
    creditLineContract,
    collateralAmount,
    syntheticAmount,
    transactionType,
    toggleTransactionModal,
    addTransaction,
    writeAsync,
  ]);

  return { createEstimatedNetworkFee, borrowEstimatedNetworkFee, create };
};
