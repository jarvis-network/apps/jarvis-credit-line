import { isSupportedNetworkId } from '@jarvisnetwork/toolkit';
import {
  Currency,
  FixBigNumber,
  NetworkId,
  Token,
} from '@jarvisnetwork/core-sdk';
import { useWhyDidYouUpdate } from 'ahooks';

import { useMemo } from 'react';

import { useNetwork } from 'wagmi';

import { BigNumber } from 'ethers';

import { MultiCallData, useMultiCallCreditLineContract } from './useContract';

import { PairLike } from '@/types';

import { getValidPair } from '@/utils/getValidPair';

import { CREDIT_LINE, CreditLineData } from '@/config/creditLine';
import { Collateral, SyntheticToken } from '@/enums';

type CreditLineCallData = {
  0: BigNumber;
  1: [BigNumber, BigNumber, BigNumber, BigNumber];
};

type GlobalPositionData = [BigNumber, BigNumber, BigNumber, BigNumber];

const useCreditLineBaseData = (
  synthetic?: Token,
  collateral?: Currency,
): CreditLineData | undefined => {
  const { chain } = useNetwork();
  return useMemo(() => {
    if (!synthetic || !collateral) return undefined;
    const pair = getValidPair(synthetic, collateral);
    if (!pair || !chain?.id || !isSupportedNetworkId(chain?.id))
      return undefined;
    return CREDIT_LINE[chain.id as NetworkId]?.CreditLine[pair];
  }, [chain, collateral, synthetic]);
};

export const useCreditLineData = (
  synthetic?: Token,
  collateral?: Currency,
): CreditLineData | undefined => {
  const baseData = useCreditLineBaseData(synthetic, collateral);
  const { data: creditLineInfo } = useMultiCallCreditLineContract(
    [
      { address: baseData?.address, functionName: 'capMintAmount' },
      {
        address: baseData?.address,
        functionName: 'getGlobalPositionData',
      },
    ],
    false,
  );
  return useMemo(() => {
    if (!creditLineInfo || creditLineInfo[0] == null) return baseData;
    const capMint = FixBigNumber.fromWei(creditLineInfo[0].toString());
    const data = creditLineInfo as unknown as CreditLineCallData;
    const globalPosition = {
      totalCollateral: FixBigNumber.fromWei(data[1][0].toString()),
      totalBorrowed: FixBigNumber.fromWei(data[1][1].toString()),
    };
    return { ...baseData, capMint, globalPosition };
  }, [synthetic, collateral]);
};

const useAllNetworkCreditLineBaseData = ():
  | { [key in PairLike<SyntheticToken, Collateral, '_'>]?: CreditLineData }
  | undefined => {
  const { chain } = useNetwork();
  return useMemo(() => {
    if (!chain?.id || !isSupportedNetworkId(chain?.id)) return undefined;
    return CREDIT_LINE[chain.id as NetworkId]?.CreditLine;
  }, [chain]);
};

export const useAllNetworkCreditLineDataAsArray = ():
  | CreditLineData[]
  | undefined => {
  const allNetworkCreditLineData = useAllNetworkCreditLineBaseData();
  const callData: MultiCallData[] = Object.values(
    allNetworkCreditLineData,
  ).reduce(
    (prev, curr) => [
      ...prev,
      { address: curr?.address, functionName: 'capMintAmount' },
      {
        address: curr?.address,
        functionName: 'getGlobalPositionData',
      },
    ],
    [],
  );
  const { data: creditLinesInfo } = useMultiCallCreditLineContract(
    callData,
    false,
  );

  return useMemo(() => {
    const datas = Object.values(allNetworkCreditLineData);
    if (!datas || !creditLinesInfo || creditLinesInfo[0] == null) return datas;
    return datas.map((base, index) => {
      const globalPositionData = creditLinesInfo[
        index * 2 + 1
      ] as unknown as GlobalPositionData;
      const capMint = FixBigNumber.fromWei(
        creditLinesInfo[index * 2].toString(),
      );

      const globalPosition = {
        totalCollateral: FixBigNumber.fromWei(globalPositionData[0].toString()),
        totalBorrowed: FixBigNumber.fromWei(globalPositionData[1].toString()),
      };
      return { ...base, capMint, globalPosition };
    });
  }, [allNetworkCreditLineData, creditLinesInfo]);
};

export const useAllCreditLineDataAsArrayLinkToBorrowToken = (
  synthetic: string,
): CreditLineData[] | undefined => {
  const allNetworkCreditLineData = useAllNetworkCreditLineDataAsArray();
  return useMemo(() => {
    if (!allNetworkCreditLineData?.length) return undefined;
    return allNetworkCreditLineData.filter(
      creditLine => creditLine.synthetic === synthetic,
    );
  }, [allNetworkCreditLineData, synthetic]);
};
