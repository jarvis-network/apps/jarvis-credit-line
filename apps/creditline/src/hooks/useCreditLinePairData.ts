import { useMemo } from 'react';

import { useCreditLineData } from '@/hooks/useCreditLineData';
import { useCurrentPair } from '@/components/CurrentPairProvider';

export const useCreditLinePairData = () => {
  const { syntheticToken, collateralToken } = useCurrentPair();
  const currentPairData = useCreditLineData(syntheticToken, collateralToken);
  return useMemo(() => currentPairData, [currentPairData]);
};
