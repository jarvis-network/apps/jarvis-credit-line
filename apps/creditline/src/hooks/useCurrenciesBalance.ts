import {
  Currency,
  CurrencyWithBalance,
  FixBigNumber,
  NATIVE,
  Token,
} from '@jarvisnetwork/core-sdk';
import { defaultSupportedNetworkId } from '@jarvisnetwork/toolkit';
import { useMemo } from 'react';

import { useAccount, useBalance, useNetwork } from 'wagmi';

import { useMultiCallERC20Contract } from './useContract';

export const useCurrenciesBalance = (
  currencies?: Currency[],
): CurrencyWithBalance[] => {
  const { address } = useAccount();
  const { chain } = useNetwork();
  const chainId = chain?.id ?? defaultSupportedNetworkId;

  const erc20Tokens = currencies.filter(
    currency => !NATIVE[chainId].equals(currency),
  );

  const { data: tokenBalances } = useMultiCallERC20Contract(
    erc20Tokens?.map(token => ({
      address: (token as Token).address,
      functionName: 'balanceOf',
      args: [address],
    })),
    true,
    !!erc20Tokens?.length && !!address,
  );

  const { data: nativeBalance } = useBalance({
    address,
    watch: true,
    cacheTime: 2000,
  });

  return useMemo(() => {
    let currenciesBalance: CurrencyWithBalance[] = [];
    if (!currencies.length || !address) return undefined;
    if (tokenBalances?.length && tokenBalances.filter(t => t !== null).length) {
      currenciesBalance = [
        ...currenciesBalance,
        ...erc20Tokens.map((token, index) => ({
          currency: token,
          balance: FixBigNumber.fromWei(
            tokenBalances[index].toString(),
            token.decimals,
          ),
        })),
      ];
    }
    if (nativeBalance) {
      currenciesBalance = [
        ...currenciesBalance,
        ...[
          {
            currency: NATIVE[chainId],
            balance:
              nativeBalance.value.toString() !== '0'
                ? FixBigNumber.toWei(
                    nativeBalance.formatted.toString(),
                    nativeBalance.decimals,
                  )
                : FixBigNumber.toWei(0, nativeBalance.decimals),
          },
        ],
      ];
    }
    return currenciesBalance;
  }, [address, tokenBalances, currencies]);
};
