import { useCreditLineContract } from './useContract';

import { useCreditLinePairData } from './useCreditLinePairData';

import { useUserCurrentLoanPosition } from '@/state/loan/hooks';
import { useCurrentPair } from '@/components/CurrentPairProvider';
import { usePriceFeed } from '@/components/PriceFeedProvider';

export const useCurrentLoanData = () => {
  const currentPairData = useCreditLinePairData();
  const pricePair = usePriceFeed(currentPairData?.pair);
  const { syntheticToken, collateralToken } = useCurrentPair();
  const loanPositionData = useUserCurrentLoanPosition();

  const creditLineContract = useCreditLineContract(
    syntheticToken,
    collateralToken,
  );

  return {
    currentPairData,
    pricePair,
    syntheticToken,
    collateralToken,
    loanPositionData,
    creditLineContract,
  };
};
