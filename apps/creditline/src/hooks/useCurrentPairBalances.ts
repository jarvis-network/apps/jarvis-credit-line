import { useMemo, useState } from 'react';

import { CurrencyWithBalance, Token } from '@jarvisnetwork/core-sdk';

import { useCurrentPair } from '@/components/CurrentPairProvider';
import { useAllUserCurrenciesBalance } from '@/components/UserCurrenciesBalanceProvider';

export const useCurrentPairBalances = () => {
  const tokensBalance = useAllUserCurrenciesBalance();
  const { collateralToken, syntheticToken } = useCurrentPair();

  const [currentPairBalances, setCurrentPairBalances] = useState<{
    collateralBalance: CurrencyWithBalance | undefined;
    syntheticBalance: CurrencyWithBalance | undefined;
    nativeCurrency: CurrencyWithBalance | undefined;
  }>({
    collateralBalance: undefined,
    syntheticBalance: undefined,
    nativeCurrency: undefined,
  });

  useMemo(() => {
    if (!tokensBalance || !collateralToken || !syntheticToken) {
      return;
    }
    const collateralBalance = tokensBalance.find(
      token =>
        (token.currency as Token).address ===
        (collateralToken as Token).address,
    );

    const syntheticBalance = tokensBalance.find(
      token => (token.currency as Token).address === syntheticToken.address,
    );

    const nativeCurrency = tokensBalance.find(t => t.currency.isNative);

    setCurrentPairBalances({
      collateralBalance,
      syntheticBalance,
      nativeCurrency,
    });
  }, [tokensBalance, collateralToken, syntheticToken]);

  return currentPairBalances;
};
