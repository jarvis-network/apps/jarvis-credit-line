import { FixBigNumber } from '@jarvisnetwork/core-sdk';
import { BigNumber, Contract } from 'ethers';
import { useCallback, useState } from 'react';

import { useEstimatedNetworkFee } from './useEstimatedNetworkFee';

import { useWriteCreditLineContract } from './useContract';

import { useTransactionAdder } from '@/state/transactions/hooks';
import { useToggleTransactionModal } from '@/state/app/hooks';
import { TransactionType } from '@/types';

import { DECIMAL_DIGITS_COUNT } from '@/config/app';
import { useCurrentPair } from '@/components/CurrentPairProvider';

export const useDepositLoan = (
  collateralAmount: FixBigNumber | undefined,
  creditLineContract: Contract | undefined | null,
) => {
  const toggleTransactionModal = useToggleTransactionModal();
  const addTransaction = useTransactionAdder();
  const { collateralToken, syntheticToken } = useCurrentPair();
  const [networkFee, setNetworkFee] = useState({ gasLimit: '0', fee: '0' });

  const estimateGasFunction = () =>
    creditLineContract?.estimateGas.deposit(collateralAmount?.toFormat());

  const depositNetworkFee = useEstimatedNetworkFee(estimateGasFunction);

  const depositEstimatedNetworkFee = useCallback(async () => {
    if (!collateralAmount) return networkFee.fee;
    const data = await depositNetworkFee();
    if (!data) return networkFee.fee;
    setNetworkFee({
      gasLimit: data.gasLimit.toString(),
      fee: data.fee?.toFixed(DECIMAL_DIGITS_COUNT),
    });
    return data.fee.toFixed(DECIMAL_DIGITS_COUNT);
  }, [collateralAmount, depositNetworkFee]);

  const { writeAsync } = useWriteCreditLineContract(
    creditLineContract?.address,
    'deposit',
    [BigNumber.from(collateralAmount?.toFormat() ?? 0)],
    networkFee.gasLimit,
    !!networkFee.gasLimit,
  );
  const deposit = useCallback(async () => {
    const logFailure = (error: Error | string) => {
      console.warn(`Create Loan failed:`, error);
    };
    if (!collateralToken) return logFailure('Collateral Token is null');
    if (!syntheticToken) return logFailure('Synthetic Token is null');
    if (!creditLineContract) {
      return logFailure('creditLineContract is null');
    }
    if (!collateralAmount) {
      return logFailure('collateralAmount amount undefined');
    }
    const tx = await writeAsync();
    addTransaction(tx.hash, {
      type: TransactionType.DEPOSIT,
      pairAddress: creditLineContract.address,
      collateralSymbol: collateralToken.symbol,
      collateralAmount: collateralAmount.toExact(),
    });
    toggleTransactionModal();
  }, [
    collateralToken,
    syntheticToken,
    creditLineContract,
    collateralAmount,
    toggleTransactionModal,
    addTransaction,
    writeAsync,
  ]);

  return { depositEstimatedNetworkFee, deposit };
};
