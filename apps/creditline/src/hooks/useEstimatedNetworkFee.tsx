import { FixBigNumber } from '@jarvisnetwork/core-sdk';
import { calculateGasMargin } from '@jarvisnetwork/toolkit';

import { useCallback } from 'react';
import { useFeeData, useProvider } from 'wagmi';

export const useEstimatedNetworkFee = (
  estimateGasCall: (data?: any) => Promise<any> | undefined,
) => {
  const { data } = useFeeData();

  return useCallback(
    async (input?: any) => {
      if (!data) return null;
      const { gasPrice } = data;

      try {
        const estimatedGas = input
          ? await estimateGasCall(input)
          : await estimateGasCall();
        if (!gasPrice || !estimatedGas) return null;
        return {
          fee: FixBigNumber.fromWei(gasPrice.toString()).multiply(
            calculateGasMargin(estimatedGas).toString(),
          ),
          gasLimit: calculateGasMargin(estimatedGas),
        };
      } catch (error) {
        console.log({ error });
        return null;
      }
    },
    [estimateGasCall, data],
  );
};
