import { Currency, NetworkId } from '@jarvisnetwork/core-sdk';

import { useNetwork } from 'wagmi';

import { defaultSupportedNetworkId } from '@jarvisnetwork/toolkit';

import { useMemo } from 'react';

import { useReadCreditLineRegistry } from '@/hooks/useContract';
import { CREDIT_LINE } from '@/config/creditLine';
import { SyntheticToken } from '@/enums';
import { useNetworkCurrencies } from '@/components/NetworkCurrenciesProvider';

export const useGetCollateralTokensList = (
  synthetic: SyntheticToken,
): Currency[] | [] => {
  const { chain } = useNetwork();
  const chainId = chain?.id ?? defaultSupportedNetworkId;
  const networkCurrencies = useNetworkCurrencies();
  const { data } = useReadCreditLineRegistry('getCollaterals');

  return useMemo(() => {
    if (!networkCurrencies || !data || !data.length || !chainId) return [];
    const supportedCollateral = networkCurrencies.filter(
      currency => currency.isToken && data.includes(currency.address),
    );
    if (
      !supportedCollateral ||
      !supportedCollateral.length ||
      !CREDIT_LINE[chainId as NetworkId] ||
      !CREDIT_LINE[chainId as NetworkId]?.CreditLine
    )
      return [];
    let collaterals: Currency[] | [] = [];
    for (const pair of Object.values(
      CREDIT_LINE[chainId as NetworkId]!.CreditLine,
    )) {
      if (pair.synthetic !== synthetic) continue;
      const el = supportedCollateral.find(
        col => col.symbol === pair.collateral,
      );
      if (!el) continue;
      collaterals = [...collaterals, el];
    }

    return collaterals;
  }, [networkCurrencies, data, chainId]);
};
