import {
  Price,
  CHAINLINK_PRICE_FEED,
  FixBigNumber,
} from '@jarvisnetwork/core-sdk';
import { defaultSupportedNetworkId } from '@jarvisnetwork/toolkit';
import { ethers } from 'ethers';
import { useMemo } from 'react';

import { useNetwork } from 'wagmi';

import { useMultiCallChainLink, useReadPriceFeed } from './useContract';

export const useGetPrices = (pairs?: string[]): Price[] => {
  const { chain } = useNetwork();
  const chainId = chain?.id ?? defaultSupportedNetworkId;
  const stablePairs = ['USDCUSD', 'BUSDUSD', 'USDTUSD', 'DAIUSD'];

  const pairsFiltered = pairs?.filter(p => !stablePairs.includes(p));

  const uniquePairs = [...new Set(pairsFiltered)];

  const chainLinkPriceFeedPairs = uniquePairs.filter(pair =>
    CHAINLINK_PRICE_FEED[chainId] ? CHAINLINK_PRICE_FEED[chainId][pair] : false,
  );
  const { data: chainLinkPrices } = useMultiCallChainLink(
    chainLinkPriceFeedPairs.map(pair => ({
      address: CHAINLINK_PRICE_FEED[chainId][pair].address,
      functionName: 'latestAnswer',
    })),
    true,
    chainId,
    !!chainLinkPriceFeedPairs?.length,
  );

  const noChainLinkPriceFeedPairs = uniquePairs.filter(
    pair => !chainLinkPriceFeedPairs.includes(pair),
  );

  const { data: aggregatorPrices } = useReadPriceFeed(
    'getLatestPrices',
    [
      noChainLinkPriceFeedPairs.map(pair =>
        ethers.utils.formatBytes32String(pair),
      ),
    ],
    true,
    chainId,
    !!noChainLinkPriceFeedPairs?.length,
  );

  return useMemo(() => {
    let pairsPrice: Price[] = [];
    if (!pairs || !pairs?.length) return pairsPrice;
    if (
      chainLinkPrices?.length &&
      chainLinkPrices.filter(p => p !== null).length
    ) {
      pairsPrice = [
        ...pairsPrice,
        ...chainLinkPrices.map((price, index) => ({
          pair: chainLinkPriceFeedPairs[index],
          price: FixBigNumber.fromWei(price.toString()).multiply(
            10 **
              (18 -
                CHAINLINK_PRICE_FEED[chainId][chainLinkPriceFeedPairs[index]]
                  .decimals),
          ),
        })),
      ];
    }

    if (
      aggregatorPrices?.length &&
      aggregatorPrices.filter(p => p !== null).length
    ) {
      pairsPrice = [
        ...pairsPrice,
        ...aggregatorPrices.map((price: unknown, index: number) => ({
          pair: noChainLinkPriceFeedPairs[index],
          price: FixBigNumber.fromWei(price.toString()),
        })),
      ];
    }
    const addStablePrice = pairs.reduce((acc: Price[], pair) => {
      if (stablePairs.includes(pair)) {
        acc.push({ pair, price: FixBigNumber.toWei('1') });
      }
      return acc;
    }, []);
    return [...pairsPrice, ...addStablePrice];
  }, [pairs, chainLinkPrices, aggregatorPrices]);
};
