import { Currency, NetworkId } from '@jarvisnetwork/core-sdk';

import { useNetwork, useContractRead } from 'wagmi';

import { defaultSupportedNetworkId } from '@jarvisnetwork/toolkit';

import { useMemo } from 'react';

import { SelfMintingRegistry_Abi } from '@jarvisnetwork/synthereum-contracts/build/abi';

import { NetworksTokens } from '@/config/tokens';
import { CREDIT_LINE } from '@/config/creditLine';

export const useGetSyntheticTokens = (): Currency[] | [] => {
  const { chain } = useNetwork();
  const chainId = chain?.id ?? defaultSupportedNetworkId;

  const { data } = useContractRead({
    address: CREDIT_LINE[chainId as NetworkId]
      ?.CreditLineRegistry as `0x${string}`,
    // eslint-disable-next-line camelcase
    abi: SelfMintingRegistry_Abi,
    functionName: 'getSyntheticTokens',
  });

  return useMemo(() => {
    if (!chainId || !data) return [];
    const networkTokens = NetworksTokens[chainId as NetworkId];
    if (!networkTokens) return [];
    return Object.values(networkTokens).filter(token =>
      data.includes(token.symbol),
    );
  }, [data, chainId]);
};
