import { FixBigNumber } from '@jarvisnetwork/core-sdk';
import { BigNumber, Contract } from 'ethers';
import { useCallback, useState } from 'react';

import { useEstimatedNetworkFee } from './useEstimatedNetworkFee';

import { useWriteCreditLineContract } from './useContract';

import { TransactionType } from '@/types';
import { DECIMAL_DIGITS_COUNT } from '@/config/app';
import { useTransactionAdder } from '@/state/transactions/hooks';
import { useToggleTransactionModal } from '@/state/app/hooks';
import { useCurrentPair } from '@/components/CurrentPairProvider';

export const useRedeemLoan = (
  syntheticAmount: FixBigNumber | undefined,
  estimatedCollateralAmount: FixBigNumber | undefined,
  creditLineContract: Contract | undefined | null,
  actionType: TransactionType.REDEEM | TransactionType.CLOSE,
) => {
  const toggleTransactionModal = useToggleTransactionModal();
  const addTransaction = useTransactionAdder();
  const { collateralToken, syntheticToken } = useCurrentPair();
  const [networkFee, setNetworkFee] = useState({ gasLimit: '0', fee: '0' });

  const estimateGasFunction = () =>
    creditLineContract?.estimateGas.redeem(syntheticAmount?.toFormat());

  const redeemNetworkFee = useEstimatedNetworkFee(estimateGasFunction);

  const redeemEstimatedNetworkFee = useCallback(async () => {
    if (!syntheticAmount) return networkFee.fee;
    const data = await redeemNetworkFee();
    if (!data) return networkFee.fee;
    setNetworkFee({
      gasLimit: data.gasLimit.toString(),
      fee: data.fee?.toFixed(DECIMAL_DIGITS_COUNT),
    });

    return data.fee.toFixed(DECIMAL_DIGITS_COUNT);
  }, [syntheticAmount, redeemNetworkFee]);

  const { writeAsync } = useWriteCreditLineContract(
    creditLineContract?.address,
    'redeem',
    [BigNumber.from(syntheticAmount?.toFormat() ?? 0)],
    networkFee.gasLimit,
    !!networkFee.gasLimit,
  );

  const redeem = useCallback(async () => {
    const logFailure = (error: Error | string) => {
      console.warn(`Create Loan failed:`, error);
    };
    if (!collateralToken) return logFailure('Collateral Token is null');
    if (!syntheticToken) return logFailure('Synthetic Token is null');
    if (!creditLineContract) {
      return logFailure('creditLineContract is null');
    }

    if (!syntheticAmount) {
      return logFailure('syntheticAmount amount undefined');
    }

    if (!estimatedCollateralAmount) {
      return logFailure('CollateralAmount amount undefined');
    }
    const tx = await writeAsync();
    addTransaction(tx.hash, {
      type: actionType,
      pairAddress: creditLineContract.address,
      collateralSymbol: collateralToken.symbol,
      collateralAmount: estimatedCollateralAmount.toExact(),
      syntheticSymbol: syntheticToken.symbol,
      syntheticAmount: syntheticAmount.toExact(),
    });
    toggleTransactionModal();
  }, [
    collateralToken,
    syntheticToken,
    creditLineContract,
    syntheticAmount,
    estimatedCollateralAmount,
    toggleTransactionModal,
    addTransaction,
    actionType,
    writeAsync,
  ]);

  return { redeemEstimatedNetworkFee, redeem };
};
