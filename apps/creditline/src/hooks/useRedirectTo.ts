import { useRouter } from 'next/router';
import { useCallback } from 'react';

export const useRedirectToHome = () => {
  const router = useRouter();
  return useCallback(() => {
    router.push('/', undefined, { shallow: true });
  }, [router]);
};

export const useRedirectToPreviousPage = () => {
  const router = useRouter();
  return useCallback(() => {
    router.back();
  }, [router]);
};
