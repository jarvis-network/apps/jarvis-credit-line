import React, { useEffect, useMemo, useState } from 'react';
import Fuse from 'fuse.js';

const isArraysEqual = (a1?: any[], a2?: any[]) =>
  a1 &&
  a2 &&
  a1.length === a2.length &&
  a1.every(
    (o, idx) =>
      Object.keys(o).length === Object.keys(a2[idx]).length &&
      Object.keys(o).every(p => o[p] === a2[idx][p]),
  );

export const useSearchBar = <T>(
  keysSearched: string[],
  placeholder: string,
  dataToSearch?: T[],
) => {
  const [searchResult, setSearchResult] =
    useState<typeof dataToSearch>(dataToSearch);

  const [currentDataProvided, setCurrentDataProvided] =
    useState<typeof dataToSearch>(dataToSearch);

  useEffect(() => {
    if (!isArraysEqual(dataToSearch, currentDataProvided)) {
      setCurrentDataProvided(dataToSearch);
      setSearchResult(dataToSearch);
    }
  }, [dataToSearch, currentDataProvided]);

  const searcher = useMemo(() => {
    if (!dataToSearch) return undefined;
    return new Fuse(dataToSearch, {
      keys: keysSearched,
      threshold: 0.2,
    });
  }, [dataToSearch, keysSearched]);

  const searchBarProps = useMemo(
    () => ({
      placeholder,
      data: dataToSearch,
      onChange: (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.value === '') {
          setSearchResult(dataToSearch);
          return;
        }
        setSearchResult(
          searcher?.search(event.target.value).map(result => result.item),
        );
      },
    }),
    [searcher, dataToSearch, placeholder],
  );

  return { searchBarProps, searchResult };
};
