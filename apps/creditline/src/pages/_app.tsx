import React from 'react';
import 'rc-slider/assets/index.css';

import './_app.scss';
import Head from 'next/head';
import { AppProps } from 'next/app';
import { useIsMounted, NotificationsProvider, styled } from '@jarvisnetwork/ui';

import { Provider as StateProvider } from 'react-redux';

import { WagmiConfig } from 'wagmi';

import NoSSR from '@/components/NoSSR';

import { store } from '@/state/store';
import { AppThemeProvider } from '@/components/AppThemeProvider';

import { wagmiAppClient } from '@/config/connectors';
import { Modals } from '@/components/Modals';
import { StickyHeader } from '@/components/StickyHeader';
import TransactionModalHandler from '@/components/TransactionModalHandler';

import { NetworkCurrenciesProvider } from '@/components/NetworkCurrenciesProvider';
import { PriceFeedProvider } from '@/components/PriceFeedProvider';

import CurrentUserLoanPositionUpdater from '@/state/loan/updater';

import { CurrentPairProvider } from '@/components/CurrentPairProvider';
import { UserCurrenciesBalanceProvider } from '@/components/UserCurrenciesBalanceProvider';
import { RainbowKit } from '@/components/RaimbowKit';

const MainWrapper = styled.div`
  height: 100vh;
  width: 100vw;
  background: ${props => props.theme.background.primary};
  color: ${props => props.theme.text.primary};
`;

const Updater = () => (
  <>
    <CurrentUserLoanPositionUpdater />
  </>
);
function CustomApp({ Component, pageProps }: AppProps) {
  const isWindowVisible = useIsMounted();
  if (!isWindowVisible) return null;

  return (
    <NoSSR>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0, viewport-fit=cover"
        />
      </Head>
      <StateProvider store={store}>
        <WagmiConfig client={wagmiAppClient}>
          <AppThemeProvider>
            <NotificationsProvider>
              <RainbowKit>
                <NetworkCurrenciesProvider>
                  <PriceFeedProvider>
                    <UserCurrenciesBalanceProvider>
                      <CurrentPairProvider>
                        <Updater />
                        <Modals />
                        <MainWrapper>
                          <StickyHeader>
                            <Component {...pageProps} />
                          </StickyHeader>
                        </MainWrapper>
                        <TransactionModalHandler />
                      </CurrentPairProvider>
                    </UserCurrenciesBalanceProvider>
                  </PriceFeedProvider>
                </NetworkCurrenciesProvider>
              </RainbowKit>
            </NotificationsProvider>
          </AppThemeProvider>
        </WagmiConfig>
      </StateProvider>
    </NoSSR>
  );
}

export default CustomApp;
