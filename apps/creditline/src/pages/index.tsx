import React from 'react';
import { Icon, OnMobilOrTablet, styled } from '@jarvisnetwork/ui';

import { IconButton } from '@jarvisnetwork/toolkit';

import PageSwitcher from '@/components/PageSwitcher';
import SelectSyntheticToBorrow from '@/components/SelectSyntheticToBorrow';
import {
  useCleanSelectedSynthetic,
  useSelectedSynthetic,
} from '@/state/assets/hooks';

import SelectCollateral from '@/components/SelectCollateral';

const Container = styled.div`
  padding: 0px 16px;
  margin-top: 12px;
  @media (min-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.tabletIndex]}px) {
    padding: 0px 40px;
  }
  @media screen and (min-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    margin-top: 8em;
    padding: 0 20vw;
    padding-bottom: 2vh;
  }
`;

const BackText = styled.div`
  font-size: ${props => props.theme.font.sizes.m};
  margin-left: 8px;
`;

const BackIconButton = styled(IconButton)`
  color: ${props => props.theme.text.primary};
  display: flex;
  align-items: center;
  padding: 0;
  i {
    display: block;
    svg {
      width: 100%;
      height: 100%;
      right: 0px;
      top: 0px;
    }
  }
  @media screen and (min-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    top: 96px;
  }
`;

const Title = styled.h1`
  margin-top: 24px;
  font: normal normal bold 44px/50px Kaisei HarunoUmi;
  align-items: center;
  @media screen and (min-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    margin-top: 32px;
    min-height: 87px;
    display: flex;
    font: normal normal bold 60px/42px Kaisei HarunoUmi;
  }
`;
const SubTitle = styled.h2`
  margin-top: 8px;
  color: ${props => props.theme.text.secondary};
  font-weight: normal;
`;

export const Index = () => {
  const selectedSynthetic = useSelectedSynthetic();
  const cleanSelectedSynthetic = useCleanSelectedSynthetic();
  return (
    <Container>
      <OnMobilOrTablet>
        <PageSwitcher />
      </OnMobilOrTablet>
      {selectedSynthetic && (
        <BackIconButton onClick={() => cleanSelectedSynthetic()}>
          <Icon icon="backArrowIcon" />
          <BackText>Back</BackText>
        </BackIconButton>
      )}
      <Title>Borrow without interest fee</Title>
      <SubTitle>Pay a fixed 0.5% origination fee to borrow any asset</SubTitle>
      {!selectedSynthetic ? <SelectSyntheticToBorrow /> : <SelectCollateral />}
    </Container>
  );
};

export default Index;
