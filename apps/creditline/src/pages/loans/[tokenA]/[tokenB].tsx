import React, { useMemo } from 'react';

import { useAccount } from 'wagmi';

import {
  useRedirectToPreviousPage,
  useRedirectToHome,
} from '@/hooks/useRedirectTo';

import { Container } from '@/components/BorrowPage/styles';

import BorrowPageDesktopView from '@/components/BorrowPage/DesktopView';
import { useCurrentPair } from '@/components/CurrentPairProvider';

const LoanPage = (): JSX.Element => {
  const currentPair = useCurrentPair();
  const redirectToPreviousPage = useRedirectToPreviousPage();
  const redirectToHome = useRedirectToHome();
  const { isConnected } = useAccount();

  useMemo(() => {
    if (
      !isConnected ||
      currentPair.collateralToken === null ||
      currentPair.syntheticToken === null
    ) {
      redirectToHome();
    }
  }, [isConnected, currentPair]);
  return (
    <Container>
      <BorrowPageDesktopView redirectTo={() => redirectToPreviousPage()} />
    </Container>
  );
};

export default LoanPage;
