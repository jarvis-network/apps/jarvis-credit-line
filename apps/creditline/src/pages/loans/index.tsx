import React, { useMemo, useState } from 'react';
import {
  Button,
  ColumnType,
  DataGrid,
  DataGridColumnProps,
  Flag,
  OnMobilOrTablet,
  SearchBar,
  styled,
  styledScrollbars,
} from '@jarvisnetwork/ui';

import { Currency, FixBigNumber } from '@jarvisnetwork/core-sdk';

import { useRouter } from 'next/router';

import { useDispatch } from 'react-redux';

import PageSwitcher from '@/components/PageSwitcher';
import { useAllUserCreditLineWithUserLoanPositionData } from '@/hooks/useActiveLoan';

import { useSearchBar } from '@/hooks/useSearchBar';
import { PANEL_TYPE } from '@/enums';
import { setPanelType } from '@/state/app/reducer';
import { usePricesFeed } from '@/components/PriceFeedProvider';
import { useNetworkCurrencies } from '@/components/NetworkCurrenciesProvider';
import { numberFormat } from '@/utils/formatNumber';

const Container = styled.div`
  padding: 0px 16px;
  margin-top: 2em;
  @media (min-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.tabletIndex]}px) {
    padding: 0px 40px;
  }
  @media screen and (min-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    margin-top: 8em;
    padding: 0px 20vw;
  }
`;

const MoneyBag = styled.div`
  &:before {
    content: '💰';
    display: block;
    font-size: 50px;
    margin-bottom: 1rem;
  }
  text-align: center;
`;

const LoansContainer = styled.div`
  margin-top: 28px;
`;

const LoansTabContainer = styled.div`
  margin-top: 40px;
  padding: 24px;
  width: 100%;
  height: 100%;
  background: ${props => props.theme.background.secondary};
  border: 1px solid ${props => props.theme.border.primary};
  border-radius: ${props => props.theme.borderRadius.l};
`;

const SearchContainer = styled.div`
  display: flex;
  max-width: 485px;
  margin-top: 24px;
  height: 50px;
`;

const CustomValue = styled.div`
  text-align: end;
  p {
    margin: 0;
  }
  .good {
    color: ${props => props.theme.common.primary};
  }
  .warning {
    color: ${props => props.theme.common.warning};
  }
  .danger {
    color: ${props => props.theme.common.danger};
  }
  .in-dollars {
    color: ${props => props.theme.text.secondary};
    @media screen and (max-width: ${props =>
        props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
      display: none;
    }
  }
`;

const LoanIconsWrapper = styled.div`
  display: flex;
  img {
    width: 24px;
  }
  align-items: center;
  > div {
    margin-right: 8px;
  }
  .synth-currency {
    margin-left: -10px;
  }

  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex]}px) {
    margin-right: 10px;
  }
`;

const Grid = styled(DataGrid)`
  background: transparent;
  height: 100%;
  .rt-td:first-child {
    padding: 8px 0px !important;
  }
  .rt-tr-group:last-child {
    border: none !important;
  }
  .rt-tr-group {
    height: 80px !important;
    .rt-tr {
      height: 100%;
    }
  }
  .rt-th {
    text-align: center;
    padding: 12px 0px !important;
  }
  .rt-tbody {
    ${props => styledScrollbars(props.theme)}
  }
  .logo {
    width: 32px;
    height: 32px;
  }
`;

export type LoanGridData = {
  syntheticCurrency: Currency | undefined;
  collateralCurrency: Currency | undefined;
  debtAmount: FixBigNumber;
  collateralAmount: FixBigNumber;
  liquidationRatio: FixBigNumber;
  collateralRequirement: FixBigNumber;
  price: FixBigNumber | undefined;
};

const LoansPageDasboard = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const currencyArrayList = useNetworkCurrencies();
  const loansData = useAllUserCreditLineWithUserLoanPositionData();
  const prices = usePricesFeed();

  const [gridData, setGridData] = useState<LoanGridData[]>([]);
  const { searchBarProps, searchResult: activeLoansFound } =
    useSearchBar<LoanGridData>(
      ['syntheticCurrency.symbol', 'collateralCurrency.symbol'],
      'Search',
      gridData,
    );

  useMemo(() => {
    if (
      !prices ||
      !Object.keys(prices).length ||
      !loansData?.length ||
      !currencyArrayList?.length
    )
      return undefined;

    const data = loansData
      .filter(
        loanData =>
          loanData.collateralAmount.greaterThan(0) ||
          (loanData.tokensAmount.greaterThan(0) &&
            currencyArrayList.find(
              currency => currency.symbol === loanData.synthetic,
            ) &&
            currencyArrayList.find(
              currency => currency.symbol === loanData.collateral,
            ) &&
            !!prices[loanData.pair]),
      )
      .map(loanData => {
        const syntheticCurrency = currencyArrayList.find(
          currency => currency.symbol === loanData.synthetic,
        );
        const collateralCurrency = currencyArrayList.find(
          currency => currency.symbol === loanData.collateral,
        );
        const price = prices[loanData.pair];

        return {
          syntheticCurrency,
          collateralCurrency,
          debtAmount: loanData.tokensAmount,
          collateralAmount: loanData.collateralAmount,
          liquidationRatio: loanData.collateralRequirement,
          collateralRequirement: loanData.collateralRequirement,
          price,
        };
      });

    setGridData(data);
  }, [currencyArrayList, prices]);

  const loansTableCols: DataGridColumnProps[] = [
    {
      header: 'Loan',
      key: 'loan',
      type: ColumnType.CustomCell,
      width: 60,
      cell: ({ original, index }) => {
        const isLoaded =
          !!original.syntheticCurrency?.symbol &&
          !!original.collateralCurrency?.symbol;

        return (
          <LoanIconsWrapper>
            {isLoaded ? (
              <>
                <Flag
                  flag={original.collateralCurrency.symbol}
                  className="logo"
                />
                <Flag
                  className="synth-currency logo"
                  flag={original.syntheticCurrency.symbol}
                />
              </>
            ) : (
              '-'
            )}
          </LoanIconsWrapper>
        );
      },
    },
    {
      header: 'Debt',
      key: 'debt',
      type: ColumnType.CustomCell,
      cell: ({ original }) => {
        const isLoaded =
          !!original.syntheticCurrency?.symbol &&
          !!original.debtAmount &&
          !!original.price;
        return (
          <CustomValue>
            {isLoaded ? (
              <div>
                {numberFormat(original.debtAmount.toExact(), false)}{' '}
                {original.syntheticCurrency.symbol}
              </div>
            ) : (
              '-'
            )}
          </CustomValue>
        );
      },
    },
    {
      header: 'Collateral',
      key: 'collateral',
      type: ColumnType.CustomCell,
      cell: ({ original }) => {
        const isLoaded =
          !!original.collateralAmount &&
          !!original.collateralCurrency?.symbol &&
          !!original.price;
        return (
          <CustomValue>
            {' '}
            {isLoaded ? (
              <div>
                {numberFormat(original.collateralAmount.toExact(), false)}{' '}
                {original.collateralCurrency.symbol}
              </div>
            ) : (
              '-'
            )}
          </CustomValue>
        );
      },
    },
    {
      header: 'Liquidation',
      key: 'liquidationRatio',
      type: ColumnType.CustomCell,
      width: 80,
      cell: ({ original }) => {
        const isLoaded = !!original.liquidationRatio;
        return (
          <CustomValue>
            {isLoaded ? (
              <div>{original.liquidationRatio.multiply(100).toFixed(2)} %</div>
            ) : (
              '-'
            )}
          </CustomValue>
        );
      },
    },
    {
      header: 'Health Ratio',
      key: 'userRatio',
      type: ColumnType.CustomCell,
      width: 150,
      cell: ({ original }) => {
        const isLoaded =
          !!original.debtAmount &&
          !!original.collateralRequirement &&
          !!original.price;
        if (!isLoaded) {
          return <CustomValue>-</CustomValue>;
        }

        const ratio = original.collateralAmount
          .divide(original.debtAmount.multiply(original.price))
          .multiply(100);
        const ratioRequirement = original.collateralRequirement.multiply(100);

        const ratioStatus = ratio.greaterThanOrEqualTo(125)
          ? 'good'
          : ratio.lessThanOrEqualTo(ratioRequirement)
          ? 'danger'
          : 'warning';
        const smiley = ratio.greaterThanOrEqualTo(125)
          ? '😊'
          : ratio.lessThanOrEqualTo(ratioRequirement)
          ? '😟'
          : '😕';
        return (
          <CustomValue>
            <div className={ratioStatus}>
              {smiley} {ratio.toFixed(2)} %
            </div>
          </CustomValue>
        );
      },
    },
    {
      header: '',
      key: 'manage',
      type: ColumnType.CustomCell,
      width: 100,
      cell: ({ original }) => {
        const redirectToManageLoan = () => {
          const url = `/loans/${original.collateralCurrency.address}/${original.syntheticCurrency.address}`;
          dispatch(setPanelType(PANEL_TYPE.HOME));
          router.push(url, undefined, { shallow: true });
        };

        return (
          <Button
            type="secondary"
            size="m"
            onClick={() => redirectToManageLoan()}
          >
            Manage
          </Button>
        );
      },
    },
  ];

  return (
    <Container>
      <OnMobilOrTablet>
        <PageSwitcher />
      </OnMobilOrTablet>
      <LoansContainer>
        <SearchContainer>
          <SearchBar {...searchBarProps} />
        </SearchContainer>
        <LoansTabContainer>
          {activeLoansFound?.length &&
          prices &&
          Object.values(prices).length &&
          loansData &&
          currencyArrayList &&
          currencyArrayList?.length > 0 ? (
            <Grid
              columns={loansTableCols}
              data={activeLoansFound}
              showPagination={false}
              pageSize={activeLoansFound.length}
            />
          ) : (
            <MoneyBag>Here you will see your open loans...</MoneyBag>
          )}
        </LoansTabContainer>
      </LoansContainer>
    </Container>
  );
};
export default LoansPageDasboard;
