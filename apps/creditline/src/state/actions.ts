import { createAction } from '@reduxjs/toolkit';

// Contains actions that multiple slices will be subscribing to

export const logoutAction = createAction('logout');
export const addressSwitchAction = createAction<{
  networkId?: number;
}>('addressSwitch');
export const networkSwitchAction = createAction<{
  networkId?: number;
}>('networkSwitch');
