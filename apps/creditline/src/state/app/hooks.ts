import { useCallback } from 'react';
import { useDispatch } from 'react-redux';

import { useReduxSelector } from '../useReduxSelector';

import { ApplicationModal, setOpenModal, setPanelType } from './reducer';

import { PANEL_TYPE } from '@/enums';

export const useModalIsOpen = (modal: ApplicationModal): boolean => {
  const openModal = useReduxSelector(state => state.app.openModal);
  return openModal === modal;
};

export const useToggleModal = (modal: ApplicationModal): (() => void) => {
  const isOpen = useModalIsOpen(modal);
  const dispatch = useDispatch();
  return useCallback(
    () => dispatch(setOpenModal(isOpen ? null : modal)),
    [dispatch, modal, isOpen],
  );
};

export const useOpenModal = (modal: ApplicationModal): (() => void) => {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(setOpenModal(modal)), [dispatch, modal]);
};

export const useCloseModal = (_modal: ApplicationModal): (() => void) => {
  const dispatch = useDispatch();
  return useCallback(() => dispatch(setOpenModal(null)), [dispatch]);
};

export const useToggleAuthenticationModal = () =>
  useToggleModal(ApplicationModal.AUTH);

export const useToggleAccountSummaryModal = () =>
  useToggleModal(ApplicationModal.ACCOUNT);

export const useToggleTransactionModal = () =>
  useToggleModal(ApplicationModal.TRANSACTION);

export const useSetPanelType = (panelType: PANEL_TYPE) => {
  const dispatch = useDispatch();
  return useCallback(
    () => dispatch(setPanelType(panelType)),
    [dispatch, panelType],
  );
};

export const usePanelType = () => {
  const panelType = useReduxSelector(state => state.app.panelType);
  return panelType;
};
