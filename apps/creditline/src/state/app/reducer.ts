import { createSlice } from '@reduxjs/toolkit';

import { initialAppState } from '@/state/initialState';

import { PANEL_TYPE } from '@/enums';

export enum ApplicationModal {
  AUTH,
  ACCOUNT,
  TRANSACTION,
}

const appSlice = createSlice({
  name: 'app',
  initialState: initialAppState.app,
  reducers: {
    setAuthModalVisible: (state, action: { payload: boolean }) => {
      state.isAuthModalVisible = action.payload;
    },
    setAccountModalVisible: (state, action: { payload: boolean }) => {
      state.isAccountModalVisible = action.payload;
    },
    setOpenModal: (state, action: { payload: ApplicationModal | null }) => {
      state.openModal = action.payload;
    },
    setIsUnsupportedNetworkId: (state, action: { payload: boolean }) => {
      state.isUnsupportedNetworkId = action.payload;
    },
    setPanelType: (state, action: { payload: PANEL_TYPE }) => {
      state.panelType = action.payload;
    },
  },
});

export const {
  setOpenModal,
  setIsUnsupportedNetworkId,
  setAuthModalVisible,
  setAccountModalVisible,
  setPanelType,
} = appSlice.actions;
export default appSlice.reducer;
