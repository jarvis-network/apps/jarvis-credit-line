import { useCallback } from 'react';
import { useDispatch } from 'react-redux';

import { useReduxSelector } from '../useReduxSelector';

import { setSelectedSynthetic } from './reducer';

export const useSelectedSynthetic = () => {
  const asset = useReduxSelector(state => state.assets);
  return asset.selectedSynthetic;
};

export const useCleanSelectedSynthetic = () => {
  const dispatch = useDispatch();
  return useCallback(
    () => dispatch(setSelectedSynthetic(undefined)),
    [dispatch],
  );
};
