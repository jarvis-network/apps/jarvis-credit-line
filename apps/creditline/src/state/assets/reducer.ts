import { createSlice } from '@reduxjs/toolkit';

import { initialAppState } from '../initialState';

const assetsSlice = createSlice({
  name: 'assets',
  initialState: initialAppState.assets,
  reducers: {
    setSelectedSynthetic: (assets, { payload }) => {
      assets.selectedSynthetic = payload;
    },
  },
});

export const { setSelectedSynthetic } = assetsSlice.actions;
export default assetsSlice.reducer;
