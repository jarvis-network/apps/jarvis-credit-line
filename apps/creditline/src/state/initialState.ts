import { ThemeNameType } from '@jarvisnetwork/ui';

import { cache } from '@jarvisnetwork/toolkit';

import { ApplicationModal } from './app/reducer';

import { UserLoanPosition } from './loan/reducer';

import { PANEL_TYPE } from '@/enums';
import { TransactionDetails } from '@/types';

export interface State {
  theme: ThemeNameType;
  app: {
    isAuthModalVisible: boolean;
    isAccountModalVisible: boolean;
    isUnsupportedNetworkId: boolean;
    openModal: ApplicationModal | null;
    panelType: PANEL_TYPE;
  };
  assets: {
    selectedSynthetic: string | undefined;
  };
  loan: UserLoanPosition | Record<string, never>;
  transactions: {
    [chainId: number]: {
      [txHash: string]: TransactionDetails;
    };
  };
}

export const initialAppState: State = {
  theme: cache.get<ThemeNameType | null>('jarvis/state/theme') || 'light',
  app: {
    isAuthModalVisible: false,
    isAccountModalVisible: false,
    isUnsupportedNetworkId: false,
    openModal: null,
    panelType: PANEL_TYPE.HOME,
  },
  loan: {},
  assets: {
    selectedSynthetic: undefined,
  },

  transactions: {},
};
