import { FixBigNumber } from '@jarvisnetwork/core-sdk';

import { useMemo } from 'react';

import { useReduxSelector } from '../useReduxSelector';

import { useCurrentPair } from '@/components/CurrentPairProvider';
import { useCreditLinePairData } from '@/hooks/useCreditLinePairData';

export const useUserCurrentLoanPosition = () => {
  const { collateralToken } = useCurrentPair();
  const currentPairData = useCreditLinePairData();
  const loan = useReduxSelector(state => state.loan);

  return useMemo(() => {
    if (!loan.pair || loan.pair !== currentPairData?.pair) return undefined;
    return {
      ...loan,
      collateralAmount: FixBigNumber.toWei(
        loan.collateralAmount,
        collateralToken?.decimals,
      ),
      syntheticAmount: FixBigNumber.toWei(loan.syntheticAmount),
      healthRatio: FixBigNumber.toWei(loan.healthRatio),
      liquidationPrice: FixBigNumber.toWei(loan.liquidationPrice),
    };
  }, [loan, currentPairData]);
};
