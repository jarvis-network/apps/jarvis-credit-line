import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { initialAppState } from '../initialState';

export interface UserLoanPosition {
  pair: string;
  collateralAmount: string;
  syntheticAmount: string;
  healthRatio: string;
  liquidationPrice: string;
}

const loanSlice = createSlice({
  name: 'loan',
  initialState: initialAppState.loan,
  reducers: {
    setUserLoanPositionData: (
      loan,
      { payload }: PayloadAction<UserLoanPosition | undefined>,
    ) => {
      if (!payload) {
        return {};
      }
      return { ...payload };
    },
  },
});

export const { setUserLoanPositionData } = loanSlice.actions;
export default loanSlice.reducer;
