import { useWeb3React } from '@web3-react/core';
import { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { FixBigNumber } from '@jarvisnetwork/core-sdk';

import { useAccount } from 'wagmi';

import { setUserLoanPositionData } from './reducer';

import {
  useCreditLineContract,
  useReadCreditLineContract,
} from '@/hooks/useContract';
import { useCreditLineData } from '@/hooks/useCreditLineData';
import {
  calcHealthRatio,
  calcLiquidationPrice,
} from '@/utils/loanManagementHelpers';
import { useCurrentPair } from '@/components/CurrentPairProvider';
import { usePriceFeed } from '@/components/PriceFeedProvider';

const Updater = (): null => {
  const dispatch = useDispatch();
  const { address: account } = useAccount();
  const { syntheticToken, collateralToken } = useCurrentPair();
  const creditLineData = useCreditLineData(syntheticToken, collateralToken);
  const pairPrice = usePriceFeed(creditLineData?.pair);
  const { data: position } = useReadCreditLineContract(
    syntheticToken,
    collateralToken,
    'getPositionData',
    [account],
    true,
  );

  useEffect(() => {
    if (!creditLineData || !position || !account || !pairPrice) {
      dispatch(setUserLoanPositionData(undefined));
      return;
    }

    const collateralAmount = FixBigNumber.fromWei(
      position.collateralAmount.toString(),
      collateralToken?.decimals,
    );
    const debtAmount = FixBigNumber.fromWei(position.tokensAmount.toString());
    const currentHealthRatio = calcHealthRatio(
      collateralAmount,
      debtAmount,
      pairPrice,
    );
    const currentLiquidationPrice = calcLiquidationPrice(
      collateralAmount,
      debtAmount,
      creditLineData.collateralRequirement,
    );

    if (!currentHealthRatio || !currentLiquidationPrice) {
      dispatch(setUserLoanPositionData(undefined));
      return;
    }

    dispatch(
      setUserLoanPositionData({
        pair: creditLineData.pair,
        collateralAmount: collateralAmount.toExact(),
        syntheticAmount: debtAmount.toExact(),
        healthRatio: currentHealthRatio.toExact(),
        liquidationPrice: currentLiquidationPrice.toExact(),
      }),
    );
  }, [
    dispatch,
    pairPrice,
    creditLineData,
    position,
    account,
    collateralToken?.decimals,
  ]);

  return null;
};

export default Updater;
