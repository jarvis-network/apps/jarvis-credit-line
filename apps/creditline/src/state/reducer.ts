import { combineReducers } from '@reduxjs/toolkit';

import theme from '@/state/theme/reducer';
import app from '@/state/app/reducer';
import assets from '@/state/assets/reducer';
import transactions from '@/state/transactions/reducer';
import loan from '@/state/loan/reducer';

export const reducer = combineReducers({
  theme,
  app,
  assets,
  loan,
  transactions,
});

export type RootState = ReturnType<typeof reducer>;
