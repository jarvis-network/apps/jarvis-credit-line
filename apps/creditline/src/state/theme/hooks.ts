import { useReduxSelector } from '../useReduxSelector';

export const useAppTheme = () => useReduxSelector(state => state.theme);
