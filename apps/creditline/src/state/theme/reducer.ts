import { createSlice } from '@reduxjs/toolkit';
import { ThemeNameType } from '@jarvisnetwork/ui';

import { initialAppState } from '@/state/initialState';

const themeSlice = createSlice({
  name: 'theme',
  initialState: initialAppState.theme,
  reducers: {
    setTheme: (state, action: { payload: ThemeNameType }) => action.payload,
  },
});

export const { setTheme } = themeSlice.actions;
export default themeSlice.reducer;
