import { useCallback, useMemo } from 'react';

import { useDispatch } from 'react-redux';

import { useAccount, useNetwork } from 'wagmi';

import { defaultSupportedNetworkId } from '@jarvisnetwork/toolkit';

import { TransactionDetails, TransactionInfo } from '@/types';

import { addTransaction } from '@/state/transactions/reducer';
import { useReduxSelector } from '@/state/useReduxSelector';

export const useTransactionAdder = (): ((
  hash: string,
  info: TransactionInfo,
) => void) => {
  const { address: account } = useAccount();
  const { chain } = useNetwork();

  const chainId = chain?.id ?? defaultSupportedNetworkId;

  const dispatch = useDispatch();

  return useCallback(
    (hash: string, info: TransactionInfo) => {
      if (!account) return;
      if (!chainId) return;

      if (!hash) {
        throw Error('No transaction hash found.');
      }
      dispatch(addTransaction({ hash, from: account, info, chainId }));
    },
    [account, chainId, dispatch],
  );
};

// returns all the transactions
export const useAllTransactions = (): {
  [txHash: string]: TransactionDetails;
} => {
  const { chain } = useNetwork();

  const chainId = chain?.id ?? defaultSupportedNetworkId;

  const txs = useReduxSelector(state => state.transactions);

  return chainId ? txs[chainId] ?? {} : {};
};

export const useTransaction = (
  transactionHash?: string,
): TransactionDetails | undefined => {
  const allTransactions = useAllTransactions();

  if (!transactionHash) {
    return undefined;
  }

  return allTransactions[transactionHash];
};

export const useLastTransaction = (): TransactionDetails | undefined => {
  const allTransactions = useAllTransactions();
  return useMemo(
    () => Object.values(allTransactions)?.pop(),
    [allTransactions],
  );
};

export const useIsTransactionPending = (transactionHash?: string): boolean => {
  const transactions = useAllTransactions();

  if (!transactionHash || !transactions[transactionHash]) return false;

  return !transactions[transactionHash].receipt;
};

export const useIsTransactionConfirmed = (
  transactionHash?: string,
): boolean => {
  const transactions = useAllTransactions();

  if (!transactionHash || !transactions[transactionHash]) return false;

  return Boolean(transactions[transactionHash].receipt);
};
