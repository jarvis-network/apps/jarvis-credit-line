import { createSlice } from '@reduxjs/toolkit';

import { initialAppState } from '../initialState';

const now = () => new Date().getTime();

const transactionSlice = createSlice({
  name: 'transactions',
  initialState: initialAppState.transactions,
  reducers: {
    addTransaction: (
      transactions,
      { payload: { chainId, from, hash, info } },
    ) => {
      if (transactions[hash]) {
        throw Error('Attempted to add existing transaction.');
      }
      const txs = transactions[chainId] ?? {};
      txs[hash] = { hash, info, from, addedTime: now() };
      transactions[chainId] = txs;
    },
    clearAllTransactions: transactions => {
      if (!transactions) return;
      transactions = {};
    },
    checkedTransaction: (
      transactions,
      { payload: { chainId, hash, blockNumber } },
    ) => {
      const tx = transactions[chainId]?.[hash];
      if (!tx) {
        return;
      }
      if (!tx.lastCheckedBlockNumber) {
        tx.lastCheckedBlockNumber = blockNumber;
      } else {
        tx.lastCheckedBlockNumber = Math.max(
          blockNumber,
          tx.lastCheckedBlockNumber,
        );
      }
    },
    finalizeTransaction: (
      transactions,
      { payload: { chainId, hash, receipt } },
    ) => {
      const tx = transactions[chainId]?.[hash];
      if (!tx) {
        return;
      }
      tx.receipt = receipt;
      tx.confirmedTime = now();
    },
  },
});

export const {
  addTransaction,
  clearAllTransactions,
  checkedTransaction,
  finalizeTransaction,
} = transactionSlice.actions;
export default transactionSlice.reducer;
