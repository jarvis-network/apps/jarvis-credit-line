export type PairLike<
  SyntheticType extends string,
  CollateralType extends string,
  Separator extends string = '/',
> = `${SyntheticType}${Separator}${CollateralType}`;
