export interface SerializableTransactionReceipt {
  to: string;
  from: string;
  contractAddress: string;
  transactionIndex: number;
  blockHash: string;
  transactionHash: string;
  blockNumber: number;
  status?: number;
}

export enum TransactionType {
  APPROVAL = 'Approve',
  CREATE_LOAN = 'Create Loan',
  BORROW = 'Borrow',
  REPAY = ' Repay',
  REDEEM = 'Redeem',
  DEPOSIT = 'Deposit',
  WITHDRAW = 'Withdraw',
  CLOSE = 'Close',
}
export interface BaseTransactionInfo {
  type: TransactionType;
}
export interface ApproveTransactionInfo extends BaseTransactionInfo {
  type: TransactionType.APPROVAL;
  tokenAddress: string;
  tokenSymbol: string;
  spender: string;
}

interface CreateLoanTransactionInfo extends BaseTransactionInfo {
  type: TransactionType.CREATE_LOAN;
  pairAddress: string;
  collateralSymbol: string;
  collateralAmount: string;
  syntheticSymbol: string;
  syntheticAmount: string;
}
interface BorrowTransactionInfo extends BaseTransactionInfo {
  type: TransactionType.BORROW;
  pairAddress: string;
  collateralSymbol: string;
  collateralAmount: string;
  syntheticSymbol: string;
  syntheticAmount: string;
}
interface RedeemLoanTransactionInfo extends BaseTransactionInfo {
  type: TransactionType.REDEEM;
  pairAddress: string;
  collateralSymbol: string;
  collateralAmount: string;
  syntheticSymbol: string;
  syntheticAmount: string;
}
interface CloseLoanTransactionInfo extends BaseTransactionInfo {
  type: TransactionType.CLOSE;
  pairAddress: string;
  collateralSymbol: string;
  collateralAmount: string;
  syntheticSymbol: string;
  syntheticAmount: string;
}

interface RepayLoanTransactionInfo extends BaseTransactionInfo {
  type: TransactionType.REPAY;
  pairAddress: string;
  syntheticSymbol: string;
  syntheticAmount: string;
}

interface DepositLoanTransactionInfo extends BaseTransactionInfo {
  type: TransactionType.DEPOSIT;
  pairAddress: string;
  collateralSymbol: string;
  collateralAmount: string;
}
interface WithdrawLoanTransactionInfo extends BaseTransactionInfo {
  type: TransactionType.WITHDRAW;
  pairAddress: string;
  collateralSymbol: string;
  collateralAmount: string;
}

export type TransactionInfo =
  | ApproveTransactionInfo
  | RepayLoanTransactionInfo
  | RedeemLoanTransactionInfo
  | BorrowTransactionInfo
  | DepositLoanTransactionInfo
  | CloseLoanTransactionInfo
  | WithdrawLoanTransactionInfo
  | CreateLoanTransactionInfo;

export interface TransactionDetails {
  hash: string;
  receipt?: SerializableTransactionReceipt;
  lastCheckedBlockNumber?: number;
  addedTime: number;
  confirmedTime?: number;
  from: string;
  info: TransactionInfo;
}
