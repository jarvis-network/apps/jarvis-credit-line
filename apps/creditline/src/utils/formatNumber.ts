export const priceFormat = (
  number: number | string,
  currency: string,
  isNotation = true,
) =>
  new Intl.NumberFormat(`en-US`, {
    style: 'currency',
    currency,
    notation: isNotation ? 'compact' : undefined,
  })
    .format(number as number)
    .replace('TRY', '₺');

export const numberFormat = (
  number: number | string,
  isNotation = true,
  minimumFractionDigits = 2,
  maximumFractionDigits = 2,
) =>
  new Intl.NumberFormat('en-US', {
    minimumFractionDigits,
    maximumFractionDigits,
    notation: isNotation ? 'compact' : undefined,
  }).format(number as number);

export const percentFormat = (number: number, size = 3) =>
  new Intl.NumberFormat('en-US', {
    style: 'percent',
    minimumFractionDigits: size,
    maximumFractionDigits: size,
  }).format(number);
