import { Currency, Token } from '@jarvisnetwork/core-sdk';

import { PairLike } from '@/types';

import { Collateral, SyntheticToken } from '@/enums';

type AllColltaral = Collateral | SyntheticToken;
export const getValidPair = (
  synthetic?: Token | null,
  collateral?: Currency | null,
): PairLike<SyntheticToken, Collateral | SyntheticToken, '_'> | undefined => {
  if (
    synthetic &&
    collateral &&
    synthetic.symbol &&
    synthetic.symbol in SyntheticToken &&
    collateral.symbol &&
    (collateral.symbol as AllColltaral)
  ) {
    return `${synthetic.symbol as SyntheticToken}_${
      collateral.symbol as Collateral | SyntheticToken
    }`;
  }
  return undefined;
};
