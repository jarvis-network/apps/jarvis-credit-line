/* eslint-disable no-restricted-globals */
import { FixBigNumber } from '@jarvisnetwork/core-sdk';

import { EmojiKeys } from '@jarvisnetwork/ui/dist/Emoji/types';

import { FormCalculations } from '@/components/LoanManagementCard/interfaces';
import { DECIMAL_DIGITS_COUNT } from '@/config/app';
import { ERROR_TYPE, PANEL_FLOW_STEPS, PANEL_TYPE } from '@/enums';

export const calcHealthRatio = (
  collateralAmount?: FixBigNumber,
  syntheticAmount?: FixBigNumber,
  pairPrice?: FixBigNumber,
): FixBigNumber | undefined => {
  if (
    collateralAmount?.equalTo(0) ||
    syntheticAmount?.equalTo(0) ||
    !pairPrice?.greaterThan(0)
  )
    return undefined;
  if (collateralAmount.lessThan(0) || syntheticAmount.lessThan(0))
    return FixBigNumber.toWei(0);
  return collateralAmount
    .divide(syntheticAmount.multiply(pairPrice))
    .multiply(100);
};

export const calcCollateralAmountFromRatioWithoutFee = (
  healthRatio?: FixBigNumber,
  syntheticAmount?: FixBigNumber,
  pairPrice?: FixBigNumber,
): FixBigNumber | undefined => {
  if (
    !healthRatio?.greaterThan(0) ||
    !syntheticAmount?.greaterThan(0) ||
    !pairPrice?.greaterThan(0)
  )
    return undefined;
  return healthRatio.divide(100).multiply(syntheticAmount.multiply(pairPrice));
};

export const calcHealthRatioStyles = (
  healthRatio: FixBigNumber | number | undefined,
  collateralRequirement?: FixBigNumber | number,
):
  | {
      colorClass: string;
      emoji: EmojiKeys;
    }
  | undefined => {
  if (!healthRatio || !collateralRequirement) {
    return undefined;
  }
  const ratio =
    typeof healthRatio === 'number'
      ? healthRatio
      : Number(healthRatio.toFixed(2));
  const ratioRequirement =
    typeof collateralRequirement === 'number'
      ? collateralRequirement
      : Number(collateralRequirement.multiply(100).toFixed(2));
  const HRMarging = ratioRequirement - 100;
  if (ratio > ratioRequirement + HRMarging) {
    return {
      colorClass: 'success',
      emoji: 'MoneyMouthFace',
    };
  }
  if (ratio >= ratioRequirement + HRMarging / 2) {
    return {
      colorClass: 'warning',
      emoji: 'FearfulFace',
    };
  }
  if (ratio >= ratioRequirement) {
    return {
      colorClass: 'danger',
      emoji: 'OverheatedFace',
    };
  }
  return {
    colorClass: '',
    emoji: 'DeadFace',
  };
};

export const calcLiquidationPrice = (
  collateralAmount: FixBigNumber,
  syntheticAmount: FixBigNumber,
  collateralRequirement: FixBigNumber,
) => {
  if (collateralAmount.equalTo(0)) return undefined;
  return collateralRequirement.multiply(
    syntheticAmount.divide(collateralAmount),
  );
};
export const maxToBorrow = (
  collateralInput: FixBigNumber | undefined,
  feePercentage: FixBigNumber | undefined,
  price: FixBigNumber | undefined,
  collateralRequirement: FixBigNumber | undefined,
) => {
  if (!collateralInput || !feePercentage || !price || !collateralRequirement)
    return undefined;
  return collateralInput
    .subtract(collateralInput.multiply(feePercentage))
    .divide(collateralRequirement)
    .divide(price);
};

export const validateAmount = (
  collateralAmount: FixBigNumber | undefined,
  syntheticAmount: FixBigNumber | undefined,
  minSponsorTokens: FixBigNumber,
  collateralRequirement: FixBigNumber,
  currentPricePair: FixBigNumber | undefined,
  type: PANEL_TYPE,
) => {
  let calculations = null;
  let errors = null;
  if (!currentPricePair) {
    return { calculations, errors };
  }

  if (
    type !== PANEL_TYPE.CLOSE &&
    syntheticAmount &&
    minSponsorTokens.greaterThan(syntheticAmount)
  ) {
    errors = {
      type: ERROR_TYPE.MIN_SPONSOR,
      message: `Minimum borrow amount is ${minSponsorTokens.toFixed(
        DECIMAL_DIGITS_COUNT,
      )} jFIAT`,
      controls: ['borrow'],
    };
  }

  if (!errors && !!collateralAmount && !!syntheticAmount) {
    calculations = {
      healthRatio: calcHealthRatio(
        collateralAmount,
        syntheticAmount,
        currentPricePair,
      ),
      liquidationPrice: calcLiquidationPrice(
        collateralAmount,
        syntheticAmount,
        collateralRequirement,
      ),
    } as FormCalculations;
    if (
      calculations.healthRatio &&
      calculations.healthRatio.lessThan(collateralRequirement.multiply(100))
    ) {
      errors = {
        type: ERROR_TYPE.COLLATERAL_RATIO,
        message: 'Your health ratio must be higher',
        controls: ['borrow', 'collateral'],
      };
    }
  }
  return { calculations, errors };
};

export const calcOriginationFee = (
  collateralAmount: FixBigNumber | undefined,
  feePercentage: FixBigNumber | undefined,
) => {
  if (!collateralAmount || !feePercentage) {
    return undefined;
  }
  return feePercentage.multiply(collateralAmount);
};

export const shouldSkipWelcomeCard = (
  step: PANEL_FLOW_STEPS,
  type: PANEL_TYPE,
  syntheticSymbol?: string,
  collateralSymbol?: string,
) =>
  !!localStorage.getItem(
    `jarvis-creditLine/${syntheticSymbol}${collateralSymbol}/welcome-${type}-accepted`,
  ) || step !== PANEL_FLOW_STEPS.WELCOME;
